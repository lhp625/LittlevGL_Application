/*
*******************************************************************************************************
*
* 文件名称 : ir_send.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 红外发射驱动文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "ir_send.h"


/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/


/* 函数声明 ---------------------------------------------------------*/

/**
  * @brief 初始化红外对于的IO
  * @param None
  * @retval	None
  */
void ir_send_init(void)
{

	/* 定义一个GPIO初始化结构体 */
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	
	__HAL_RCC_GPIOI_CLK_ENABLE();


	GPIO_InitStruct.Pin = IR_SEND_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(IR_SEND_PORT, &GPIO_InitStruct);

	HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_RESET);


}


/**
  * @brief IO口模拟38KHz
  * @param None
  * @retval	None
  */
void ir_38khz(uint32_t us)
{
    uint32_t count;

    count = us / 24;

    for(uint32_t i = 0; i < count; i++)
    {
		HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_RESET);
        delay_us(6);
		HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_SET);
        delay_us(18);
    }
}


/**
  * @brief 数据转换
  * @param data-需要变换的数据
  * @retval	变换后的数据
  */
uint8_t m2m(uint8_t data)
{
    uint8_t y = 0;

    for(uint8_t i = 0; i < 8; i++)
    {
        if(((data >> i) & 0x01) == 1)
        {
            y |= (0x01 << (7 - i));
        }
    }

    return y;
}


/**
  * @brief 发送红外数据函数
  * @param addr-地址码
  * @param key-控制码
  * @param times-控制码连发次数
  * @retval	None
  */
void ir_send_data(uint8_t addr,uint8_t key,uint8_t times)
{
    uint8_t index;
    uint32_t data;

    key = m2m(key);

    data = ((~key & 0xFF) << 24) + ((key & 0xFF) << 16) + ((~addr & 0xff) << 8) + addr;
    //32位
    HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_RESET);
    ir_38khz(9000);
    HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_SET);
    delay_us(4500);
    //引导码
	HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_RESET);

    for(index = 0; index < 32; index++)
    {
        ir_38khz(560);
        HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_SET);

        if(((data >> index) & 0x00000001) == 0)
        {
            delay_us(550);
        }

        else
        {
            delay_us(1680);
        }

        HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_RESET);
    }

    ir_38khz(560);
    HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_SET);

    //连发码
    for(index = 0; index < times; index++)
    {
        HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_RESET);
        ir_38khz(9000);
        HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_SET);
        ir_38khz(2250);
        HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_RESET);
        ir_38khz(560);
        HAL_GPIO_WritePin(IR_SEND_PORT, IR_SEND_PIN, GPIO_PIN_SET);
        delay_us(48970);
		delay_us(48970);
    }
}



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
