/*
*******************************************************************************************************
*
* 文件名称 : ir_receive.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 定时器输入捕获红外接收
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "ir_receive.h"


/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/

/* 由于时钟不固定,所以定时器的分频系数根据系统时钟定 */
extern uint32_t SystemCoreClock;

TIM_HandleTypeDef htim3;

/***********************************************
//遥控器接收状态
//[7]:收到了引导码标志
//[6]:得到了一个按键的所有信息
//[5]:保留	
//[4]:标记上升沿是否已经被捕获								   
//[3:0]:溢出计时器
**********************************************/
uint8_t  RmtSta=0;	  	  
uint16_t Dval;		//下降沿时计数器的值
uint32_t RmtRec=0;	//红外接收到的数据	   		    
uint8_t  RmtCnt=0;	//按键按下的次数

/* 函数声明 ---------------------------------------------------------*/


/**
  * @brief 红外接收对于的定时器初始化
  * @param None
  * @retval	None
  */
void ir_receive_init(void)
{
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	TIM_IC_InitTypeDef sConfigIC = {0};

	htim3.Instance = TIM3;
	htim3.Init.Prescaler = (SystemCoreClock/2)/1000000;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 10000;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	if (HAL_TIM_IC_Init(&htim3) != HAL_OK)
	{
		Error_Handler();
	}
	sConfigIC.ICPolarity = TIM_ICPOLARITY_FALLING;
	sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
	sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
	sConfigIC.ICFilter = 0x03;
	if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_4) != HAL_OK)
	{
		Error_Handler();
	}
	
	HAL_TIM_IC_Start_IT(&htim3,TIM_CHANNEL_4);   //开启TIM3的捕获通道4，并且开启捕获中断
	__HAL_TIM_ENABLE_IT(&htim3,TIM_IT_UPDATE);   //使能更新中断

}

/**
  * @brief 开启接收
  * @param None
  * @retval	None
  */
void ir_receive_start_scan(void)
{
	HAL_NVIC_EnableIRQ(TIM3_IRQn);
}
/**
  * @brief 停止接收
  * @param None
  * @retval	None
  */
void ir_receive_stop_scan(void)
{
	HAL_NVIC_DisableIRQ(TIM3_IRQn);
}

/**
  * @brief 处理红外键盘接收
  * @param None
  * @retval	0-没有任何按键按下 other-键值
  */
uint8_t ir_receive_scan(void)
{        
	uint8_t sta=0;       
	uint8_t t1,t2;  
	if(RmtSta&(1<<6))//得到一个按键的所有信息了
	{ 
	    t1=RmtRec>>24;			//得到地址码
	    t2=(RmtRec>>16)&0xff;	//得到地址反码 
 	    if((t1==(uint8_t)~t2)&&t1==REMOTE_ID)//检验遥控识别码(ID)及地址 
	    { 
	        t1=RmtRec>>8;
	        t2=RmtRec; 	
	        if(t1==(uint8_t)~t2)sta=t1;//键值正确	 
		}   
		if((sta==0)||((RmtSta&0X80)==0))//按键数据错误/遥控已经没有按下了
		{
		 	RmtSta&=~(1<<6);//清除接收到有效按键标识
			RmtCnt=0;		//清除按键次数计数器
		}
	}  
	return sta;
	
}




/**
  * @brief 定时器更新中断(计数溢出)中断处理回调函数
  * @param htim-定时器句柄
  * @retval	None
  * @note 该函数在HAL_TIM_IRQHandler中会被调用
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance==TIM3)
	{
		if(RmtSta&0x80)//上次有数据被接收到了
		{	
			RmtSta&=~0X10;						//取消上升沿已经被捕获标记
			if((RmtSta&0X0F)==0X00)
				RmtSta|=1<<6;//标记已经完成一次按键的键值信息采集
			if((RmtSta&0X0F)<14)
				RmtSta++;
			else
			{
				RmtSta&=~(1<<7);//清空引导标识
				RmtSta&=0XF0;	//清空计数器	
			}						 	   	
		}	
	}
}





/**
  * @brief 定时器输入捕获中断处理回调函数
  * @param htim-定时器句柄
  * @retval	None
  * @note 该函数在HAL_TIM_IRQHandler中会被调用
  */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)//捕获中断发生时执行
{

	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1))		//上升沿捕获
	{
		TIM_RESET_CAPTUREPOLARITY(&htim3,TIM_CHANNEL_4);   //一定要先清除原来的设置！！
		TIM_SET_CAPTUREPOLARITY(&htim3,TIM_CHANNEL_4,TIM_ICPOLARITY_FALLING);//CC1P=1 设置为下降沿捕获
		__HAL_TIM_SET_COUNTER(&htim3,0);  //清空定时器值   	  
		RmtSta|=0X10;					//标记上升沿已经被捕获
	}
	else		//下降沿捕获
	{
		Dval=HAL_TIM_ReadCapturedValue(&htim3,TIM_CHANNEL_4);//读取CCR1也可以清CC1IF标志位
		TIM_RESET_CAPTUREPOLARITY(&htim3,TIM_CHANNEL_4);   //一定要先清除原来的设置！！
		TIM_SET_CAPTUREPOLARITY(&htim3,TIM_CHANNEL_4,TIM_ICPOLARITY_RISING);//配置TIM5通道1上升沿捕获
		if(RmtSta & 0x10)		//完成一次高电平捕获
		{
			if(RmtSta & 0x80)		//接收到了前导码
			{
				if(Dval>300&&Dval<800)			//560为标准值,560us
				{
					RmtRec<<=1;	//左移一位.
					RmtRec|=0;	//接收到0	   
				}
				else if(Dval>1400&&Dval<1800)	//1680为标准值,1680us
				{
					RmtRec<<=1;	//左移一位.
					RmtRec|=1;	//接收到1
				}
				else if(Dval>2200&&Dval<2600)	//得到按键键值增加的信息 2500为标准值2.5ms
				{
					RmtCnt++; 		//按键次数增加1次
					RmtSta&=0XF0;	//清空计时器		
				}
			}
			else if(Dval > 4200 && Dval < 4700)
			{
					RmtSta|=1<<7;	//标记成功接收到了引导码
					RmtCnt=0;		//清除按键次数计数器
			}
		}
		RmtSta&=~(1<<4);
	}
}


/**
  * @brief TIM底层初始化函数
  * @param None
  * @retval	None
  * @note 该函数会被HAL_xxx_Init()调用,主要完成时钟使能,IO配置
  */
void HAL_TIM_IC_MspInit(TIM_HandleTypeDef* tim_icHandle)
{

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(tim_icHandle->Instance==TIM3)
	{

		/* TIM3 clock enable */
		__HAL_RCC_TIM3_CLK_ENABLE();

		__HAL_RCC_GPIOB_CLK_ENABLE();
		/**TIM3 GPIO Configuration    
		PB1     ------> TIM3_CH4 
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_1;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		/* TIM3 interrupt Init */
		HAL_NVIC_SetPriority(TIM3_IRQn, 15, 0);
		//HAL_NVIC_EnableIRQ(TIM3_IRQn);

	}
}


/**
  * @brief TIM底层去初始化函数
  * @param None
  * @retval	None
  * @note 该函数会被HAL_xxx_DeInit()调用,主要完成时钟禁能,IO配置清除
  */
void HAL_TIM_IC_MspDeInit(TIM_HandleTypeDef* tim_icHandle)
{

	if(tim_icHandle->Instance==TIM3)
	{

		/* Peripheral clock disable */
		__HAL_RCC_TIM3_CLK_DISABLE();

		/**TIM3 GPIO Configuration    
		PB1     ------> TIM3_CH4 
		*/
		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_1);

		/* TIM3 interrupt Deinit */
		HAL_NVIC_DisableIRQ(TIM3_IRQn);

	}
} 







/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
