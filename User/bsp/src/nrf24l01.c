/*
*******************************************************************************************************
*
* 文件名称 : nrf24l01.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : nrf24l01驱动文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "nrf24l01.h"


/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/

const uint8_t sendtest[32] = {0x01,0x23,0x99,0x05,0x09,0xfe,0xad,0xed,
								0x01,0x23,0x99,0x05,0x09,0xfe,0xad,0x01,
								0x01,0x23,0x99,0x05,0x09,0xfe,0xad,0xed,
								0x01,0x23,0x99,0x05,0x09,0xfe,0xad,0xed};

const uint8_t TX_ADDRESS[TX_ADR_WIDTH]={0x12,0x15,0x09,0x45,0x10};
const uint8_t RX_ADDRESS[RX_ADR_WIDTH]={0x12,0x15,0x09,0x45,0x10}; 


/* 函数声明 ---------------------------------------------------------*/
static void nrf_gpio_config(void);
static void nrf_spi_config(void);
static void nrf_ce(uint8_t enable);
static void nrf_cs(uint8_t select);
static uint8_t nrf_irq(void);
static uint8_t nrf_spi_send_receive_byte(uint8_t txdata);



/**
  * @brief NRF24L01初始化
  * @param None
  * @retval	None
  * @note 
  */
void nrf_init(void)
{
	nrf_gpio_config();
	nrf_spi_config();

	nrf_spi_send_receive_byte(0xff);

	nrf_cs(1);
	nrf_ce(0);
}

/**
  * @brief 检测24L01是否存在
  * @param None
  * @retval	0-成功  1-失败
  * @note 
  */
uint8_t nrf_check(void)
{
	uint8_t buf[5]={0XA5,0XA5,0XA5,0XA5,0XA5};
	uint8_t i;
	nrf_write_buf(NRF_WRITE_REG+TX_ADDR,buf,5);//写入5个字节的地址.	
	nrf_read_buf(TX_ADDR,buf,5); //读出写入的地址  
	for(i=0;i<5;i++)
	{
		if(buf[i]!=0XA5)
		{
			break;	
		}
	}		
	if(i!=5)
	{
		/* 检测24L01错误 */
		return 1;
	}
	return 0;		 //检测到24L01
}	 	 

/**
  * @brief SPI写NRF24L01的寄存器
  * @param reg-指定寄存器地址
  * @param value-写入的值
  * @retval	返回状态值
  * @note 
  */
uint8_t nrf_write_reg(uint8_t reg,uint8_t value)
{
	uint8_t status;	
	nrf_cs(0);                 //使能SPI传输
	status =nrf_spi_send_receive_byte(reg);//发送寄存器号 
	nrf_spi_send_receive_byte(value);      //写入寄存器的值
	nrf_cs(1);                 //禁止SPI传输	   
	return(status);       		    //返回状态值
}
//读取SPI寄存器值
//reg:要读的寄存器

/**
  * @brief SPI读NRF24L01的寄存器
  * @param reg-要读的寄存器地址
  * @retval	读取到的值
  * @note 
  */
uint8_t nrf_read_reg(uint8_t reg)
{
	uint8_t reg_val;	    
	nrf_cs(0);             //使能SPI传输		
	nrf_spi_send_receive_byte(reg);    //发送寄存器号
	reg_val=nrf_spi_send_receive_byte(0XFF);//读取寄存器内容
	nrf_cs(1);             //禁止SPI传输		    
	return(reg_val);            //返回状态值
}	


/**
  * @brief SPI读NRF24L01指定长度的数据
  * @param reg-要读的寄存器地址
  * @param buf-读取到的数据的存储地址
  * @param len-数据长度
  * @retval	寄存器状态值
  * @note 
  */
uint8_t nrf_read_buf(uint8_t reg,uint8_t *buf,uint8_t len)
{
	uint8_t status,u8_ctr;	       
	nrf_cs(0);            //使能SPI传输
	status=nrf_spi_send_receive_byte(reg);//发送寄存器值(位置),并读取状态值   	   
	for(u8_ctr=0;u8_ctr<len;u8_ctr++)
	{
		/* 读出数据 */
		buf[u8_ctr]=nrf_spi_send_receive_byte(0XFF);
	}
	nrf_cs(1);            //关闭SPI传输
	return status;             //返回读到的状态值
}

/**
  * @brief SPI写NRF24L01指定长度的数据
  * @param reg-要写的寄存器地址
  * @param buf-待写入的数据的存储地址
  * @param len-数据长度
  * @retval	寄存器状态值
  * @note 
  */
uint8_t nrf_write_buf(uint8_t reg, uint8_t *buf, uint8_t len)
{
	uint8_t status,u8_ctr;	    
	nrf_cs(0);             //使能SPI传输
	status = nrf_spi_send_receive_byte(reg);//发送寄存器值(位置),并读取状态值
	for(u8_ctr=0; u8_ctr<len; u8_ctr++)
	{
		/* 写入数据	 */
		nrf_spi_send_receive_byte(*buf++);
	}		
	nrf_cs(1);             //关闭SPI传输
	return status;              //返回读到的状态值
}				   


/**
  * @brief 启动NRF24L01发送一次数据到空中
  * @param txbuf-待发送的数据的存储地址
  * @retval	发送完成状况
  * @note 
  */
uint8_t nrf_tx_packet(uint8_t *txbuf)
{
	uint32_t time_out = 0;
	uint8_t sta;  
	nrf_ce(0);
	nrf_write_buf(WR_TX_PLOAD,txbuf,TX_PLOAD_WIDTH);//写数据到TX BUF  32个字节
	nrf_ce(1);                         //启动发送	   
	while((nrf_irq()!=0) && ((time_out++) < 0X02FFFFFF));    //等待发送完成,超时时间约1S
	sta=nrf_read_reg(STATUS);          //读取状态寄存器的值	   
	nrf_write_reg(NRF_WRITE_REG+STATUS,sta); //清除TX_DS或MAX_RT中断标志
	if(sta&MAX_TX)                          //达到最大重发次数
	{
		nrf_write_reg(FLUSH_TX,0xff);  //清除TX FIFO寄存器 
		return MAX_TX; 
	}
	if(sta&TX_OK)                           //发送完成
	{
		return TX_OK;
	}
	return 0xff;//其他原因发送失败
}


/**
  * @brief 启动NRF24L01接收一次空中数据
  * @param rxbuf-待接收的数据的存储地址
  * @retval	0-收到数据 1-未收到数据
  * @note 
  */
uint8_t nrf_rx_packet(uint8_t *rxbuf)
{
	uint8_t sta;		    							   
	sta=nrf_read_reg(STATUS);          //读取状态寄存器的值    	 
	nrf_write_reg(NRF_WRITE_REG+STATUS,sta); //清除TX_DS或MAX_RT中断标志
	if(sta&RX_OK)//接收到数据
	{
		nrf_read_buf(RD_RX_PLOAD,rxbuf,RX_PLOAD_WIDTH);//读取数据
		nrf_write_reg(FLUSH_RX,0xff);  //清除RX FIFO寄存器 
		return 0; 
	}	   
	return 1;//没收到任何数据
}					    


/**
  * @brief 配置NRF24L01为发送模式
  * @param None
  * @retval	None
  * @note 
  */
void nrf_tx_mode(void)
{														 
	nrf_ce(0);	    
	nrf_write_buf(NRF_WRITE_REG+TX_ADDR,(uint8_t*)TX_ADDRESS,TX_ADR_WIDTH);//写TX节点地址 
	nrf_write_buf(NRF_WRITE_REG+RX_ADDR_P0,(uint8_t*)RX_ADDRESS,RX_ADR_WIDTH); //设置TX节点地址,主要为了使能ACK	  

	nrf_write_reg(NRF_WRITE_REG+EN_AA,0x01);     //使能通道0的自动应答    
	nrf_write_reg(NRF_WRITE_REG+EN_RXADDR,0x01); //使能通道0的接收地址  
	nrf_write_reg(NRF_WRITE_REG+SETUP_RETR,0x1a);//设置自动重发间隔时间:500us + 86us;最大自动重发次数:10次
	nrf_write_reg(NRF_WRITE_REG+RF_CH,60);       //设置RF通道为100
	nrf_write_reg(NRF_WRITE_REG+RF_SETUP,0x07);  //设置TX发射参数,0db增益,2Mbps,低噪声增益开启   
	nrf_write_reg(NRF_WRITE_REG+CONFIG,0x0e);    //配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式,开启所有中断
	nrf_ce(1);//CE为高,10us后启动发送
}



/**
  * @brief 配置NRF24L01为接收模式
  * @param None
  * @retval	None
  * @note 
  */
void nrf_rx_mode(void)
{
	nrf_ce(0);	    
	nrf_write_buf(NRF_WRITE_REG+RX_ADDR_P0,(uint8_t*)RX_ADDRESS,RX_ADR_WIDTH);//写RX节点地址
	  
	nrf_write_reg(NRF_WRITE_REG+EN_AA,0x01);    //使能通道0的自动应答    
	nrf_write_reg(NRF_WRITE_REG+EN_RXADDR,0x01);//使能通道0的接收地址  	 
	nrf_write_reg(NRF_WRITE_REG+RF_CH,60);	     //设置RF通信频率		  
	nrf_write_reg(NRF_WRITE_REG+RX_PW_P0,RX_PLOAD_WIDTH);//选择通道0的有效数据宽度 	    
	nrf_write_reg(NRF_WRITE_REG+RF_SETUP,0x07);//设置TX发射参数,0db增益,2Mbps,低噪声增益开启
		//0x0f=2M		0x07=1M    0x26=250k
	nrf_write_reg(NRF_WRITE_REG+CONFIG, 0x0f);//配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式 
	nrf_ce(1);//CE为高,10us后启动发送
}	

















/*
**********************************************************************
*以下是驱动接口函数
***********************************************************************
*/


/**
  * @brief 配置NRF模块的GPIO和NRF电源
  * @param None
  * @retval	None
  * @note 
  */
static void nrf_gpio_config(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();

	
	GPIO_InitStruct.Pin = NRF_CE_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(NRF_CE_GPIO_PORT, &GPIO_InitStruct);
	
	
	GPIO_InitStruct.Pin = NRF_CS_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(NRF_CS_GPIO_PORT, &GPIO_InitStruct);


	GPIO_InitStruct.Pin = NRF_IRQ_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(NRF_IRQ_GPIO_PORT, &GPIO_InitStruct);
	
	
	GPIO_InitStruct.Pin = NRF_PWR_EN_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(NRF_PWR_EN_GPIO_PORT, &GPIO_InitStruct);

	/* 打开NRF的电源 */
	HAL_GPIO_WritePin(NRF_PWR_EN_GPIO_PORT,NRF_PWR_EN_PIN,GPIO_PIN_SET);
	
}


/**
  * @brief 配置NRF模块对应的SPI
  * @param None
  * @retval	None
  * @note 
  */
static void nrf_spi_config(void)
{
	spi2_init();
}

/**
  * @brief NRF模块的CE引脚输出对应的电平,模块的使能
  * @param enable-输出的电平
  * @retval	None
  * @note 
  */
static void nrf_ce(uint8_t enable)
{
	HAL_GPIO_WritePin(NRF_CE_GPIO_PORT, NRF_CE_PIN, (GPIO_PinState)enable);
}

/**
  * @brief NRF模块的CS引脚输出对应的电平,模块的SPI片选
  * @param select-片选 0-选择 1-不选
  * @retval	None
  * @note 
  */
static void nrf_cs(uint8_t select)
{
	HAL_GPIO_WritePin(NRF_CS_GPIO_PORT, NRF_CS_PIN, (GPIO_PinState)select);
}

/**
  * @brief 读取NRF模块的IRQ引脚的电平,模块的中断
  * @param None
  * @retval	读取到的电平
  * @note 
  */
static uint8_t nrf_irq(void)
{
	return(HAL_GPIO_ReadPin(NRF_IRQ_GPIO_PORT,NRF_IRQ_PIN));
}


/**
* @brief 启用NRF对应的SPI发送接收一次数据,仅操作NRF模块,并非将数据发送到空中
  * @param None
  * @retval	读取到的数据
  * @note 
  */
static uint8_t nrf_spi_send_receive_byte(uint8_t txdata)
{
	return spi2_send_receive_byte(txdata);
}



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
