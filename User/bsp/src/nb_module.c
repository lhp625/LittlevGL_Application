/*
*******************************************************************************************************
*
* 文件名称 : nb_module.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : NB模块驱动文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "nb_module.h"



/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/

uint8_t nb_rx_buf[NB_RX_BUF_MAX_LENGTH];
uint8_t nb_rx_buf_temp[NB_RX_BUF_MAX_LENGTH];
uint8_t nb_cmd_buf[100];
uint16_t nb_rx_cnt = 0, nb_rx_cntPre = 0;
uint16_t nb_rx_cnt_temp = 0;


/* 函数声明 ---------------------------------------------------------*/


/**
  * @brief NB模块对应的串口发送指定长度字符串
  * @param str-字符串指针
  * @param len-字符串长度
  * @retval	None
  */
void nb_uart_send_string(unsigned char *str,uint16_t len)
{
	usart2_send_string(str,len);
}




/**
  * @brief NB模块复位
  * @param None
  * @retval	None
  */
void nb_rst(void)
{

	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,1);
	delay_ms(100);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,0);
	
}




/**
  * @brief 清空缓冲区
  * @param None
  * @retval	None
  */
void nb_buf_clear(void)
{
	memset(nb_rx_buf, 0, NB_RX_BUF_MAX_LENGTH);
	nb_rx_cnt = 0;
}



/**
  * @brief 清空缓冲区
  * @param None
  * @retval	None
  */
void nb_buf_temp_clear(void)
{
	memset(nb_rx_buf_temp, 0, NB_RX_BUF_MAX_LENGTH);
	nb_rx_cnt_temp = 0;
}
/**
  * @brief 获取缓冲区长度
  * @param None
  * @retval	None
  */
uint16_t nb_buf_temp_get_len(void)
{
	return nb_rx_cnt_temp;
}



/**
  * @brief 等待接收完成
  * @param None
  * @retval	REV_OK-接收完成		REV_WAIT-接收超时未完成
  * @note 循环调用检测是否接收完成
  */
uint8_t nb_wait_recive(void)
{

	if(nb_rx_cnt == 0) 							//如果接收计数为0 则说明没有处于接收数据中，所以直接跳出，结束函数
		return REV_WAIT;
		
	if(nb_rx_cnt == nb_rx_cntPre)				//如果上一次的值和这次相同，则说明接收完毕
	{
		nb_rx_cnt = 0;							//清0接收计数
			
		return REV_OK;								//返回接收完成标志
	}
		
	nb_rx_cntPre = nb_rx_cnt;					//置为相同
	
	return REV_WAIT;								//返回接收未完成标志

}



/**
  * @brief 发送命令
  * @param cmd-命令字符串
  * @param res-需要检查的返回指令
  * @retval	0-成功	1-失败
  */
uint8_t nb_send_cmd(unsigned char *cmd, unsigned char *res)
{
	
	uint8_t time_out = 200;

	nb_buf_clear();									//清空缓存
	
	nb_uart_send_string(cmd, strlen((const char *)cmd));
	
	while(time_out--)
	{
		if(nb_wait_recive() == REV_OK)							//如果收到数据
		{
			if(strstr((const char *)nb_rx_buf, (const char *)res) != NULL)		//如果检索到关键词
			{
				//nb_buf_clear();									//清空缓存
				
				return 0;
			}
		}
		
		delay_ms(10);
	}
	
	return 1;

}




/**
  * @brief 获取平台返回的数据
  * @param cmd-命令字符串
  * @param res-需要检查的返回指令
  * @retval	0-成功	1-失败
  */
uint8_t nb_get_message(uint16_t time_out)
{
	
	char *ptrIPD = NULL;
	
	memset((char*)nb_cmd_buf,0,sizeof(nb_cmd_buf));
	
	do
	{
		if(nb_wait_recive() == REV_OK)
		//if(1)
		{
			ptrIPD = strstr((char *)nb_rx_buf, "+NNMI:");
			if(ptrIPD == NULL)
			{
			}
			else
			{
				if(strstr(ptrIPD, "\n") == NULL)
					return 1;
				memcpy((char*)nb_cmd_buf,strstr(ptrIPD, ",")+1,50);
				return 0;
			}
		}
		delay_ms(1);
	}
	while(time_out--);
	
	
	return 1;
	
	
}




/**
  * @brief NB模块初始化
  * @param None
  * @retval	None
  */
uint8_t nb_init(void)
{
	uint8_t error = 0;

	/* nb_rst--pb3 */
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	
	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	/*Configure GPIO pin : PtPin */
	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	nb_rst();
	
	delay_ms(1000);
//	nb_send_cmd((uint8_t*)"AT+CFUN=0\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+NBAND?\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+NBAND=5\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+NCDP=180.101.147.115,5683\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+NRB\r\n",(uint8_t*)"OK");
//	delay_ms(3000);
//	nb_send_cmd((uint8_t*)"AT+CSCON=1\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+CEREG=4\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+NNMI=1\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+CGATT=1\r\n",(uint8_t*)"OK");

	while(nb_send_cmd((uint8_t*)"AT\r\n", (uint8_t*)"OK"))
	{
		if(error>=10)
			return 1;
		delay_ms(500);
		error++;
	}
	error=0;
	
//	nb_send_cmd((uint8_t*)"AT+CFUN=0\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+NBAND?\r\n",(uint8_t*)"OK");
//	//nb_send_cmd((uint8_t*)"AT+NBAND=8\r\n",(uint8_t*)"OK");
//	//nb_send_cmd((uint8_t*)"AT+NCDP=180.101.147.115,5683\r\n",(uint8_t*)"OK");
//	nb_send_cmd((uint8_t*)"AT+NRB\r\n",(uint8_t*)"OK");
//	delay_ms(3000);
	nb_send_cmd((uint8_t*)"AT+CSCON=1\r\n",(uint8_t*)"OK");
	nb_send_cmd((uint8_t*)"AT+CEREG=4\r\n",(uint8_t*)"OK");
	nb_send_cmd((uint8_t*)"AT+NNMI=1\r\n",(uint8_t*)"OK");
	nb_send_cmd((uint8_t*)"AT+CFUN=1\r\n",(uint8_t*)"OK");
	nb_send_cmd((uint8_t*)"AT+CGATT=1\r\n",(uint8_t*)"OK");
	nb_send_cmd((uint8_t*)"ATE0\r\n",(uint8_t*)"OK");

	//usart3_init();
	return 0;
}



/**
  * @brief  USART2接收一字节数据
  * @param  c-接收的数据
  * @retval None
  * @note 该函数会被串口中断函数调用
  */
void usart2_put_char(uint8_t c)
{
	/* USART2接收一字节 */
	if(nb_rx_cnt >= NB_RX_BUF_MAX_LENGTH)
		nb_rx_cnt = 0;
	if(nb_rx_cnt_temp >= NB_RX_BUF_MAX_LENGTH)
		nb_rx_cnt_temp = 0;
	nb_rx_buf[nb_rx_cnt++] = c;
	nb_rx_buf_temp[nb_rx_cnt_temp++] = c;
	//USART1->DR = c;
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
