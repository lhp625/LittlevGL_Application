/*
*******************************************************************************************************
*
* 文件名称 : beep.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 蜂鸣器驱动文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "beep.h"


/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/


/* 函数声明 ---------------------------------------------------------*/



/**
  * @brief 蜂鸣器初始化
  * @param None
  * @retval	None
  * @note 4KHz的PWM
  * @note fre = clk / ((Prescaler+1)*(Period+1)) Hz
  */
void beep_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	/* TIM5 clock enable */
	__HAL_RCC_TIM5_CLK_ENABLE();

	__HAL_RCC_GPIOA_CLK_ENABLE();
	/**TIM5 GPIO Configuration    
	PA2     ------> TIM5_CH3 
	*/
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	//使能定时器5的时钟
	RCC->APB1ENR |= 1<<3;
	//设置预分频值(500K的定时器频率)
	TIM5->PSC = (SystemCoreClock/2)/500000-1;
	//设置自动重装载值(500K频率 125的自动重装载值 PWM为4K)
	TIM5->ARR = 125-1;
	
	//下面两步没有的话预分频的值必须等到产生一次更新事件后才会被采用
	//重新初始化定时器计数器并生成寄存器更新事件,确保预分频值被采用
	TIM5->EGR |= (1<<0);
	//清除更新标志位,该位在发生更新事件时通过硬件置 1,但需要通过软件清零
	TIM5->SR &= ~(1<<0);
	
	//TIM5的CH3配置为PWM
	TIM5->CCMR2 |= 6 << 4;
	//输出比较2预装载使能
	TIM5->CCMR2 |= 1<<3;
	
	//比较 3 输出使能
	TIM5->CCER |= 1<<8;
	//CH3输出极性设为高电平有效
	TIM5->CCER &= ~(1<<9);
	
	//设置占空比
	TIM5->CCR3 = 0;
	
	//自动重载预装载使能
	TIM5->CR1 |= 1<<7;
	//关闭定时器
	TIM5->CR1 &= ~(1<<0);
}


/**
  * @brief 蜂鸣器开关
  * @param en 1-打开蜂鸣器 0-关闭蜂鸣器
  * @retval	None
  */
void beep_enable(uint8_t en)
{
	if(en)
		TIM5->CR1 |= 1<<0;
	else
		TIM5->CR1 &= ~(1<<0);
}

/**
  * @brief 蜂鸣器PWM占空比调节
  * @param sound-百分比0-100
  * @retval	None
  */
void beep_adjust(uint8_t sound)
{
	TIM5->CCR3 = (uint32_t)((float)sound/100*(TIM5->ARR));
}



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
