/*
*******************************************************************************************************
*
* 文件名称 : adc.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : ADC驱动文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "adc.h"


/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/
static __IO uint16_t bat_adc_value = 0;
static __IO uint16_t tempsensor_adc_value = 0;
static __IO float tempsensor_value = 0;
static __IO float bat_voltage = 0;

ADC_HandleTypeDef hadc3;
ADC_HandleTypeDef hadc1;
/* 函数声明 ---------------------------------------------------------*/


/**
  * @brief CPU温度传感器对应的ADC1初始化
  * @param None
  * @retval	None
  */
void adc_tempsensor_init(void)
{
	ADC_ChannelConfTypeDef sConfig = {0};
	
	/** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
	*/
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	hadc1.Init.ScanConvMode = DISABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 1;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&hadc1) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
	*/
	sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

}

/**
  * @brief ADC3初始化
  * @param None
  * @retval	None
  */
void adc_light_init(void)
{
	ADC_ChannelConfTypeDef sConfig = {0};
	
	/** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
	*/
	hadc3.Instance = ADC3;
	hadc3.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	hadc3.Init.Resolution = ADC_RESOLUTION_12B;
	hadc3.Init.ScanConvMode = DISABLE;
	hadc3.Init.ContinuousConvMode = DISABLE;
	hadc3.Init.DiscontinuousConvMode = DISABLE;
	hadc3.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc3.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc3.Init.NbrOfConversion = 1;
	hadc3.Init.DMAContinuousRequests = DISABLE;
	hadc3.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&hadc3) != HAL_OK)
	{
		Error_Handler();
	}
	/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
	*/
	sConfig.Channel = ADC_CHANNEL_6;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_56CYCLES;
	if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	
	sConfig.Channel = ADC_CHANNEL_11;;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;;
	if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	

}


/**
  * @brief 获取光敏电阻的ADC值
  * @param None
  * @retval	None
  */
uint16_t adc_light_get(void)
{
	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Channel = ADC_CHANNEL_6;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_56CYCLES;
	
	if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	HAL_ADC_Start(&hadc3);
	HAL_ADC_PollForConversion(&hadc3,10);
	return HAL_ADC_GetValue(&hadc3);

}

/**
  * @brief 获取电池对应的ADC通道的值(未滤波未转换)
  * @param None
  * @retval	获取到的12位ADC转换的值
  */
uint16_t adc_bat_get(void)
{
	uint8_t i;
	uint32_t temp = 0;
	ADC_ChannelConfTypeDef sConfig = {0};
	for(i=0;i<10;i++)
	{
		sConfig.Channel = ADC_CHANNEL_11;
		sConfig.Rank = 1;
		sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
		HAL_ADC_ConfigChannel(&hadc3, &sConfig);
		HAL_ADC_Start(&hadc3);
		HAL_ADC_PollForConversion(&hadc3,10);
		bat_adc_value = HAL_ADC_GetValue(&hadc3);
		temp += bat_adc_value;
	}
	bat_adc_value = temp / 10;
	bat_voltage=(float)bat_adc_value*(3.3/4096);
	//printf("ADC值:%d\r\n",bat_adc_value);
	return bat_adc_value;
}



/**
  * @brief 获取CPU温度传感器ADC通道的值(未滤波未转换)
  * @param None
  * @retval	获取到的12位ADC转换的值
  */
uint16_t adc_tempsensor_get(void)
{
	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
	
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1,10);
	tempsensor_adc_value = HAL_ADC_GetValue(&hadc1);
	tempsensor_value=(float)tempsensor_adc_value*(3.3/4096);	
	tempsensor_value=(tempsensor_value-0.76)/0.0025 + 25; //转换为温度值 
	return tempsensor_adc_value;
}

/**
  * @brief ADC底层初始化函数
  * @param None
  * @retval	None
  * @note 该函数会被HAL_xxx_Init()调用,主要完成时钟使能,IO配置
  */
void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(adcHandle->Instance==ADC3)
	{

		/* ADC3 clock enable */
		__HAL_RCC_ADC3_CLK_ENABLE();

		__HAL_RCC_GPIOF_CLK_ENABLE();
		__HAL_RCC_GPIOC_CLK_ENABLE();
		/**ADC3 GPIO Configuration    
		PF8     ------> ADC3_IN6
		PF9     ------> ADC3_IN7 
		PC1     ------> ADC3_IN11
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_1;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
		
	}
	else if(adcHandle->Instance==ADC1)
	{

		/* ADC1 clock enable */
		__HAL_RCC_ADC1_CLK_ENABLE();

	}
}


/**
  * @brief ADC底层去初始化函数
  * @param None
  * @retval	None
  * @note 该函数会被HAL_xxx_DeInit()调用,主要完成时钟禁能,IO配置清除
  */
void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

	if(adcHandle->Instance==ADC3)
	{

		/* Peripheral clock disable */
		__HAL_RCC_ADC3_CLK_DISABLE();

		/**ADC3 GPIO Configuration    
		PF8     ------> ADC3_IN6
		PF9     ------> ADC3_IN7 
		PC1     ------> ADC3_IN11
		*/
		HAL_GPIO_DeInit(GPIOF, GPIO_PIN_8|GPIO_PIN_9);
		
	}
	else if(adcHandle->Instance==ADC1)
	{

		/* Peripheral clock disable */
		__HAL_RCC_ADC1_CLK_DISABLE();

	}
} 







/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
