/*
*******************************************************************************************************
*
* 文件名称 : battery.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 电池电源管理文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "battery.h"


/* 宏定义 -----------------------------------------------------------*/
static BAT_TypeDef battery = {
	.adc = 0,           //电池ADC值
	.voltage = 0,       //电池电压值
	.max = 4.12,        //电池电压上限值
	.min = 3.3,        //电池电压下限值
	.ele = 0,           //电量
	.chrg = 0,          //充电标识
	.stdby = 0,         //充满标识
};

/* 变量 -------------------------------------------------------------*/


/* 函数声明 ---------------------------------------------------------*/


/**
  * @brief 初始化电池电源管理
  * @param None
  * @retval	None
  */
void bat_manage_init(void)
{
	/* 定义一个GPIO初始化结构体 */
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* 充电检测端口时钟使能 */
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	
	/* 正在充电的GPIO */
	GPIO_InitStruct.Pin = BAT_CHRG_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(BAT_CHRG_PORT, &GPIO_InitStruct);

	/* 电量充满的GPIO */
	GPIO_InitStruct.Pin = BAT_STDBY_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(BAT_STDBY_PORT, &GPIO_InitStruct);
	

	
	//adc_bat_init();          //电压检测ADC初始化
	bat_manage_ele_get();
}




/**
  * @brief 获取电池电量
  * @param None
  * @retval	电量百分比
  */
uint8_t bat_manage_ele_get(void)
{
	float adc_value;
	battery.stdby = !HAL_GPIO_ReadPin(BAT_STDBY_PORT,BAT_STDBY_PIN);
	battery.chrg = !HAL_GPIO_ReadPin(BAT_CHRG_PORT,BAT_CHRG_PIN);
	battery.adc = (float)adc_bat_get();
	bat_manage_filter(battery.adc,&adc_value,6);
	battery.voltage = adc_value * (3.3 / 4096) * 2;
	if(battery.voltage >= battery.max)
		battery.voltage = battery.max;
	else if(battery.voltage <= battery.min)
		battery.voltage = battery.min;
	battery.ele = (battery.voltage-battery.min) / (battery.max-battery.min) * 100;
	return battery.ele;
}


/**
  * @brief 获取电池电源管理信息
  * @param current_bat-存储当前电池信息的结构体
  * @retval	None
  */
void bat_manage_current(BAT_TypeDef *current_bat)
{
	current_bat->adc = battery.adc;
	current_bat->voltage = battery.voltage;
	current_bat->ele = battery.ele;
	current_bat->stdby = battery.stdby;
	current_bat->chrg = battery.chrg;
	current_bat->max = battery.max;
	current_bat->min = battery.min;
}



/**
  * @brief 获取电池电压
  * @param None
  * @retval	电压值
  * @note 电池电压值 = ADC值 * 2
  */
float bat_manage_voltage_get(void)
{
	float adc_value;
	battery.stdby = !HAL_GPIO_ReadPin(BAT_STDBY_PORT,BAT_STDBY_PIN);
	battery.chrg = !HAL_GPIO_ReadPin(BAT_CHRG_PORT,BAT_CHRG_PIN);
	battery.adc = (float)adc_bat_get();
	bat_manage_filter(battery.adc,&adc_value,6);
	battery.voltage = adc_value * (3.3 / 4096) * 2;
	if(battery.voltage >= battery.max)
		battery.voltage = battery.max;
	else if(battery.voltage <= battery.min)
		battery.voltage = battery.min;
	battery.ele = (battery.voltage-battery.min) / (battery.max-battery.min) * 100;
	return battery.voltage;
}



/**
  * @brief 滑动窗口电池ADC滤波
  * @param data-要滤波数据
  * @param filt-滤波后数据存储地址
  * @retval	None
  */
static uint8_t flag = 1;
void bat_manage_filter(float data,float *filt_data,uint8_t len)
{
	static float buf[20];
	static uint8_t count =0,flag0 = 1;
	float temp=0;
	uint8_t i;
	buf[count] = data;
	count++;
	if((count<len) && flag0)
	{
		*filt_data = data;
		return;
	}
	else
		flag0 = 0;
	
	for(i=0;i<len;i++)
	{
		temp += buf[i];
	}
	if(count>=len) count = 0;
	 *filt_data = temp/len;
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

