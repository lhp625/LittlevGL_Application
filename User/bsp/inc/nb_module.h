/*
*******************************************************************************************************
*
* 文件名称 : nb_module.h
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : NB模块驱动文件
* 
*******************************************************************************************************
*/

#ifndef _NB_MODULE_H
#define _NB_MODULE_H

/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"



/* 宏定义 -----------------------------------------------------------*/

#define NB_RX_BUF_MAX_LENGTH 500


#define REV_OK		0	//接收完成标志
#define REV_WAIT	1	//接收未完成标志


/* 全局变量 ----------------------------------------------------------*/

extern uint8_t nb_rx_buf[NB_RX_BUF_MAX_LENGTH];
extern uint8_t nb_rx_buf_temp[NB_RX_BUF_MAX_LENGTH];
extern uint8_t nb_cmd_buf[100];

/* 函数声明 ----------------------------------------------------------*/

uint8_t nb_init(void);
void nb_rst(void);
void nb_buf_clear(void);
uint8_t nb_wait_recive(void);
uint8_t nb_send_cmd(unsigned char *cmd, unsigned char *res);
uint8_t nb_get_message(uint16_t time_out);
void nb_buf_temp_clear(void);
uint16_t nb_buf_temp_get_len(void);








#endif


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
