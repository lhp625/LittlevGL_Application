/*
*******************************************************************************************************
*
* 文件名称 : ir_receive.h
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 定时器输入捕获红外接收
* 
*******************************************************************************************************
*/

#ifndef _IR_RECEIVE_H
#define _IR_RECEIVE_H


/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"


/* 宏定义 -----------------------------------------------------------*/

#define REMOTE_ID 0

/* 全局变量 ----------------------------------------------------------*/


/* 函数声明 ----------------------------------------------------------*/
void ir_receive_init(void);
uint8_t ir_receive_scan(void);

void ir_receive_start_scan(void);
void ir_receive_stop_scan(void);


#endif

/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
