/*
*******************************************************************************************************
*
* 文件名称 : battery.h
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 电池电源管理文件
* 
*******************************************************************************************************
*/

#ifndef _BATTERY_H
#define _BATTERY_H


/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"



/* 宏定义 -----------------------------------------------------------*/

/* 定义充电检测的IO */
#define BAT_CHRG_PIN             GPIO_PIN_5
#define BAT_CHRG_PORT            GPIOH
/* 定义充满检测的IO */
#define BAT_STDBY_PIN            GPIO_PIN_3
#define BAT_STDBY_PORT           GPIOA



typedef struct{
	float adc;       //电池ADC值
	float voltage;   //电池电压值
	float max;       //电池电压上限值
	float min;       //电池电压下限值
	uint8_t ele;     //电量
	uint8_t chrg;       //充电标识
	uint8_t stdby;      //充满标识
	
}BAT_TypeDef;



/* 全局变量 ----------------------------------------------------------*/


/* 函数声明 ----------------------------------------------------------*/



void bat_manage_filter(float data,float *filt_data,uint8_t len);
void bat_manage_init(void);
uint8_t bat_manage_ele_get(void);
float bat_manage_voltage_get(void);
void bat_manage_current(BAT_TypeDef *current_bat);

#endif

/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
