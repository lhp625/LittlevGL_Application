/*
*******************************************************************************************************
*
* 文件名称 : beep.h
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 蜂鸣器驱动文件
* 
*******************************************************************************************************
*/

#ifndef _BEEP_H
#define _BEEP_H


/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"


/* 宏定义 -----------------------------------------------------------*/



/* 全局变量 ----------------------------------------------------------*/


/* 函数声明 ----------------------------------------------------------*/

void beep_init(void);
void beep_enable(uint8_t en);
void beep_adjust(uint8_t sound);





#endif

/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
