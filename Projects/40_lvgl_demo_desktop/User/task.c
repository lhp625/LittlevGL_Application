/*
*******************************************************************************************************
*
* 文件名称 : task.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 主函数文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"
#include "gui_user.h"
#include "task.h"

/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/


/* 函数声明 ---------------------------------------------------------*/

/* RT-Thread相关函数和变量 */
static rt_thread_t key_thread = RT_NULL;
static void key_thread_entry(void *parameter);

static rt_thread_t gui_thread = RT_NULL;
static void gui_thread_entry(void *parameter);

static rt_thread_t idle_thread = RT_NULL;
static void idle_thread_entry(void *parameter);

static rt_thread_t wlan_thread = RT_NULL;
static void wlan_thread_entry(void *parameter);


rt_mq_t gui_mq = RT_NULL;     
rt_mq_t wlan_mq = RT_NULL;

rt_sem_t i2c3_sem = RT_NULL;			//I2C????????????
rt_sem_t data_collect_sem = RT_NULL;		//???????????
rt_sem_t eeprom_sem = RT_NULL;			//????EEPROM????????

/**
  * @brief main主函数
  * @param None
  * @retval	None
  */
void my_task_schedule(void)
{
	
	/* 数值越小优先级越高, 0代表最高优先级 */
	
	
	i2c3_sem = rt_sem_create("i2c3_sem",
	                          1,
	                          RT_IPC_FLAG_FIFO);
	if(i2c3_sem == RT_NULL)
		rt_kprintf("i2c3_sem create filed!\r\n");
	

	data_collect_sem = rt_sem_create("data_collect_sem",
	                          1,
	                          RT_IPC_FLAG_FIFO);
	if(data_collect_sem == RT_NULL)
		rt_kprintf("data_collect_sem create filed!\r\n");
	

	eeprom_sem = rt_sem_create("eeprom_sem",
	                          1,
	                          RT_IPC_FLAG_FIFO);
	if(eeprom_sem == RT_NULL)
		rt_kprintf("eeprom_sem create filed!\r\n");
	

	gui_mq = rt_mq_create("gui_mq",
							MQ_MAX_LEN,
							20,
							RT_IPC_FLAG_FIFO);
	if (gui_mq == RT_NULL)
		rt_kprintf("gui_mq create filed!\n\n");
	
	wlan_mq = rt_mq_create("wlan_mq",
							4,
							20,
							RT_IPC_FLAG_FIFO);
	if (wlan_mq == RT_NULL)
		rt_kprintf("wlan_mq create filed!\n\n");
	
	
	
	/* 创建按键线程 */
	key_thread = rt_thread_create("key_thread",
									key_thread_entry,
									RT_NULL,
									512,
									2,
									20);
	rt_thread_startup(key_thread);	
	
	/* 创建GUI线程 */
	gui_thread = rt_thread_create("gui_thread",
									gui_thread_entry,
									RT_NULL,
									4096,
									2,
									20);
	rt_thread_startup(gui_thread);
	
	/* 创建用户空闲线程 */
	idle_thread = rt_thread_create("idle_thread",
									idle_thread_entry,
									RT_NULL,
									128,
									RT_THREAD_PRIORITY_MAX-1,
									20);
	rt_thread_startup(idle_thread);
	
	/* 创建WIFI线程 */
	wlan_thread = rt_thread_create("wlan_thread",
									wlan_thread_entry,
									RT_NULL,
									4096,
									6,
									20);
	rt_thread_startup(wlan_thread);
	
	
	
}




/**
  * @brief GUI线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void gui_thread_entry(void *parameter)
{

	lv_init();
	lv_port_disp_init();
	lv_port_indev_init();
	lv_app_main();

	while(1)
	{
		lv_task_handler();
		rt_thread_delay(5);
	}
}



/**
  * @brief 按键线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void key_thread_entry(void *parameter)
{
	uint8_t key;
	uint8_t mq_send_buffer[MQ_MAX_LEN] = {0};
	while(1)
	{
		
		key = key_get();
		switch(key)
		{
			case KEY_LEFT_DOWN:
			case KEY_LEFT_UP:
			case KEY_LEFT_LONG:
			case KEY_WKUP_DOWN:
			case KEY_WKUP_UP:
			case KEY_WKUP_LONG:
			case KEY_RIGHT_DOWN:
			case KEY_RIGHT_UP:
			case KEY_RIGHT_LONG:
				mq_send_buffer[0] = MQ_KEY;
				mq_send_buffer[1] = key;
				rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
			default:
				break;
		}
		rt_thread_delay(10);
	}
}


typedef enum{
	WIFI_INIT = 0,		   /* ??????? */
	WIFI_JOIN_AP,          /* ??????AP */
	WIFI_CONNECT_SERVER,   /* ??????????? */
	WIFI_UPLOAD,            /* ????????? */
	WIFI_IDLE,            /* ???? */
}wifi_status;
/**
  * @brief WIFI线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void wlan_thread_entry(void *parameter)
{
	uint8_t status = 0;
	uint8_t error = 0;
	uint16_t tick = 0;
	uint8_t mq_send_buffer[MQ_MAX_LEN] = {0};
	uint8_t mq_recv_buffer[MQ_MAX_LEN] = {0};
	
	_SETTING wlan_setting = {0};
	

	/* ????????????????? */
	rt_thread_delay(2000);

	
	
	while(1)
	{
		switch(status)
		{
			case WIFI_INIT:
				
			
				rt_sem_take(i2c3_sem,RT_WAITING_FOREVER);
				rt_sem_take(eeprom_sem,RT_WAITING_FOREVER);
				setting_read(&wlan_setting);   //read?????????????
				rt_sem_release(i2c3_sem);
				rt_sem_release(eeprom_sem);
			
			
				mq_send_buffer[0] = MQ_WLAN_STATUS;
				mq_send_buffer[1] = 0;
				rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
			
				if(wlan_setting.wlan_power == 0)
				{
					rt_kprintf("WLAN POWER DOWN!\r\n");
					esp12s_en(0);
					status = WIFI_IDLE;
					break;
				}
				esp12s_en(1);
			
				if(esp12s_init(wlan_setting.wlan_ssid,wlan_setting.wlan_pwd) == 0)
				{
					/* ???????????WIFI??? */
					status = WIFI_CONNECT_SERVER;
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 1;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
				}
				else
				{
					/* ??????????????δ????WIFI */
					status = WIFI_JOIN_AP;
					
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 0;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
				}
				break;
			case WIFI_JOIN_AP:
				if(esp12s_join_ap(wlan_setting.wlan_ssid,wlan_setting.wlan_pwd) == 0)
				{
					/* ????WIFI??? */
					status = WIFI_CONNECT_SERVER;
					
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 1;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
					
				}
				else
				{
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 0;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
					error++;
				}
				break;
			case WIFI_CONNECT_SERVER:
				if(esp12s_connect_server(wlan_setting.server_ip,wlan_setting.server_port,(unsigned char *)" ") == 0)
				{
					/* ???????????? */
					status = WIFI_UPLOAD;
				}
				else
				{
					/* ???????????? */
					uint8_t wifi_status = 0;
					wifi_status = esp12s_get_status();
					if(wifi_status==2 || wifi_status==3 || wifi_status==4)
					{
						/* ????WIFI???????? */
						status = WIFI_CONNECT_SERVER;
					}
					else
					{
						/* WIFI??? */
						status = WIFI_JOIN_AP;
					}
					
				}
				break;
			case WIFI_UPLOAD:
				esp12s_send_data((unsigned char *)"Open Rabbit\r\n",13);
				
				break;
			case WIFI_IDLE:
				break;
			default:
				break;
		}
		if(status<=WIFI_JOIN_AP)
		{
			tick = 50;
			while(--tick)
			{
				/* ?????? */
				if(rt_mq_recv(wlan_mq,mq_recv_buffer,MQ_MAX_LEN,RT_WAITING_NO) == 0)
				{
					switch(mq_recv_buffer[0])
					{
						
						case MQ_WLAN_UPDATA_SET:
							status = WIFI_INIT;
							break;
						default:
							break;
					}
				}
				
				
				rt_thread_delay(100);
			}
		}
		else
		{
			tick = 10;
			while(--tick)
			{
				/* ?????? */
				if(rt_mq_recv(wlan_mq,mq_recv_buffer,MQ_MAX_LEN,RT_WAITING_NO) == 0)
				{
					switch(mq_recv_buffer[0])
					{
						
						case MQ_WLAN_UPDATA_SET:
							status = WIFI_INIT;
							break;
						default:
							break;
					}
				}
				rt_thread_delay(100);
			}
			if((status==WIFI_UPLOAD) && (esp12s_buf_find((unsigned char *)"SEND OK") == NULL))
			{
				/* ???????????? */
				error++;
			}
		}
		
		if(error >= 10)
		{
			if(status == WIFI_UPLOAD)
			{
				/* ????????????? */
				status = WIFI_CONNECT_SERVER;
			}
			else
			{
				/* ?????????????WIFI??? */
				status = WIFI_INIT;
			}
			error = 0;
		}
		
	}
}


/**
  * @brief 用户空闲线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void idle_thread_entry(void *parameter)
{
	while(1)
	{
		//iwdg_feed();
		rt_thread_delay(500);
	}
}



/*
		key = key_get();
		switch(key)
		{
			case KEY_LEFT_DOWN:
				printf("KEY_LEFT_DOWN\r\n");
				break;
			case KEY_LEFT_UP:
				printf("KEY_LEFT_UP\r\n");
				break;
			case KEY_LEFT_LONG:
				printf("KEY_LEFT_LONG\r\n");
				break;
			case KEY_WKUP_DOWN:
				printf("KEY_WKUP_DOWN\r\n");
				break;
			case KEY_WKUP_UP:
				printf("KEY_WKUP_UP\r\n");
				break;
			case KEY_WKUP_LONG:
				printf("KEY_WKUP_LONG\r\n");
				break;
			case KEY_RIGHT_DOWN:
				printf("KEY_RIGHT_DOWN\r\n");
				break;
			case KEY_RIGHT_UP:
				printf("KEY_RIGHT_UP\r\n");
				break;
			case KEY_RIGHT_LONG:
				printf("KEY_RIGHT_LONG\r\n");
				break;
			default:
				break;
		}
*/



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
