/*
*******************************************************************************************************
*
* 文件名称 : task.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 主函数文件
* 
*******************************************************************************************************
*/



/* 头文件 -----------------------------------------------------------*/
#include "bsp.h"
#include "gui_user.h"
#include "task.h"

/* 宏定义 -----------------------------------------------------------*/


/* 变量 -------------------------------------------------------------*/


/* 函数声明 ---------------------------------------------------------*/

/* RT-Thread相关函数和变量 */
static rt_thread_t key_thread = RT_NULL;
static void key_thread_entry(void *parameter);

static rt_thread_t gui_thread = RT_NULL;
static void gui_thread_entry(void *parameter);

static rt_thread_t idle_thread = RT_NULL;
static void idle_thread_entry(void *parameter);

static rt_thread_t wlan_thread = RT_NULL;
static void wlan_thread_entry(void *parameter);

static rt_thread_t nb_thread = RT_NULL;
static void nb_thread_entry(void *parameter);

rt_mq_t gui_mq = RT_NULL;     
rt_mq_t wlan_mq = RT_NULL;
rt_mq_t gui_nb_mq = RT_NULL;
rt_mq_t nb_mq = RT_NULL;

rt_sem_t i2c3_sem = RT_NULL;			//I2C????????????
rt_sem_t data_collect_sem = RT_NULL;		//???????????
rt_sem_t eeprom_sem = RT_NULL;			//????EEPROM????????

/**
  * @brief main主函数
  * @param None
  * @retval	None
  */
void my_task_schedule(void)
{
	
	/* 数值越小优先级越高, 0代表最高优先级 */
	
	
	i2c3_sem = rt_sem_create("i2c3_sem",
	                          1,
	                          RT_IPC_FLAG_FIFO);
	if(i2c3_sem == RT_NULL)
		rt_kprintf("i2c3_sem create filed!\r\n");
	

	data_collect_sem = rt_sem_create("data_collect_sem",
	                          1,
	                          RT_IPC_FLAG_FIFO);
	if(data_collect_sem == RT_NULL)
		rt_kprintf("data_collect_sem create filed!\r\n");
	

	eeprom_sem = rt_sem_create("eeprom_sem",
	                          1,
	                          RT_IPC_FLAG_FIFO);
	if(eeprom_sem == RT_NULL)
		rt_kprintf("eeprom_sem create filed!\r\n");
	

	gui_mq = rt_mq_create("gui_mq",
							MQ_MAX_LEN,
							20,
							RT_IPC_FLAG_FIFO);
	if (gui_mq == RT_NULL)
		rt_kprintf("gui_mq create filed!\n\n");
	
	wlan_mq = rt_mq_create("wlan_mq",
							4,
							20,
							RT_IPC_FLAG_FIFO);
	if (wlan_mq == RT_NULL)
		rt_kprintf("wlan_mq create filed!\n\n");
	
#ifdef NB_IOT
	//gui_nb_mq
	nb_mq = rt_mq_create("nb_mq",
							32,
							20,
							RT_IPC_FLAG_FIFO);
	if (nb_mq == RT_NULL)
		rt_kprintf("nb_mq create filed!\n\n");
	
	gui_nb_mq = rt_mq_create("gui_nb_mq",
							512,
							20,
							RT_IPC_FLAG_FIFO);
	if (gui_nb_mq == RT_NULL)
		rt_kprintf("gui_nb_mq create filed!\n\n");
#endif
	
	
	/* 创建按键线程 */
	key_thread = rt_thread_create("key_thread",
									key_thread_entry,
									RT_NULL,
									512,
									2,
									20);
	rt_thread_startup(key_thread);	
	
	/* 创建GUI线程 */
	gui_thread = rt_thread_create("gui_thread",
									gui_thread_entry,
									RT_NULL,
									4096,
									2,
									20);
	rt_thread_startup(gui_thread);
	
	/* 创建用户空闲线程 */
	idle_thread = rt_thread_create("idle_thread",
									idle_thread_entry,
									RT_NULL,
									512,
									RT_THREAD_PRIORITY_MAX-1,
									20);
	rt_thread_startup(idle_thread);
	
	/* 创建WIFI线程 */
	wlan_thread = rt_thread_create("wlan_thread",
									wlan_thread_entry,
									RT_NULL,
									2048,
									6,
									20);
	rt_thread_startup(wlan_thread);
	
#ifdef NB_IOT
	/* 创建NB线程 */
	nb_thread = rt_thread_create("nb_thread",
									nb_thread_entry,
									RT_NULL,
									2048,
									6,
									20);
	rt_thread_startup(nb_thread);
#endif

}




/**
  * @brief GUI线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void gui_thread_entry(void *parameter)
{

	lv_init();
	lv_port_disp_init();
	lv_port_indev_init();
	lv_app_main();

	while(1)
	{
		lv_task_handler();
		rt_thread_delay(5);
	}
}



/**
  * @brief 按键线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void key_thread_entry(void *parameter)
{
	uint8_t key;
	uint8_t mq_send_buffer[MQ_MAX_LEN] = {0};
	while(1)
	{
		
		key = key_get();
		switch(key)
		{
			case KEY_LEFT_DOWN:
			case KEY_LEFT_UP:
			case KEY_LEFT_LONG:
			case KEY_WKUP_DOWN:
			case KEY_WKUP_UP:
			case KEY_WKUP_LONG:
			case KEY_RIGHT_DOWN:
			case KEY_RIGHT_UP:
			case KEY_RIGHT_LONG:
				mq_send_buffer[0] = MQ_KEY;
				mq_send_buffer[1] = key;
				rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
			default:
				break;
		}
		rt_thread_delay(10);
	}
}


typedef enum{
	WIFI_INIT = 0,		   /* ??????? */
	WIFI_JOIN_AP,          /* ??????AP */
	WIFI_CONNECT_SERVER,   /* ??????????? */
	WIFI_UPLOAD,            /* ????????? */
	WIFI_IDLE,            /* ???? */
}wifi_status;
/**
  * @brief WIFI线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void wlan_thread_entry(void *parameter)
{
	uint8_t status = 0;
	uint8_t error = 0;
	uint16_t tick = 0;
	uint8_t mq_send_buffer[MQ_MAX_LEN] = {0};
	uint8_t mq_recv_buffer[MQ_MAX_LEN] = {0};
	
	_SETTING wlan_setting = {0};
	

	/* ????????????????? */
	rt_thread_delay(2000);

	
	
	while(1)
	{
		switch(status)
		{
			case WIFI_INIT:
				
			
				rt_sem_take(i2c3_sem,RT_WAITING_FOREVER);
				rt_sem_take(eeprom_sem,RT_WAITING_FOREVER);
				setting_read(&wlan_setting);   //read?????????????
				rt_sem_release(i2c3_sem);
				rt_sem_release(eeprom_sem);
			
			
				mq_send_buffer[0] = MQ_WLAN_STATUS;
				mq_send_buffer[1] = 0;
				rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
			
				if(wlan_setting.wlan_power == 0)
				{
					rt_kprintf("WLAN POWER DOWN!\r\n");
					esp12s_en(0);
					status = WIFI_IDLE;
					break;
				}
				esp12s_en(1);
				rt_kprintf("wlan_ssid:%s wlan_pwd:%s\r\n",wlan_setting.wlan_ssid,wlan_setting.wlan_pwd);
				if(esp12s_init(wlan_setting.wlan_ssid,wlan_setting.wlan_pwd) == 0)
				{
					/* ???????????WIFI??? */
					status = WIFI_CONNECT_SERVER;
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 1;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
				}
				else
				{
					/* ??????????????δ????WIFI */
					status = WIFI_JOIN_AP;
					
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 0;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
				}
				break;
			case WIFI_JOIN_AP:
				if(esp12s_join_ap(wlan_setting.wlan_ssid,wlan_setting.wlan_pwd) == 0)
				{
					/* ????WIFI??? */
					status = WIFI_CONNECT_SERVER;
					
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 1;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
					
				}
				else
				{
					mq_send_buffer[0] = MQ_WLAN_STATUS;
					mq_send_buffer[1] = 0;
					rt_mq_send(gui_mq,mq_send_buffer,MQ_MAX_LEN);
					error++;
				}
				break;
			case WIFI_CONNECT_SERVER:
				if(esp12s_connect_server(wlan_setting.server_ip,wlan_setting.server_port,(unsigned char *)" ") == 0)
				{
					/* ???????????? */
					status = WIFI_UPLOAD;
				}
				else
				{
					/* ???????????? */
					uint8_t wifi_status = 0;
					wifi_status = esp12s_get_status();
					if(wifi_status==2 || wifi_status==3 || wifi_status==4)
					{
						/* ????WIFI???????? */
						status = WIFI_CONNECT_SERVER;
					}
					else
					{
						/* WIFI??? */
						status = WIFI_JOIN_AP;
					}
					
				}
				break;
			case WIFI_UPLOAD:
				esp12s_send_data((unsigned char *)"Open Rabbit\r\n",13);
				
				break;
			case WIFI_IDLE:
				break;
			default:
				break;
		}
		if(status<=WIFI_JOIN_AP)
		{
			tick = 50;
			while(--tick)
			{
				/* ?????? */
				if(rt_mq_recv(wlan_mq,mq_recv_buffer,MQ_MAX_LEN,RT_WAITING_NO) == 0)
				{
					switch(mq_recv_buffer[0])
					{
						
						case MQ_WLAN_UPDATA_SET:
							status = WIFI_INIT;
							break;
						default:
							break;
					}
				}
				
				
				rt_thread_delay(100);
			}
		}
		else
		{
			tick = 10;
			while(--tick)
			{
				/* ?????? */
				if(rt_mq_recv(wlan_mq,mq_recv_buffer,MQ_MAX_LEN,RT_WAITING_NO) == 0)
				{
					switch(mq_recv_buffer[0])
					{
						
						case MQ_WLAN_UPDATA_SET:
							status = WIFI_INIT;
							break;
						default:
							break;
					}
				}
				rt_thread_delay(100);
			}
			if((status==WIFI_UPLOAD) && (esp12s_buf_find((unsigned char *)"SEND OK") == NULL))
			{
				/* ???????????? */
				error++;
			}
		}
		
		if(error >= 10)
		{
			if(status == WIFI_UPLOAD)
			{
				/* ????????????? */
				status = WIFI_CONNECT_SERVER;
			}
			else
			{
				/* ?????????????WIFI??? */
				status = WIFI_INIT;
			}
			error = 0;
		}
		
	}
}



typedef enum{
	NB_INIT = 0,
	NB_DATA_INFO,
	NB_UART_INFO,
	NB_UART_SEND,
	NB_DATA_GET,
}nb_mq_header;
_nb_iot_info nb_iot_info;
/**
  * @brief NB线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void nb_thread_entry(void *parameter)
{
	uint8_t i=0,j=0;
	uint8_t	buf[32]={0};
	uint8_t imei[16]={0};
	uint8_t imsi[16]={0};
	uint8_t nb_check = 0;
	uint8_t band[2] ={0};
	uint8_t sta_buf[64];	//存放状态信息
	uint16_t uart_buf_len = 0;
	
	
	uint8_t mq_send_buf[512];
	uint8_t mq_recv_buf[32];
	
	nb_check = nb_init();
	
	if(nb_check == 0)
	{
		
		mq_send_buf[0] = NB_INIT;
		mq_send_buf[1] = 1;
		rt_mq_send(gui_nb_mq,mq_send_buf,sizeof(mq_send_buf));
		
		//rt_kprintf("\r\nNB INIT OK\r\n",nb_rx_buf);
	}
	/* 等待UI开始 */
	rt_mq_recv(nb_mq,mq_recv_buf,sizeof(mq_recv_buf),RT_WAITING_FOREVER);
	
	while(1)
	{
		if(rt_mq_recv(nb_mq,mq_recv_buf,sizeof(mq_recv_buf),10)==RT_EOK)
		{
			if(mq_recv_buf[0] == NB_DATA_GET)
			{
				/* IMSI */
				if(imsi[0] == 0)
				{
					
					if(nb_send_cmd((uint8_t *)"AT+CIMI\r\n",(uint8_t *)"OK") == 0)
					{
						for(j=0;j<15;j++)
						{
							//找到数字第一次出现的位置
							if(nb_rx_buf[j]>=0x30 && nb_rx_buf[j]<=0x39)
								break;
						}
						for(i=0;i<15;i++)
						{
							imsi[i] = nb_rx_buf[i+j];
						}
						memcpy(nb_iot_info.imsi,imsi,sizeof(nb_iot_info.imsi));
						rt_kprintf("\r\n**********************************************\r\n");
						rt_kprintf("\r\n imsi:%s\r\n",nb_iot_info.imsi);
						rt_kprintf("\r\n**********************************************\r\n");
					}
				}
				else
				{
					memcpy(nb_iot_info.imsi,imsi,sizeof(nb_iot_info.imsi));
				}
				/* IMEI */
				if(imei[0] == 0)
				{
					
					if(nb_send_cmd((uint8_t *)"AT+CGSN=1\r\n",(uint8_t *)"OK") == 0)
					{
						for(j=0;j<15;j++)
						{
							//找到数字第一次出现的位置
							if(nb_rx_buf[j]>=0x30 && nb_rx_buf[j]<=0x39)
								break;
						}
						for(i=0;i<15;i++)
						{
							imei[i] = nb_rx_buf[i+j];
						}
						memcpy(nb_iot_info.imei,imei,sizeof(nb_iot_info.imei));
						rt_kprintf("\r\n**********************************************\r\n");
						rt_kprintf("\r\n imei:%s\r\n",nb_iot_info.imei);
						rt_kprintf("\r\n**********************************************\r\n");
					}
				}
				else
				{
					memcpy(nb_iot_info.imei,imei,sizeof(nb_iot_info.imei));
				}
				/* BAND */
				if(band[0] == 0)
				{
					//+NBAND:8
					if(nb_send_cmd((uint8_t *)"AT+NBAND?\r\n",(uint8_t *)"OK") == 0)
					{
						if(strstr((char*)nb_rx_buf,"+NBAND:")!=NULL)
						{
							strncpy((char*)band, strstr((char*)nb_rx_buf,"+NBAND:")+7, 1);
						}
						memcpy(nb_iot_info.band,band,sizeof(nb_iot_info.band));
						rt_kprintf("\r\n**********************************************\r\n");
						rt_kprintf("\r\n BAND:%s\r\n",nb_iot_info.band);
						rt_kprintf("\r\n**********************************************\r\n");
					}
				}
				else
				{
					memcpy(nb_iot_info.band,band,sizeof(nb_iot_info.band));
				}
				/* CSQ */
				if(1)
				{
					if(nb_send_cmd((uint8_t *)"AT+CSQ\r\n",(uint8_t *)"OK")==0)
					{
						memset(buf,0,sizeof(buf));
						strncpy((char*)buf, strstr((char*)nb_rx_buf,"+CSQ:")+5, sizeof(buf));
						for(i=0;i<sizeof(buf);i++)
						{
							if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
								break;	
						}
						memset(buf+i,0,(sizeof(buf)-i));
						//rt_kprintf("csq:%s\r\n",buf);
						memcpy(nb_iot_info.csq,buf,sizeof(nb_iot_info.csq));
					}
				}
				/* STATUS */
				if(1)
				{
					if(nb_send_cmd((uint8_t *)"AT+NUESTATS\r\n",(uint8_t *)"OK") == 0)
					{
						if(strstr((char*)nb_rx_buf,"Signal power:")!=NULL)
						{
							/* Signal power */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"Signal power:")+13, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.signal_power,i+1,"%s",buf);
							
							/* Total power */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"Total power:")+12, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.total_power,i+1,"%s",buf);
							
							/* TX power */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"TX power:")+9, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.tx_power,i+1,"%s",buf);
							
							/* TX time */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"TX time:")+8, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.tx_time,i+1,"%s",buf);
							
							/* RX time */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"RX time:")+8, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.rx_time,i+1,"%s",buf);
							
							/* Cell ID */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"Cell ID:")+8, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.cell_id,i+1,"%s",buf);
							
							/* ECL */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"ECL:")+4, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.ecl,i+1,"%s",buf);
							
							/* SNR */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"SNR:")+4, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.snr,i+1,"%s",buf);
							
							/* EARFCN */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"EARFCN:")+7, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.earfcn,i+1,"%s",buf);
							
							/* PCI */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"PCI:")+4, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.pci,i+1,"%s",buf);
							
							/* RSRQ */
							strncpy((char*)buf, strstr((char*)nb_rx_buf,"RSRQ:")+5, sizeof(buf));
							for(i=1;i<sizeof(buf);i++)
							{
								if((buf[i]<0x30)||(buf[i]>0x39))	//如果出现非数字部分	
									break;	
							}
							memset(buf+i,0,(sizeof(buf)-i));
							snprintf((char*)nb_iot_info.rsrq,i+1,"%s",buf);
						}
					}
				}
				
				memset(mq_send_buf,0,sizeof(mq_send_buf));
				mq_send_buf[0] = NB_DATA_INFO;
				memcpy((mq_send_buf+1),&nb_iot_info,sizeof(nb_iot_info));
				rt_mq_send(gui_nb_mq,mq_send_buf,sizeof(mq_send_buf));
			}
			else if(mq_recv_buf[0] == NB_UART_SEND)
			{
				nb_send_cmd((uint8_t *)mq_recv_buf+1,NULL);
			}
		}
		if((uart_buf_len = nb_buf_temp_get_len())!=0)
		{
			memset(mq_send_buf,0,sizeof(mq_send_buf));
			mq_send_buf[0] = NB_UART_INFO;
			memcpy((mq_send_buf+1),nb_rx_buf_temp,uart_buf_len);
			rt_mq_send(gui_nb_mq,mq_send_buf,sizeof(mq_send_buf));
			nb_buf_temp_clear();
		}
	}
}




/**
  * @brief 用户空闲线程函数
  * @param parameter-线程入口参数
  * @retval	None
  */
static void idle_thread_entry(void *parameter)
{
	while(1)
	{
		//iwdg_feed();
		rt_thread_delay(2000);

	}
}



/*
		key = key_get();
		switch(key)
		{
			case KEY_LEFT_DOWN:
				printf("KEY_LEFT_DOWN\r\n");
				break;
			case KEY_LEFT_UP:
				printf("KEY_LEFT_UP\r\n");
				break;
			case KEY_LEFT_LONG:
				printf("KEY_LEFT_LONG\r\n");
				break;
			case KEY_WKUP_DOWN:
				printf("KEY_WKUP_DOWN\r\n");
				break;
			case KEY_WKUP_UP:
				printf("KEY_WKUP_UP\r\n");
				break;
			case KEY_WKUP_LONG:
				printf("KEY_WKUP_LONG\r\n");
				break;
			case KEY_RIGHT_DOWN:
				printf("KEY_RIGHT_DOWN\r\n");
				break;
			case KEY_RIGHT_UP:
				printf("KEY_RIGHT_UP\r\n");
				break;
			case KEY_RIGHT_LONG:
				printf("KEY_RIGHT_LONG\r\n");
				break;
			default:
				break;
		}
*/



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
