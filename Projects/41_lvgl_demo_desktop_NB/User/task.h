/*
*******************************************************************************************************
*
* 文件名称 : task.h
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : RTOS任务管理文件
* 
*******************************************************************************************************
*/

#ifndef _TASK_H
#define _TASK_H


/* 头文件 -----------------------------------------------------------*/
#include "rtthread.h"

/* 宏定义 -----------------------------------------------------------*/
#define MQ_MAX_LEN 128

/* 消息头定义 */
enum{
	MQ_TEMP = 0,		   	 /* 温度 */
	MQ_WLAN_STATUS,          /* WIFI状态更新 */
	MQ_KEY,                  /* 按键信息 */
	MQ_WLAN_UPDATA_SET,      /* 更新WIFI设置 */
	MQ_MPU_UPDATA,           /* MPU6050三轴数据更新 */

};






/* 全局变量 ----------------------------------------------------------*/

extern rt_mq_t gui_mq;
extern rt_mq_t wlan_mq;

extern rt_mq_t gui_nb_mq;
extern rt_mq_t nb_mq;

extern rt_sem_t i2c3_sem;
extern rt_sem_t data_collect_sem;
extern rt_sem_t eeprom_sem;



/* 函数声明 ----------------------------------------------------------*/

void my_task_schedule(void);
void cpu_usage_init(void);
void cpu_usage_get(rt_uint8_t *major, rt_uint8_t *minor);



#endif

/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
