/*
*******************************************************************************************************
*
* 文件名称 : messagbox_win.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 消息提示框应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

static lv_obj_t *mbox_parent = NULL;
static lv_obj_t *mbox_obj = NULL;

static const char * btns2[] = { "确定", "取消",""};

static uint8_t cliked_btn1 = 0, clicked_btn2 = 0;

typedef enum{
	MB_BTN_NONE = 0,
	MB_BTN_OK,
	MB_BTN_CANCEL,
}MB_BTN;




static void message_box_close_event_cb(lv_obj_t *obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED) {
		lv_obj_t * win = lv_win_get_from_btn(obj);

		lv_obj_del(lv_obj_get_parent(win));
	}
}

static void lv_mbox_close_ready_cb(lv_anim_t * a)
{
	lv_obj_del(a->var);
	if (mbox_obj != NULL)
	{
		lv_obj_del(mbox_obj);
		mbox_obj = NULL;
	}

}
void mbox_win_start_auto_close(lv_obj_t * obj, uint16_t delay)
{
#if LV_USE_ANIMATION
		/*Add shrinking animations*/
		lv_anim_t a;
		a.var = obj;
		a.start = lv_obj_get_height(obj);
		a.end = 0;
		a.exec_cb = (lv_anim_exec_xcb_t)lv_obj_set_height;
		a.path_cb = lv_anim_path_linear;
		a.ready_cb = NULL;
		a.act_time = -delay;
		a.time = 100;
		a.playback = 0;
		a.playback_pause = 0;
		a.repeat = 0;
		a.repeat_pause = 0;
		lv_anim_create(&a);

		a.start = lv_obj_get_width(obj);
		a.exec_cb = (lv_anim_exec_xcb_t)lv_obj_set_width;
		a.ready_cb = lv_mbox_close_ready_cb;
		lv_anim_create(&a);

#else
	lv_obj_del(obj);
	if (mbox_obj != NULL)
	{
		lv_obj_del(mbox_obj);
		mbox_obj = NULL;
}
#endif
}


static void btn1_event_cb(lv_obj_t *obj, lv_event_t event)
{
	if (event == LV_EVENT_CLICKED) {
		cliked_btn1 = 1;
		lv_event_send(mbox_parent, LV_EVENT_APPLY, NULL);

		lv_obj_t * win = lv_obj_get_parent(obj);
		win = lv_obj_get_parent(win);
		win = lv_obj_get_parent(win);
		//win = lv_obj_get_parent(win);
		//lv_obj_del(win);
		mbox_win_start_auto_close(win, 0);

	}

}

static void btn2_event_cb(lv_obj_t *obj, lv_event_t event)
{
	if (event == LV_EVENT_CLICKED) {
		clicked_btn2 = 1;
		lv_event_send(mbox_parent, LV_EVENT_CANCEL, NULL);
		
		lv_obj_t * win = lv_obj_get_parent(obj);
		win = lv_obj_get_parent(win);
		win = lv_obj_get_parent(win);
		//win = lv_obj_get_parent(win);
		//lv_obj_del(win);

		mbox_win_start_auto_close(win, 0);
	}
}



lv_obj_t * message_box_win(lv_obj_t *parent, const char *title, const char *text, const char *btn1_text, const char *btn2_text)
{
	
	static lv_style_t style_label;
	static lv_style_t style_modal;

	mbox_parent = parent;

	/* 创建一个窗口用于模态化效果 */
	/* Create a full-screen background */
	lv_style_copy(&style_modal, &lv_style_plain_color);
	/* Set the background's style */
	style_modal.body.main_color = style_modal.body.grad_color = LV_COLOR_BLACK;
	style_modal.body.opa = LV_OPA_50;
	/* Create a base object for the modal background */
	lv_obj_t *obj = lv_obj_create(parent, NULL);
	lv_obj_set_style(obj, &style_modal);
	lv_obj_set_pos(obj, 0, 0);
	lv_obj_set_size(obj, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_opa_scale_enable(obj, true); /* Enable opacity scaling for the animation */

	mbox_obj = obj;

	lv_obj_t *win = lv_win_create(obj,NULL);
	lv_obj_set_width(win,lv_obj_get_width(lv_obj_get_parent(win))*2/3);
	lv_obj_set_height(win, lv_obj_get_height(lv_obj_get_parent(win)) / 3);
	lv_win_set_drag(win,true);
	lv_win_set_title(win, title);
	lv_win_set_sb_mode(win,false);
	lv_obj_align(win, obj, LV_ALIGN_CENTER, 0, 0);

	lv_obj_t *btn = lv_win_add_btn(win, LV_SYMBOL_CLOSE);
	lv_obj_set_event_cb(btn, message_box_close_event_cb);
	
	
	lv_obj_t *label = lv_label_create(win, NULL);
	lv_obj_set_height(label, lv_obj_get_width(win)-100);
	lv_style_copy(&style_label, lv_label_get_style(label,LV_LABEL_STYLE_MAIN));
	style_label.text.font = &gb2312_puhui16;
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label, text);
	lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
	lv_obj_set_height(label, lv_obj_get_width(win) - 100);
	lv_obj_set_width(label, lv_obj_get_width(win) - 10);



	lv_obj_t *label_btn;
	lv_obj_t *btn2;
	lv_obj_t *btn1;

	btn1 = lv_btn_create(win,NULL);
	label_btn = lv_label_create(btn1, NULL);
	lv_obj_set_event_cb(btn1,btn1_event_cb);
	lv_label_set_style(label_btn, LV_LABEL_STYLE_MAIN, &style_label);
	if (btn1_text != NULL)
	{
		lv_label_set_text(label_btn, btn1_text);
	}
	else
	{
		lv_label_set_text(label_btn, btns2[0]);
	}
	lv_obj_align(btn1, win, LV_ALIGN_IN_BOTTOM_MID, 0, -10);

	if (btn2_text != NULL)
	{
		btn2 = lv_btn_create(win, NULL);
		lv_obj_set_event_cb(btn2, btn2_event_cb);
		label_btn = lv_label_create(btn2, NULL);
		lv_label_set_style(label_btn, LV_LABEL_STYLE_MAIN, &style_label);

		lv_label_set_text(label_btn, btn2_text);

		lv_obj_align(btn1, win, LV_ALIGN_IN_BOTTOM_MID, -80, -10);
		lv_obj_align(btn2, win, LV_ALIGN_IN_BOTTOM_MID, 80, -10);
	}

	return obj;
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

