/*
*******************************************************************************************************
*
* 文件名称 : app_key.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 按键应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"


static lv_chart_series_t *ser_key1;
static lv_chart_series_t *ser_key2;
static lv_chart_series_t *ser_key3;

static lv_obj_t * cb1;
static lv_obj_t * cb2;
static lv_obj_t * cb3;

static lv_task_t *task;


static const uint8_t *list_value_x = "0\n1\n2\n3\n4\n5\n6\n7\n\8\n\9";
static const uint8_t *list_value_y = "1\n \n0\n \n1\n \n0\n \n1\n \n0";

static void update_chart(lv_task_t *t);

static uint8_t key1 = 0;
static uint8_t key2 = 0;
static uint8_t key3 = 0;


static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint8_t key = 0;
	switch (event)
	{
	case LV_EVENT_DELETE:
		if (task != NULL)
		{
			lv_task_del(task);
		}
		break;
	case LV_EVENT_KEY:
	{
		//printf("key:%d", (uint8_t)lv_event_get_data());
		key = (uint8_t)lv_event_get_data();
		switch (key)
		{
		case HAL_KEY_LEFT_CLICKED:
			key1 = 1;
			break;
		case HAL_KEY_LEFT_RELEASED:
			key1 = 0;
			break;
		case HAL_KEY_WKUP_CLICKED:
			key2 = 1;
			break;
		case HAL_KEY_WKUP_RELEASED:
			key2= 0;
			break;
		case HAL_KEY_RIGHT_CLICKED:
			key3 = 1;
			break;
		case HAL_KEY_RIGHT_RELEASED:
			key3 = 0;
			break;
		}

	}
		break;
	default:
		break;
	}
}


lv_obj_t * app_key_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	static lv_style_t style_label;
	static lv_style_t style_chart;
	static lv_style_t style_label_note1;
	static lv_style_t style_label_note2;
	static lv_style_t style_label_note3;

	lv_style_copy(&style_label, &lv_style_plain_color);
	if (gui_current_theme == THEME_NEMO)
		style_label.text.color = LV_COLOR_WHITE;
	else
		style_label.text.color = LV_COLOR_BLACK;
	style_label.text.font = &gb2312_puhui16;

	lv_style_copy(&style_chart, &lv_style_plain_color);
	if (gui_current_theme == THEME_NEMO)
		style_chart.text.color = LV_COLOR_WHITE;
	else
		style_chart.text.color = LV_COLOR_BLACK;


	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "按键测试");
	lv_obj_set_event_cb(obj, obj_event_cb);

	lv_obj_t * label;

	label = lv_label_create(obj, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label, "按下按键图表发生变化");
	lv_obj_align(label, obj, LV_ALIGN_IN_TOP_MID, 0, 70);

	lv_obj_t *chart = lv_chart_create(obj, NULL);		/* 创建图表控件 */
	lv_obj_align(chart, NULL, LV_ALIGN_IN_TOP_LEFT, 30, 120);	/* 设置位置 */
	lv_chart_set_point_count(chart, 10);					/* 设置显示的点数量 */
	lv_chart_set_series_width(chart, 4);					/* 设置线宽度 */
	lv_chart_set_range(chart, 0, 100);					/* 设置范围0-100 */
	lv_chart_set_div_line_count(chart, 9, 5);			/* 设置分割线的数量 */
	lv_chart_set_margin(chart, 50);						/* 设置标注的扩展长度 */
	lv_chart_set_type(chart, LV_CHART_TYPE_POINT | LV_CHART_TYPE_LINE);	/* 显示方式 显示点和线 */
	lv_chart_set_x_tick_texts(chart, list_value_x, 1, LV_CHART_AXIS_DRAW_LAST_TICK);	/* 设置标注的文本 */
	lv_obj_set_size(chart, 300, 300);						/* 设置控件尺寸 */
	lv_chart_set_x_tick_length(chart, 10, 10);			/* 刻度线长度 */
	lv_chart_set_y_tick_texts(chart, list_value_y, 1, LV_CHART_AXIS_DRAW_LAST_TICK);	/* 设置标注的文本 */
	lv_chart_set_y_tick_length(chart, 10, 10);			/* 刻度线长度 */
	lv_chart_set_style(chart, LV_CHART_STYLE_MAIN, &style_chart);


	ser_key1 = lv_chart_add_series(chart, LV_COLOR_BLUE);	/* 创建线条 */
	ser_key2 = lv_chart_add_series(chart, LV_COLOR_RED);	/* 创建线条 */
	ser_key3 = lv_chart_add_series(chart, LV_COLOR_YELLOW);	/* 创建线条 */

	lv_style_copy(&style_label_note1, &lv_style_plain_color);
	style_label_note1.text.color = LV_COLOR_BLACK;
	style_label_note1.body.radius = 10;
	//style_label_note1.body.opa = LV_OPA_20;

	lv_style_copy(&style_label_note2, &style_label_note1);
	lv_style_copy(&style_label_note3, &style_label_note1);

	style_label_note1.body.main_color = LV_COLOR_BLUE;
	style_label_note1.body.grad_color = LV_COLOR_BLUE;
	style_label_note2.body.main_color = LV_COLOR_RED;
	style_label_note2.body.grad_color = LV_COLOR_RED;
	style_label_note3.body.main_color = LV_COLOR_YELLOW;
	style_label_note3.body.grad_color = LV_COLOR_YELLOW;

	label = lv_label_create(obj, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_note1);
	lv_label_set_body_draw(label, true);
	lv_label_set_text(label, "KEY_LEFT");
	lv_obj_align(label, chart, LV_ALIGN_OUT_RIGHT_MID, 20, -30);

	label = lv_label_create(obj, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_note2);
	lv_label_set_body_draw(label, true);
	lv_label_set_text(label, "KEY_MID");
	lv_obj_align(label, chart, LV_ALIGN_OUT_RIGHT_MID, 20, 0);

	label = lv_label_create(obj, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_note3);
	lv_label_set_body_draw(label, true);
	lv_label_set_text(label, "KEY_RIGHT");
	lv_obj_align(label, chart, LV_ALIGN_OUT_RIGHT_MID, 20, 30);

	cb1 = lv_cb_create(obj, NULL);
	lv_cb_set_text(cb1,"KEY_LEFT");
	lv_obj_align(cb1, obj, LV_ALIGN_CENTER, -150, 80);

	cb2 = lv_cb_create(obj, NULL);
	lv_cb_set_text(cb2, "KEY_LEFT");
	lv_obj_align(cb2, obj, LV_ALIGN_CENTER, 0, 80);

	cb3 = lv_cb_create(obj, NULL);
	lv_cb_set_text(cb3, "KEY_RIGHT");
	lv_obj_align(cb3, obj, LV_ALIGN_CENTER, 150, 80);

	task = lv_task_create(update_chart, 100, LV_TASK_PRIO_LOW, chart);	/* 创建定期更新的任务 */

	return obj;
}


static void update_chart(lv_task_t *t)
{
	/* 更新图表 */

	lv_cb_set_checked(cb1,key1);
	lv_cb_set_checked(cb2, key2);
	lv_cb_set_checked(cb3, key3);

	if (key1)
		lv_chart_set_next(t->user_data, ser_key1, 20);
	else
		lv_chart_set_next(t->user_data, ser_key1, 0);

	if (key2)
		lv_chart_set_next(t->user_data, ser_key2, 60);
	else
		lv_chart_set_next(t->user_data, ser_key2, 40);

	if (key3)
		lv_chart_set_next(t->user_data, ser_key3, 100);
	else
		lv_chart_set_next(t->user_data, ser_key3, 80);

}



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

