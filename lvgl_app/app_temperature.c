/*
*******************************************************************************************************
*
* 文件名称 : app_temperature.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 温度应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"


static lv_obj_t *gauge_temp1;
static lv_obj_t *gauge_temp2;
static lv_obj_t *chart_temp1;
static lv_obj_t *chart_temp2;
static lv_chart_series_t *ser_temp1;
static lv_chart_series_t *ser_temp2;

static lv_task_t *task_update;

static void update_temperature(lv_task_t *t);

static const uint8_t *list_value_y = "60\n50\n40\n30\n20\n10\n0";

static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		if (task_update!=NULL)
		{
			lv_task_del(task_update);
		}
		break;
	default:
		break;
	}
}


lv_obj_t * app_temperature_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	static lv_color_t colors1[1];
	static lv_color_t colors2[1];

	static lv_style_t style_gauge;
	static lv_style_t style_label;
	static lv_style_t style_bg;
	static lv_style_t style_chart;



	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "温度");
	lv_obj_set_event_cb(obj, obj_event_cb);
	lv_style_copy(&style_bg, &lv_style_scr);
	lv_color_hex(0x272932);
	style_bg.body.main_color = lv_color_hex(0x272932);
	style_bg.body.grad_color = style_bg.body.main_color;
	//lv_obj_set_style(obj, &style_bg);

	lv_obj_t *obj_parent = lv_obj_create(obj,NULL);
	lv_obj_set_size(obj_parent, lv_obj_get_width(lv_obj_get_parent(obj_parent)), lv_obj_get_height(lv_obj_get_parent(obj_parent)) - 60);
	lv_obj_set_pos(obj_parent,0,60);
	lv_style_copy(&style_bg, &lv_style_scr);
	lv_color_hex(0x272932);
	style_bg.body.main_color = lv_color_hex(0x272932);
	style_bg.body.grad_color = style_bg.body.main_color;
	lv_obj_set_style(obj_parent, &style_bg);


	lv_style_copy(&style_label, &lv_style_scr);
	style_label.text.font = &lv_font_roboto_22;
	style_label.text.color = LV_COLOR_WHITE;

	lv_style_copy(&style_chart, &lv_style_pretty);
	//style_chart.text.font = &lv_font_roboto_22;
	//style_chart.text.color = LV_COLOR_WHITE;
	lv_color_hex(0x1e3547);
	style_chart.body.main_color = lv_color_hex(0x1e3547);
	style_chart.body.grad_color = style_chart.body.main_color;
	style_chart.text.color = LV_COLOR_WHITE;
	style_chart.line.color = LV_COLOR_WHITE;
	style_chart.line.width = 1;

	lv_style_copy(&style_gauge, &lv_style_pretty_color);
	style_gauge.body.radius = 5;
	style_gauge.line.width = 5;
	//style_gauge.body.main_color = lv_color_hex(0x228b22);
	//style_gauge.body.grad_color = lv_color_hex(0x4488bb);
	style_gauge.body.padding.left = 10;
	style_gauge.line.color = lv_color_hex(0xff4500); 
	style_gauge.text.color = LV_COLOR_WHITE;


	colors1[0] = lv_color_hex(0xcfcff2);
	colors2[0] = lv_color_hex(0xcfcff2);


	gauge_temp1 = lv_gauge_create(obj_parent, NULL);
	lv_obj_set_size(gauge_temp1, lv_obj_get_width(lv_obj_get_parent(gauge_temp1)) / 2 - 20, 230);
	lv_obj_set_pos(gauge_temp1, 10, 25);
	lv_gauge_set_range(gauge_temp1,0,60);
	lv_gauge_set_critical_value(gauge_temp1, 40);
	lv_gauge_set_needle_count(gauge_temp1, 1, colors1);
	lv_gauge_set_scale(gauge_temp1, 220, 25, 7);
	lv_gauge_set_style(gauge_temp1, LV_GAUGE_STYLE_MAIN, &style_gauge);

	gauge_temp2 = lv_gauge_create(obj_parent, NULL);
	lv_obj_set_size(gauge_temp2, lv_obj_get_width(lv_obj_get_parent(gauge_temp2)) / 2 - 20, 230);
	lv_obj_set_pos(gauge_temp2, lv_obj_get_width(lv_obj_get_parent(gauge_temp1)) / 2+10, 25);
	lv_gauge_set_range(gauge_temp2, 0, 60);
	lv_gauge_set_critical_value(gauge_temp2, 40);
	lv_gauge_set_needle_count(gauge_temp2, 1, colors2);
	lv_gauge_set_scale(gauge_temp2, 220, 25, 7);
	lv_gauge_set_style(gauge_temp2, LV_GAUGE_STYLE_MAIN, &style_gauge);


	lv_obj_t *label;
	label = lv_label_create(obj_parent, NULL);
	lv_label_set_text(label,"CPU");
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label,gauge_temp1,LV_ALIGN_OUT_TOP_MID,0, 0);

	label = lv_label_create(obj_parent, NULL);
	lv_label_set_text(label, "MPU6050");
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label, gauge_temp2, LV_ALIGN_OUT_TOP_MID, 0, 0);



	chart_temp1 = lv_chart_create(obj_parent, NULL);
	lv_obj_set_size(chart_temp1, 400, 250);
	lv_obj_align(chart_temp1, obj_parent, LV_ALIGN_IN_BOTTOM_MID, 10, -280);
	lv_chart_set_point_count(chart_temp1, 20);
	lv_chart_set_range(chart_temp1, 0, 60);
	lv_chart_set_div_line_count(chart_temp1, 5, 0);
	lv_chart_set_y_tick_texts(chart_temp1, list_value_y, 1, LV_CHART_AXIS_DRAW_LAST_TICK);
	lv_chart_set_margin(chart_temp1, 50);
	lv_chart_set_type(chart_temp1, LV_CHART_TYPE_POINT | LV_CHART_TYPE_LINE | LV_CHART_TYPE_AREA);	/* 显示方式 */
	lv_chart_set_series_width(chart_temp1, 4);					/* 设置线宽度 */
	lv_chart_set_y_tick_length(chart_temp1, 5, 5);			/* 刻度线长度 */
	lv_chart_set_series_opa(chart_temp1, LV_OPA_50);
	lv_chart_set_style(chart_temp1, LV_CHART_STYLE_MAIN, &style_chart);

	ser_temp1 = lv_chart_add_series(chart_temp1, lv_color_hex(0xcfcff2));	/* 创建线条 */

	chart_temp2 = lv_chart_create(obj_parent, NULL);
	lv_obj_set_size(chart_temp2, 400, 250);
	lv_obj_align(chart_temp2, obj_parent, LV_ALIGN_IN_BOTTOM_MID, 10, -5);
	lv_chart_set_point_count(chart_temp2, 20);
	lv_chart_set_range(chart_temp2, 0, 60);
	lv_chart_set_div_line_count(chart_temp2, 5, 0);
	lv_chart_set_y_tick_texts(chart_temp2, list_value_y, 1, LV_CHART_AXIS_DRAW_LAST_TICK);
	lv_chart_set_margin(chart_temp2, 50);
	lv_chart_set_type(chart_temp2, LV_CHART_TYPE_POINT | LV_CHART_TYPE_LINE | LV_CHART_TYPE_AREA);	/* 显示方式 */
	lv_chart_set_series_width(chart_temp2, 4);					/* 设置线宽度 */
	lv_chart_set_y_tick_length(chart_temp2, 5, 5);			/* 刻度线长度 */
	lv_chart_set_series_opa(chart_temp2, LV_OPA_50);
	lv_chart_set_style(chart_temp2, LV_CHART_STYLE_MAIN, &style_chart);

	ser_temp2 = lv_chart_add_series(chart_temp2, lv_color_hex(0x9494ce));	/* 创建线条 */


	task_update = lv_task_create(update_temperature, 1000, LV_TASK_PRIO_LOW, NULL);	/* 创建定期更新的任务 */

	return obj;
}

static void update_temperature(lv_task_t *t)
{
	float cpu_temp = 0;
	float mpu6050_temp = 0;
	cpu_temp = gui_hal_get_cpu_temperature();
	mpu6050_temp = gui_hal_get_mpu6050_temperature();

	if (cpu_temp <= lv_gauge_get_max_value(gauge_temp1))
	{
		lv_gauge_set_value(gauge_temp1, 0, cpu_temp);
		lv_chart_set_next(chart_temp1, ser_temp1, cpu_temp);
	}

	if (mpu6050_temp <= lv_gauge_get_max_value(gauge_temp2))
	{
		lv_gauge_set_value(gauge_temp2, 0, mpu6050_temp);
		lv_chart_set_next(chart_temp2, ser_temp2, mpu6050_temp);
	}
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

