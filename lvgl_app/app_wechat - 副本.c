﻿/*
*******************************************************************************************************
*
* 文件名称 : app_wechat.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 微信应用
*
*******************************************************************************************************
*/

#include "gui_user.h"



icon_item icon_wechat_page_chats[13] =
{
	{ &wechat_tiaotu, "tiaotu", "跳兔", "0:/lvgl/wechat_tiaotu.bin", NULL },
	{ &wechat_aliyun, "aliyun", "跳兔", "0:/lvgl/wechat_aliyun.bin", NULL },
	{ &wechat_armfly, "armfly", "跳兔", "0:/lvgl/wechat_armfly.bin", NULL },
	{ &wechat_ebf, "ebf", "跳兔", "0:/lvgl/wechat_ebf.bin", NULL },
	{ &wechat_esp, "esp", "跳兔", "0:/lvgl/wechat_esp.bin", NULL },
	{ &wechat_huxiaozhi, "huxiaozhi", "跳兔", "0:/lvgl/wechat_huxiaozhi.bin", NULL },
	{ &wechat_mi, "mi", "跳兔", "0:/lvgl/wechat_mi.bin", NULL },
	{ &wechat_myir, "myir", "跳兔", "0:/lvgl/wechat_myir.bin", NULL },
	{ &wechat_nuclei, "nuclei", "跳兔", "0:/lvgl/wechat_nuclei.bin", NULL },
	{ &wechat_onenet, "onenet", "跳兔", "0:/lvgl/wechat_onenet.bin", NULL },
	{ &wechat_quectel, "quectel", "跳兔", "0:/lvgl/wechat_quectel.bin", NULL },
	{ &wechat_rtt, "rtt", "跳兔", "0:/lvgl/wechat_rtt.bin", NULL },
	{ &wechat_st, "st", "跳兔", "0:/lvgl/wechat_st.bin", NULL },
};
icon_item icon_wechat_page_contacts[4] =
{
	{ &wechat_newfriend, "newfriend", "跳兔", "0:/lvgl/wechat_newfriend.bin", NULL },
	{ &wechat_groupchat, "groupchat", "跳兔", "0:/lvgl/wechat_groupchat.bin", NULL },
	{ &wechat_label, "label", "跳兔", "0:/lvgl/wechat_label.bin", NULL },
	{ &wechat_public, "publie", "跳兔", "0:/lvgl/wechat_public.bin", NULL },
};

icon_item icon_wechat_page_discover[8] =
{
	{ &wechat_moments, "moments", "跳兔", "0:/lvgl/wechat_moments.bin", NULL },
	{ &wechat_scan, "scan", "跳兔", "0:/lvgl/wechat_scan.bin", NULL },
	{ &wechat_shake, "shake", "跳兔", "0:/lvgl/wechat_shake.bin", NULL },
	{ &wechat_top_stories, "top_stories", "跳兔", "0:/lvgl/wechat_top_stories.bin", NULL },
	{ &wechat_search, "search", "跳兔", "0:/lvgl/wechat_search.bin", NULL },
	{ &wechat_people_nearby, "people_nearby", "跳兔", "0:/lvgl/wechat_people_nearby.bin", NULL },
	{ &wechat_games, "games", "跳兔", "0:/lvgl/wechat_games.bin", NULL },
	{ &wechat_mini_programs, "mini_programs", "跳兔", "0:/lvgl/wechat_mini_programs.bin", NULL },

};
icon_item icon_wechat_page_me[7] =
{
	{ &wechat_id, "id", "跳兔", "0:/lvgl/wechat_id.bin", NULL },
	{ &wechat_pay, "pay", "跳兔", "0:/lvgl/wechat_pay.bin", NULL },
	{ &wechat_favorites, "favorites", "跳兔", "0:/lvgl/wechat_favorites.bin", NULL },
	{ &wechat_my_posts, "my_posts", "跳兔", "0:/lvgl/wechat_my_posts.bin", NULL },
	{ &wechat_cards_offers, "cards_offers", "跳兔", "0:/lvgl/wechat_cards_offers.bin", NULL },
	{ &wechat_sticker_gallery, "sticker_gallery", "跳兔", "0:/lvgl/wechat_sticker_gallery.bin", NULL },
	{ &wechat_settings, "settings", "跳兔", "0:/lvgl/wechat_settings.bin", NULL },

};


icon_item icon_wechat_header[4] =
{
	{ &wechat_chats0, "chats0", "跳兔", "0:/lvgl/wechat_chats0.bin", NULL },
	{ &wechat_contacts0, "contacts0", "跳兔", "0:/lvgl/wechat_contacts0.bin", NULL },
	{ &wechat_discover0, "discover0", "跳兔", "0:/lvgl/wechat_discover0.bin", NULL },
	{ &wechat_me0, "me0", "跳兔", "0:/lvgl/wechat_me0.bin", NULL },

};










static void btn_back_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED)
	{
		/* 退出窗口 */
		lv_obj_del(lv_obj_get_parent(obj));
	}
}


lv_obj_t * app_wechat_create(void *user_data)
{
	(void)user_data;

	uint8_t i = 0;

	static lv_style_t style_btn_back;
	static lv_style_t style_label_back;
	static lv_style_t style_label_title;
	static lv_style_t style_bg;
	static lv_style_t style_list;
	static lv_style_t style_tabview;

	lv_style_copy(&style_btn_back, &lv_style_transp);
	lv_style_copy(&style_bg, &lv_style_scr);
	lv_style_copy(&style_label_title, &lv_style_scr);
	lv_style_copy(&style_label_back, &lv_style_transp);
	style_label_back.text.font = &lv_font_roboto_28;
	style_label_back.text.color = LV_COLOR_WHITE;
	style_bg.body.main_color = lv_color_hex(0x32323a);
	style_bg.body.grad_color = lv_color_hex(0x32323a);

	lv_obj_t *obj = lv_obj_create(lv_scr_act(), NULL);
	lv_obj_set_size(obj, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_style(obj, &style_bg);

	lv_obj_t *btn_back = lv_btn_create(obj, NULL);
	lv_obj_set_size(btn_back, 100, 60);
	lv_obj_set_event_cb(btn_back, btn_back_event_cb);
	lv_btn_set_style(btn_back, LV_BTN_STYLE_REL, &style_btn_back);
	lv_btn_set_style(btn_back, LV_BTN_STYLE_PR, &style_btn_back);

	lv_obj_t *label_back = lv_label_create(obj, NULL);
	lv_label_set_style(label_back, LV_LABEL_STYLE_MAIN, &style_label_back);
	lv_label_set_text(label_back, LV_SYMBOL_LEFT);
	//lv_obj_set_pos(label_back,20,20);
	lv_obj_align(label_back, btn_back, LV_ALIGN_IN_LEFT_MID, 20, 0);


	lv_obj_t *label_title = lv_label_create(obj, NULL);
	style_label_title.text.color = LV_COLOR_WHITE;
	style_label_title.text.font = &lv_font_roboto_28;
	lv_label_set_style(label_title, LV_LABEL_STYLE_MAIN, &style_label_title);
	lv_label_set_text(label_title, "微信");
	lv_obj_align(label_title, obj, LV_ALIGN_IN_TOP_MID, 0, 16);


	lv_obj_t *tabview;
	tabview = lv_tabview_create(obj, NULL);
	lv_obj_set_pos(tabview, 0, 60);
	lv_obj_set_size(tabview, LV_HOR_RES, LV_VER_RES-60-80);
	//lv_tabview_set_btns_pos(tabview, LV_TABVIEW_BTNS_POS_BOTTOM);
	lv_tabview_set_btns_hidden(tabview,true);


	lv_style_copy(&style_tabview,&lv_style_plain);
	style_tabview.body.border.width = 0;

	lv_tabview_set_style(tabview, LV_TABVIEW_STYLE_BG, &style_tabview);

	lv_obj_t *page_chats = lv_tabview_add_tab(tabview, "Chats");
	lv_obj_t *page_contacts = lv_tabview_add_tab(tabview, "Contacts");
	lv_obj_t *page_discover = lv_tabview_add_tab(tabview, "Discover");
	lv_obj_t *page_me = lv_tabview_add_tab(tabview, "Me");

	lv_tabview_set_tab_act(tabview,1,true);


	lv_page_set_style(page_chats, LV_PAGE_STYLE_BG, &lv_style_transp_fit);
	lv_page_set_style(page_chats, LV_PAGE_STYLE_SCRL, &lv_style_transp_fit);

	lv_page_set_sb_mode(page_chats, LV_SB_MODE_OFF);

	

	lv_obj_t * list = lv_list_create(page_chats, NULL);
	//lv_obj_set_pos(list, -10, -10);
	lv_obj_set_size(list, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL)-120);
	lv_obj_t * list_btn;

	for (i = 0; i < sizeof(icon_wechat_page_chats) / sizeof(icon_wechat_page_chats[0]); i++)
	{
		lv_list_add_btn(list, icon_wechat_page_chats[i].icon_img, icon_wechat_page_chats[i].text_cn);
	}

	lv_obj_t *btn_header;
	for ( i = 0; i < 4; i++)
	{
		btn_header = lv_btn_create(obj,NULL);
		lv_obj_set_size(btn_header,120,80);
		lv_obj_set_pos(btn_header, 120 * i, LV_VER_RES - 80);
		lv_btn_set_style(btn_header,LV_BTN_STATE_REL,&lv_style_scr);

		lv_obj_t *image = lv_img_create(btn_header, NULL);
		lv_img_set_src(image, icon_wechat_header[i].icon_img);


	}



	return obj;
}











/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

