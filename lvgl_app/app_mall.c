/*
*******************************************************************************************************
*
* 文件名称 : app_mall.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 商城应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"




static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		break;
	default:
		break;
	}
}


lv_obj_t * app_mall_create(lv_obj_t *parent,void *user_data)
{
	(void)user_data;

	static lv_style_t style_page;
	static lv_style_t style_label;
	static lv_style_t style_bg;
	static lv_style_t style_ta;
	static lv_style_t style_cont1;
	

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "跳兔商城");
	lv_obj_set_event_cb(obj, obj_event_cb);

	lv_style_copy(&style_bg, &lv_style_scr);
	style_bg.body.main_color = lv_color_hex(0xff1727);
	style_bg.body.grad_color = style_bg.body.main_color;
	lv_obj_set_style(obj, &style_bg);

	lv_style_copy(&style_page, &lv_style_scr);
	style_page.body.main_color = lv_color_hex(0xff1727);
	style_page.body.grad_color = style_page.body.main_color;
	style_page.body.border.width = 0;
	style_page.body.border.part = LV_BORDER_FULL;
	style_page.body.padding.top = 0;
	style_page.body.padding.bottom = 0;
	style_page.body.padding.left = 0;
	style_page.body.padding.right = 0;
	style_page.body.padding.inner = 0;

	lv_style_copy(&style_ta, &lv_style_pretty);
	style_ta.body.radius = LV_RADIUS_CIRCLE;
	style_ta.body.main_color = LV_COLOR_WHITE;
	style_ta.body.grad_color = LV_COLOR_WHITE;
	style_ta.body.padding.left = 16;
	style_ta.body.padding.right = 16;
	style_ta.body.padding.top = 15;
	style_ta.body.padding.bottom = 0;
	style_ta.body.padding.inner = 10;
	style_ta.text.color = lv_color_hex3(0x333);
	style_ta.image.color = lv_color_hex3(0x333);
	style_ta.text.font = &gb2312_puhui16;

	lv_style_copy(&style_cont1, &lv_style_pretty);
	style_cont1.body.main_color = LV_COLOR_WHITE;
	style_cont1.body.grad_color = LV_COLOR_WHITE;
	style_cont1.body.radius = 10;
	style_cont1.body.padding.top = 10;
	style_cont1.body.padding.bottom = 10;
	style_cont1.body.padding.left = 10;
	style_cont1.body.padding.right = 10;
	style_cont1.body.border.width = 0;


	lv_obj_t *page = lv_page_create(obj, NULL);
	lv_obj_set_pos(page,0,60+30);
	lv_obj_set_size(page, lv_obj_get_width(lv_obj_get_parent(page)), lv_obj_get_height(lv_obj_get_parent(page))-60);
	lv_page_set_style(page, LV_PAGE_STYLE_BG, &style_page);

	lv_obj_t *ta = lv_ta_create(obj, NULL);
	lv_obj_set_size(ta,lv_obj_get_width(ta),30);
	lv_obj_align(ta, obj, LV_ALIGN_IN_TOP_MID, 0, 60);
	lv_ta_set_style(ta, LV_TA_STYLE_BG, &style_ta);
	lv_ta_set_text(ta,"跳兔");

	lv_obj_t *cont1 = lv_cont_create(page,NULL);
	lv_cont_set_fit2(cont1, LV_FIT_NONE, LV_FIT_TIGHT);
	lv_cont_set_style(cont1, LV_CONT_STYLE_MAIN, &style_cont1);
	lv_obj_set_width(cont1, lv_page_get_fit_width(page));




	return obj;
}



static const lv_style_t style_page1 = {

	0
};


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

