/*
*******************************************************************************************************
*
* 文件名称 : app_nb_iot.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : NB信号测试仪应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

#ifdef NB_IOT

/* 消息队列的头 */
typedef enum{
	NB_INIT = 0,
	NB_DATA_INFO,
	NB_UART_INFO,
	NB_UART_SEND,
	NB_DATA_GET,
}gui_nb_mq_header;

static lv_obj_t * label_service;	//运营商
static lv_obj_t * label_imsi;		//卡号
static lv_obj_t * label_imei;		//模组号
static lv_obj_t * label_csq;		//信号



static lv_obj_t * label_signal_power;	//信号功率
static lv_obj_t * label_total_power;	//总功率
static lv_obj_t * label_tx_power;		//发射功率
static lv_obj_t * label_tx_time;		//发射时间
static lv_obj_t * label_rx_time;		//接收时间
static lv_obj_t * label_cell_id;		//基站小区标识
static lv_obj_t * label_ecl;			//覆盖增强等级
static lv_obj_t * label_snr;		//信噪比
static lv_obj_t * label_earfcn;		//中心频点
static lv_obj_t * label_pci;		//物理小区标识
static lv_obj_t * label_rsrq;		//参考信号接收质量


/*
Signal power : -763
Total power : -695
TX power : 0
TX time : 1192
RX time : 24037
Cell ID : 67516757
ECL : 0
SNR : 200
EARFCN : 2507中
PCI : 283
RSRQ : -108
OPERATOR MODE : 4
*/

static lv_obj_t * page_parent;		//总page
static lv_obj_t * page_info;		//串口数据显示区
static lv_obj_t * label_info;		//串口数据显示区
static lv_obj_t * label_data;		//参数显示区
static lv_obj_t * ta_input;			//AT指令输入区域
static lv_obj_t * btn_clear;		//清除接收按钮
static lv_obj_t * kb;				//键盘
static lv_obj_t * cb_newline;		//换行发送
static lv_obj_t * cb_pause_recv;		//暂停接收

static uint8_t nb_data_buf[300];		//参数显示缓冲区

static _nb_iot_info gui_nb_iot_info;
static uint8_t hal_mq_send_buf[32] = {0};		//消息队列
//static uint8_t hal_mq_recv_buf[512];		//消息队列

static void btn_clear_event_cb(lv_obj_t * obj, lv_event_t event);
static void kb_event_cb(lv_obj_t * obj, lv_event_t event);
static void cb_pause_recv_event_cb(lv_obj_t * obj, lv_event_t event);


static void nb_info_update(lv_task_t *t);
static void nb_info_enable(lv_task_t *t);

static void label_data_info_update(_nb_iot_info *my_gui_nb_iot_info);
static void label_uart_info_append(uint8_t *text);

static lv_task_t *task_nb_enable;
static lv_task_t *task_nb_recv;

static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		if (task_nb_recv != NULL)
		{
			lv_task_del(task_nb_recv);
		}
		if (task_nb_enable != NULL)
		{
			lv_task_del(task_nb_enable);
		}
		break;
	default:
		break;
	}
}


lv_obj_t * app_nb_iot_create(lv_obj_t *parent,void *user_data)
{
	printf("\n size:%d \n", sizeof(_nb_iot_info));

	(void)user_data;

	uint16_t x_pos = 0, y_pos = 0;
	uint8_t y_blank = 20;

	static lv_style_t style_label;
	static lv_style_t style_bg;
	static lv_style_t style_ta;
	static lv_style_t style_cb_bg;

	lv_style_copy(&style_bg, &lv_style_scr);
	style_bg.body.main_color = lv_color_hex(0x004874e8);;
	style_bg.body.grad_color = lv_color_hex(0x004874e8);;

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "NB信号测试");
	lv_obj_set_event_cb(obj, obj_event_cb);
	lv_obj_set_style(obj, &style_bg);

	lv_style_copy(&style_label, &lv_style_plain_color);
	if (gui_current_theme == THEME_NEMO)
		style_label.text.color = LV_COLOR_WHITE;
	else
		style_label.text.color = LV_COLOR_BLACK;
	style_label.text.font = &gb2312_puhui16;

	page_parent = lv_page_create(obj, NULL);
	lv_obj_set_size(page_parent, lv_obj_get_width(obj), lv_obj_get_height(obj)-60);
	lv_obj_align(page_parent, obj, LV_ALIGN_IN_TOP_MID, 0, 60);

	x_pos = 10;
	y_pos = 10;

	label_service = lv_label_create(page_parent, NULL);
	lv_label_set_style(label_service, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_service,"运营商家");
	lv_obj_set_pos(label_service, x_pos, y_pos);

	label_csq = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_csq,true);
	lv_label_set_style(label_csq, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_csq, "CSQ: #FF0000  #");
	lv_obj_set_pos(label_csq, 130, y_pos);

	y_pos += y_blank;
	label_imei = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_imei, true);
	lv_label_set_style(label_imei, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_imei, "模组号 imei:");
	lv_obj_set_pos(label_imei, x_pos, y_pos);

	y_pos += y_blank;
	label_imsi = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_imsi, true);
	lv_label_set_style(label_imsi, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_imsi, "卡号 imsi:");
	lv_obj_set_pos(label_imsi, x_pos, y_pos);

	y_pos += y_blank;
	label_signal_power = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_signal_power, true);
	lv_label_set_style(label_signal_power, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_signal_power, "信号功率 Signal power");
	lv_obj_set_pos(label_signal_power, x_pos, y_pos);

	y_pos += y_blank;
	label_total_power = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_total_power, true);
	lv_label_set_style(label_total_power, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_total_power, "总功率 Total power");
	lv_obj_set_pos(label_total_power, x_pos, y_pos);

	y_pos += y_blank;
	label_tx_power = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_tx_power, true);
	lv_label_set_style(label_tx_power, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_tx_power, "发射功率 TX power");
	lv_obj_set_pos(label_tx_power, x_pos, y_pos);

	y_pos += y_blank;
	label_tx_time = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_tx_time, true);
	lv_label_set_style(label_tx_time, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_tx_time, "发射时间 TX time");
	lv_obj_set_pos(label_tx_time, x_pos, y_pos);

	y_pos += y_blank;
	label_rx_time = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_rx_time, true);
	lv_label_set_style(label_rx_time, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_rx_time, "接收时间 RX time");
	lv_obj_set_pos(label_rx_time, x_pos, y_pos);

	y_pos += y_blank;
	label_cell_id = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_cell_id, true);
	lv_label_set_style(label_cell_id, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_cell_id, "基站小区标识 Cell ID");
	lv_obj_set_pos(label_cell_id, x_pos, y_pos);

	y_pos += y_blank;
	label_ecl = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_ecl, true);
	lv_label_set_style(label_ecl, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_ecl, "覆盖增强等级 ECL");
	lv_obj_set_pos(label_ecl, x_pos, y_pos);

	y_pos += y_blank;
	label_snr = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_snr, true);
	lv_label_set_style(label_snr, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_snr, "信噪比 SNR");
	lv_obj_set_pos(label_snr, x_pos, y_pos);

	y_pos += y_blank;
	label_earfcn = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_earfcn, true);
	lv_label_set_style(label_earfcn, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_earfcn, "中心频点 EARFCN");
	lv_obj_set_pos(label_earfcn, x_pos, y_pos);

	y_pos += y_blank;
	label_pci = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_pci, true);
	lv_label_set_style(label_pci, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_pci, "物理小区标识 PCI");
	lv_obj_set_pos(label_pci, x_pos, y_pos);

	y_pos += y_blank;
	label_rsrq = lv_label_create(page_parent, NULL);
	lv_label_set_recolor(label_rsrq, true);
	lv_label_set_style(label_rsrq, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label_rsrq, "参考信号接收质量 RSRQ");
	lv_obj_set_pos(label_rsrq, x_pos, y_pos);


	//label_data = lv_label_create(page_parent, NULL);
	//lv_label_set_long_mode(label_data, LV_LABEL_LONG_BREAK);
	//lv_obj_set_width(label_data, lv_page_get_fit_width(page_parent));
	//lv_label_set_style(label_data, LV_LABEL_STYLE_MAIN, &style_label);
	////lv_label_set_text(label_rsrq, "参考信号接收质量 RSRQ");
	//sprintf((char*)nb_data_buf,"%s     CSQ:%s\n\
	//						   信号功率 Signal power:%s\n\
	//						   总功率 Total power:%s\n\
	//						   发射功率 TX power:%s\n\
	//						   发射时间 TX time:%s\n\
	//						   接收时间 RX time:%s\n\
	//						   基站小区标识 Cell ID:%s\n\
	//						   覆盖增强等级 ECL:%s\n\
	//						   信噪比 SN:%s\n\
	//						   中心频点 EARFCN:%s\n\
	//						   物理小区标识 PCI:%s\n\
	//						   参考信号接收质量 RSRQ:%s\n\",
	//						   );

	page_info = lv_page_create(page_parent, NULL);
	lv_obj_set_size(page_info, lv_page_get_fit_width(page_parent), (lv_obj_get_height(page_parent) / 2)-30);
	//lv_obj_align(page_info, page_parent, LV_ALIGN_IN_BOTTOM_MID, 0, 0);
	lv_obj_set_y(page_info,300);
	lv_page_set_scroll_propagation(page_info, true);	/* 滑动到底时,带动父窗口移动 */

	label_info = lv_label_create(page_info,NULL);
	lv_label_set_long_mode(label_info, LV_LABEL_LONG_BREAK);
	lv_obj_set_width(label_info, lv_page_get_fit_width(page_info));
	lv_label_set_text(label_info, "receive");


	btn_clear = lv_btn_create(page_parent, NULL);
	lv_obj_t *label = lv_label_create(btn_clear, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_text(label, "清除接收");
	lv_obj_align(btn_clear, page_info, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 5);
	lv_obj_set_height(btn_clear,40);
	lv_obj_set_event_cb(btn_clear,btn_clear_event_cb);


	cb_newline = lv_cb_create(page_parent, NULL);
	lv_style_copy(&style_cb_bg, &lv_style_plain_color);
	//lv_style_copy(&style_cb_bg, lv_theme_get_current()->style.cb.bg);
	style_cb_bg.text.font = &gb2312_puhui16;
	style_cb_bg.text.color = LV_COLOR_BLACK;
	style_cb_bg.body.opa = 0;
	lv_cb_set_style(cb_newline, LV_CB_STYLE_BG, &style_cb_bg);
	lv_cb_set_text(cb_newline, "新行");
	lv_obj_align(cb_newline, btn_clear, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
	lv_obj_set_height(cb_newline, 40);


	cb_pause_recv = lv_cb_create(page_parent, NULL);
	lv_cb_set_style(cb_pause_recv, LV_CB_STYLE_BG, &style_cb_bg);
	lv_cb_set_text(cb_pause_recv, "暂停接收");
	lv_obj_align(cb_pause_recv, cb_newline, LV_ALIGN_OUT_RIGHT_MID, 10, 0);
	lv_obj_set_height(cb_pause_recv, 40);

	//lv_style_copy(&style_ta, lv_theme_get_current()->style.ta.area);
	ta_input = lv_ta_create(page_parent, NULL);
	lv_obj_set_height(ta_input,65);
	lv_obj_set_width(ta_input, lv_page_get_fit_width(page_parent));
	lv_obj_align(ta_input, page_info, LV_ALIGN_OUT_BOTTOM_MID, 0, 50);
	lv_ta_set_text(ta_input,"AT");
	lv_ta_set_max_length(ta_input,25);
	//lv_obj_set_y(ta_input, 540);

	/* 创建键盘 */
	kb = lv_kb_create(page_parent, NULL);
	lv_obj_set_event_cb(kb, kb_event_cb);
	lv_obj_set_height(kb, LV_VER_RES / 3);
	lv_obj_align(kb, ta_input, LV_ALIGN_OUT_BOTTOM_MID, 0, 5);

	/* 设置键盘对应的输入区域 */
	lv_kb_set_ta(kb, ta_input);
	lv_kb_set_cursor_manage(kb, true);


	/* 查询的task */
	task_nb_recv = lv_task_create(nb_info_update, 10, LV_TASK_PRIO_LOW, NULL);
	/* 通知RTOS进行采集的task */
	task_nb_enable = lv_task_create(nb_info_enable, 1000, LV_TASK_PRIO_LOW, NULL);

	/*通知RTOS UI已经准备完成  */
	gui_hal_nb_message_send(hal_mq_send_buf, sizeof(hal_mq_send_buf));

	return obj;
}


static void kb_event_cb(lv_obj_t * obj, lv_event_t event)
{
	
	if (event == LV_EVENT_APPLY)
	{
		memset(hal_mq_send_buf, 0, sizeof(hal_mq_send_buf));

		hal_mq_send_buf[0] = NB_UART_SEND;
		strcat(hal_mq_send_buf, lv_ta_get_text(ta_input));
		strcat(hal_mq_send_buf, "\r\n");
		////memcpy(hal_mq_send_buf + 1, lv_ta_get_text(ta_input), lv_ta_get_);
		////lv_ta_get_text(ta_input);
		//if (lv_cb_is_checked(cb_newline))
		//{
		//	sprintf(hal_mq_send_buf, "%d%s\r\n", NB_UART_SEND, lv_ta_get_text(ta_input));
		//}
		//else
		//{
		//	sprintf(hal_mq_send_buf, "%d%s", NB_UART_SEND, lv_ta_get_text(ta_input));
		//}
		gui_hal_nb_message_send(hal_mq_send_buf, sizeof(hal_mq_send_buf));
	}
	/* Just call the regular event handler */
	lv_kb_def_event_cb(obj, event);

}


static void btn_clear_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED)
	{
		lv_label_set_text(label_info,"");
		
	}
}

static void nb_info_update(lv_task_t *t)
{
	static uint8_t mq_recv_buf[512];
	static uint8_t label_buf[128];
	if (gui_hal_nb_message_recv(mq_recv_buf, sizeof(mq_recv_buf)) == 0)
	{
		if (mq_recv_buf[0] == NB_DATA_INFO)
		{
			memcpy(&gui_nb_iot_info, (mq_recv_buf + 1), sizeof(gui_nb_iot_info));
			label_data_info_update(&gui_nb_iot_info);
		}
		else if(mq_recv_buf[0] == NB_UART_INFO)
		{
			lv_label_set_text(label_info, mq_recv_buf+1);
		}
	}
}

static void nb_info_enable(lv_task_t *t)
{
	if (!lv_cb_is_checked(cb_pause_recv))
	{
		hal_mq_send_buf[0] = NB_DATA_GET;
		/*通知RTOS UI已经准备完成  */
		gui_hal_nb_message_send(hal_mq_send_buf, sizeof(hal_mq_send_buf));
	}
}

static void label_uart_info_append(uint8_t *text)
{
	//lv_label_ins_text(label_info,lv_label_get_text_sel_end(), text);
}

static void label_data_info_update(_nb_iot_info *my_gui_nb_iot_info)
{
	static uint8_t label_buf[128];
	if (my_gui_nb_iot_info->band[0] == '8')
	{
		sprintf((char*)label_buf, "%s", "中国移动");
		lv_label_set_text(label_service, (const char*)label_buf);
	}
	else if (my_gui_nb_iot_info->band[0] == '5')
	{
		sprintf((char*)label_buf, "%s", "中国电信");
		lv_label_set_text(label_service, (const char*)label_buf);
	}

	sprintf((char*)label_buf, "信号 CSQ: #FF0000 %s#", my_gui_nb_iot_info->csq);
	lv_label_set_text(label_csq, (const char*)label_buf);

	sprintf((char*)label_buf, "模组号 imei: #FF0000 %s#", my_gui_nb_iot_info->imei);
	lv_label_set_text(label_imei, (const char*)label_buf);

	sprintf((char*)label_buf, "卡号 imsi: #FF0000 %s#", my_gui_nb_iot_info->imsi);
	lv_label_set_text(label_imsi, (const char*)label_buf);

	sprintf((char*)label_buf, "信号功率 Signal power: #FF0000 %s#", my_gui_nb_iot_info->signal_power);
	lv_label_set_text(label_signal_power, (const char*)label_buf);

	sprintf((char*)label_buf, "总功率 Total power: #FF0000 %s#", my_gui_nb_iot_info->total_power);
	lv_label_set_text(label_total_power, (const char*)label_buf);

	sprintf((char*)label_buf, "发射功率 TX power: #FF0000 %s#", my_gui_nb_iot_info->tx_power);
	lv_label_set_text(label_tx_power, (const char*)label_buf);

	sprintf((char*)label_buf, "发射时间 TX time: #FF0000 %s#", my_gui_nb_iot_info->tx_time);
	lv_label_set_text(label_tx_time, (const char*)label_buf);

	sprintf((char*)label_buf, "接收时间 RX time: #FF0000 %s#", my_gui_nb_iot_info->rx_time);
	lv_label_set_text(label_rx_time, (const char*)label_buf);

	sprintf((char*)label_buf, "基站小区标识 Cell ID: #FF0000 %s#", my_gui_nb_iot_info->cell_id);
	lv_label_set_text(label_cell_id, (const char*)label_buf);

	sprintf((char*)label_buf, "覆盖增强等级 ECL: #FF0000 %s#", my_gui_nb_iot_info->ecl);
	lv_label_set_text(label_ecl, (const char*)label_buf);

	sprintf((char*)label_buf, "信噪比 SNR: #FF0000 %s#", my_gui_nb_iot_info->snr);
	lv_label_set_text(label_snr, (const char*)label_buf);

	sprintf((char*)label_buf, "中心频点 EARFCN: #FF0000 %s#", my_gui_nb_iot_info->earfcn);
	lv_label_set_text(label_earfcn, (const char*)label_buf);

	sprintf((char*)label_buf, "物理小区标识 PCI: #FF0000 %s#", my_gui_nb_iot_info->pci);
	lv_label_set_text(label_pci, (const char*)label_buf);

	sprintf((char*)label_buf, "参考信号接收质量 RSRQ: #FF0000 %s#", my_gui_nb_iot_info->rsrq);
	lv_label_set_text(label_rsrq, (const char*)label_buf);
}

static void cb_pause_recv_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		
	}
}
#else
lv_obj_t * app_nb_iot_create(lv_obj_t *parent, void *user_data)
{
	message_box(lv_scr_act(), "抱歉,您的设备暂时不支持该功能");
	return lv_scr_act();
}
#endif


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

