/*
*******************************************************************************************************
*
* 文件名称 : app_wechat.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 微信应用
*
*******************************************************************************************************
*/

#include "gui_user.h"



icon_item icon_wechat_page_chats[20] =
{
	{ &wechat_tiaotu, "tiaotu", "跳兔",  NULL },
	{ &wechat_aliyun, "aliyun", "阿里",  NULL },
	{ &wechat_armfly, "armfly", "安富",  NULL },
	{ &wechat_ebf, "ebf", "野火",  NULL },
	{ &wechat_esp, "esp", "安信",  NULL },
	{ &wechat_huxiaozhi, "huxiaozhi", "湖职",  NULL },
	{ &wechat_mi, "mi", "小米",  NULL },
	{ &wechat_myir, "myir", "米尔",  NULL },
	{ &wechat_nuclei, "nuclei", "芯来",  NULL },
	{ &wechat_onenet, "onenet", "中移物联",  NULL },
	{ &wechat_quectel, "quectel", "移远通信",  NULL },
	{ &wechat_rtt, "rtt", "RT-Thread",  NULL },
	{ &wechat_st, "st", "ST",  NULL },
	{ &wechat_asus, "asus", "华硕",  NULL },
	{ &wechat_21ic, "21ic", "21ic电子网站",  NULL },
	{ &wechat_jingdong, "jingdong", "京东",  NULL },
	{ &wechat_grouphelp, "grouphelp", "公众平台安全助手",  NULL },
	{ &wechat_shunfeng, "shunfeng", "顺丰",  NULL },
	{ &wechat_exmail, "exmail", "腾讯企业邮箱",  NULL },
	{ &wechat_lcsc, "lcsc", "立创商城",  NULL },

};
icon_item icon_wechat_page_contacts[4] =
{
	{ &wechat_newfriend, "newfriend", "新的朋友",  NULL },
	{ &wechat_groupchat, "groupchat", "群聊",  NULL },
	{ &wechat_label, "label", "标签",  NULL },
	{ &wechat_public, "publie", "公众",  NULL },
};

icon_item icon_wechat_page_contacts1[12] =
{
	{ &wechat_tiaotu, "rabbit", "子鼠",  NULL },
	{ &wechat_tiaotu, "rabbit", "丑牛",  NULL },
	{ &wechat_tiaotu, "rabbit", "寅虎",  NULL },
	{ &wechat_tiaotu, "rabbit", "卯兔",  NULL },
	{ &wechat_tiaotu, "rabbit", "辰龙",  NULL },
	{ &wechat_tiaotu, "rabbit", "巳蛇",  NULL },
	{ &wechat_tiaotu, "rabbit", "午马",  NULL },
	{ &wechat_tiaotu, "rabbit", "未羊",  NULL },
	{ &wechat_tiaotu, "rabbit", "申猴",  NULL },
	{ &wechat_tiaotu, "rabbit", "酉鸡",  NULL },
	{ &wechat_tiaotu, "rabbit", "戌狗",  NULL },
	{ &wechat_tiaotu, "rabbit", "亥猪",  NULL },
};

icon_item icon_wechat_page_discover[8] =
{
	{ &wechat_moments, "moments", "朋友",  NULL },
	{ &wechat_scan, "scan", "扫一",  NULL },
	{ &wechat_shake, "shake", "摇一",  NULL },
	{ &wechat_top_stories, "top_stories", "看一",  NULL },
	{ &wechat_search, "search", "搜一",  NULL },
	{ &wechat_people_nearby, "people_nearby", "附近的人",  NULL },
	{ &wechat_games, "games", "游戏",  NULL },
	{ &wechat_mini_programs, "mini_programs", "小小程序",  NULL },

};
icon_item icon_wechat_page_me[7] =
{
	{ &wechat_id, "id", "OpenRabbit",  NULL },
	{ &wechat_pay, "pay", "支付",  NULL },
	{ &wechat_favorites, "favorites", "收藏",  NULL },
	{ &wechat_my_posts, "my_posts", "相册",  NULL },
	{ &wechat_cards_offers, "cards_offers", "卡包",  NULL },
	{ &wechat_sticker_gallery, "sticker_gallery", "表情",  NULL },
	{ &wechat_settings, "settings", "设置",  NULL },

};


icon_item icon_wechat_header[8] =
{
	{ &wechat_chats0, "chats0", "跳兔",  NULL },
	{ &wechat_contacts0, "contacts0",  NULL },
	{ &wechat_discover0, "discover0",  NULL },
	{ &wechat_me0, "me0", "跳兔",  NULL },
	{ &wechat_chats1, "chats1", "跳兔",  NULL },
	{ &wechat_contacts1, "contacts1", "跳兔",  NULL },
	{ &wechat_discover1, "discover1", "跳兔",  NULL },
	{ &wechat_me1, "me1", "跳兔",  NULL },

};


static void btn_header_event_cb(lv_obj_t * obj, lv_event_t event);

static lv_obj_t *image_header[4];
static lv_obj_t *tabview_charts;




//static void btn_back_event_cb(lv_obj_t * obj, lv_event_t event)
//{
//	if (event == LV_EVENT_RELEASED)
//	{
//		/* 退出窗口 */
//		lv_obj_del(lv_obj_get_parent(obj));
//	}
//}


lv_obj_t * app_wechat_create(lv_obj_t *parent,void *user_data)
{
	(void)user_data;

	uint8_t i = 0;
	uint8_t index = 0;

	static lv_style_t style_btn_header;		/* tabview的切换按钮 */
	static lv_style_t style_label_list;		/* list的常规label */
	static lv_style_t style_label_back;		/* 返回按钮上的label */
	static lv_style_t style_label_title;	/* 标题 */
	static lv_style_t style_bg;				/* obj的背景 */
	static lv_style_t style_page_bg;		/* tabview的背景 */
	static lv_style_t style_tabview;		/* tabview */
	static lv_style_t style_list_btn_label_right;	/* list上面的按钮文本向右的箭头 */

	static lv_style_t style_list_bg;		/* list的背景 */
	static lv_style_t style_list_scrl;		/* list的部件 */
	static lv_style_t style_list_btn_rel;	/* list上按钮释放 */
	static lv_style_t style_list_btn_pr;	/* list上的按钮按下 */
	static lv_style_t style_list_btn_ina;	/* list上的按钮禁用 */


	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "微信");

/**************************************** 以上是初始化部分 *************************************************/

	/* 微信的四个页面基于tabview控件 */
	tabview_charts = lv_tabview_create(obj, NULL);
	lv_obj_set_pos(tabview_charts, 0, 60);
	lv_obj_set_size(tabview_charts, LV_HOR_RES, LV_VER_RES - 60 - 80);
	lv_tabview_set_btns_hidden(tabview_charts, true);

	/* 初始化并设置tabview的style */
	lv_style_copy(&style_tabview,&lv_style_plain);
	style_tabview.body.border.width = 0;
	lv_tabview_set_style(tabview_charts, LV_TABVIEW_STYLE_BG, &style_tabview);
	
	/* 创建四个页面到tabview */
	lv_obj_t *page_chats = lv_tabview_add_tab(tabview_charts, "Chats");
	lv_obj_t *page_contacts = lv_tabview_add_tab(tabview_charts, "Contacts");
	lv_obj_t *page_discover = lv_tabview_add_tab(tabview_charts, "Discover");
	lv_obj_t *page_me = lv_tabview_add_tab(tabview_charts, "Me");

	/* 设置页面样式 */
	lv_style_copy(&style_page_bg, &lv_style_transp_fit);
	style_page_bg.body.main_color = lv_color_hex(0xededed);
	style_page_bg.body.grad_color = lv_color_hex(0xededed);
	style_page_bg.body.opa = LV_OPA_100;
	style_page_bg.body.padding.bottom = 0;
	style_page_bg.body.padding.top = 0;
	style_page_bg.body.padding.left = 0;
	style_page_bg.body.padding.right = 0;

	lv_page_set_style(page_chats, LV_PAGE_STYLE_BG, &style_page_bg);
	lv_page_set_style(page_chats, LV_PAGE_STYLE_SCRL, &style_page_bg);
	lv_page_set_sb_mode(page_chats, LV_SB_MODE_OFF);
	lv_page_set_style(page_contacts, LV_PAGE_STYLE_BG, &style_page_bg);
	lv_page_set_style(page_contacts, LV_PAGE_STYLE_SCRL, &style_page_bg);
	lv_page_set_sb_mode(page_contacts, LV_SB_MODE_OFF);
	lv_page_set_style(page_discover, LV_PAGE_STYLE_BG, &style_page_bg);
	lv_page_set_style(page_discover, LV_PAGE_STYLE_SCRL, &style_page_bg);
	lv_page_set_sb_mode(page_discover, LV_SB_MODE_OFF);
	lv_page_set_style(page_me, LV_PAGE_STYLE_BG, &style_page_bg);
	lv_page_set_style(page_me, LV_PAGE_STYLE_SCRL, &style_page_bg);
	lv_page_set_sb_mode(page_me, LV_SB_MODE_OFF);
	

	/* 每个页面公用的对象指针 */
	lv_obj_t * list;
	lv_obj_t * list_btn;
	lv_obj_t * list_label;


	/* 第一个页面的list */
	list = lv_list_create(page_chats, NULL);
	lv_obj_set_size(list, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL) - 60 - 80);
	lv_list_set_sb_mode(list, LV_SB_MODE_OFF);
	/* 设置list的文本字体 */
	lv_style_copy(&style_label_list, &lv_style_transp);
	style_label_list.text.font = &gb2312_puhui16;



	/* 初始化list的背景的样式 */
	lv_style_copy(&style_list_bg, &lv_style_transp_fit);
	style_list_bg.body.main_color = lv_color_hex(0xededed);
	style_list_bg.body.grad_color = lv_color_hex(0xededed);
	style_list_bg.body.radius = 0;
	style_list_bg.body.opa = LV_OPA_COVER;
	style_list_bg.body.border.width = 1;
	style_list_bg.body.border.color = lv_color_hex3(0xbbb);
	style_list_bg.body.border.opa = LV_OPA_COVER;
	style_list_bg.body.shadow.color = LV_COLOR_WHITE;
	style_list_bg.body.shadow.type = LV_SHADOW_BOTTOM;
	style_list_bg.body.shadow.width = 0;
	style_list_bg.body.padding.left = 0;
	style_list_bg.body.padding.right = 0;
	style_list_bg.body.padding.top = 0;
	style_list_bg.body.padding.bottom = 0;
	style_list_bg.body.padding.inner = 0;

	/* 初始化滚动部件的样式 */
	lv_style_copy(&style_list_scrl, &lv_style_pretty);
	style_list_scrl.body.main_color = lv_color_hex(0xededed);
	style_list_scrl.body.grad_color = lv_color_hex(0xededed);
	style_list_scrl.body.padding.left = 0;
	style_list_scrl.body.padding.right = 0;
	style_list_scrl.body.padding.top = 0;
	style_list_scrl.body.padding.bottom = 0;
	style_list_scrl.body.padding.inner = 0;

	/* 初始化list的按钮的样式 */
	lv_style_copy(&style_list_btn_rel, &lv_style_btn_rel);
	style_list_btn_rel.body.main_color = LV_COLOR_WHITE;
	style_list_btn_rel.body.grad_color = LV_COLOR_WHITE;
	style_list_btn_rel.body.radius = 0;
	style_list_btn_rel.body.opa = LV_OPA_COVER;
	style_list_btn_rel.body.border.color = lv_color_hex3(0xbbb);
	style_list_btn_rel.body.border.width = 1;
	style_list_btn_rel.body.border.part = LV_BORDER_BOTTOM;
	style_list_btn_rel.body.border.opa = LV_OPA_COVER;
	style_list_btn_rel.body.shadow.width = 0;
	style_list_btn_rel.body.padding.left = 10;
	style_list_btn_rel.body.padding.right = 10;
	style_list_btn_rel.body.padding.top = 10;
	style_list_btn_rel.body.padding.bottom = 10;
	style_list_btn_rel.body.padding.inner = 15;

	lv_style_copy(&style_list_btn_pr, &style_list_btn_rel);
	lv_style_copy(&style_list_btn_ina, &style_list_btn_rel);

	style_list_btn_pr.body.main_color = lv_color_hex(0xededed);
	style_list_btn_pr.body.grad_color = style_list_btn_pr.body.main_color;

	style_list_btn_ina.body.main_color = lv_color_hex(0xededed);
	style_list_btn_ina.body.grad_color = style_list_btn_ina.body.main_color;

	/* 设置list的样式 */
	lv_list_set_style(list, LV_LIST_STYLE_SCRL, &style_list_scrl);
	lv_list_set_style(list, LV_LIST_STYLE_BG, &style_list_bg);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_PR, &style_list_btn_pr);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_REL, &style_list_btn_rel);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_INA, &style_list_btn_ina);

	/* 往list添加按钮和文本 */
	for (i = 0; i < sizeof(icon_wechat_page_chats) / sizeof(icon_wechat_page_chats[0]); i++)
	{
		list_btn = lv_list_add_btn(list, icon_wechat_page_chats[i].icon_img, NULL);
		list_label = lv_label_create(list_btn,NULL);
		lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_label_list);
		lv_label_set_text(list_label, icon_wechat_page_chats[i].text_cn);
		lv_obj_align(list_label, list_btn, LV_ALIGN_IN_LEFT_MID, 200, 0);
	}

	/* 第二个页面的list */
	list = lv_list_create(page_contacts, NULL);
	lv_obj_set_size(list, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL) - 60 - 80);
	lv_list_set_sb_mode(list, LV_SB_MODE_OFF);

	lv_list_set_style(list, LV_LIST_STYLE_SCRL, &style_list_scrl);
	lv_list_set_style(list, LV_LIST_STYLE_BG, &style_list_bg);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_PR, &style_list_btn_pr);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_REL, &style_list_btn_rel);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_INA, &style_list_btn_ina);

	for (i = 0; i < sizeof(icon_wechat_page_contacts) / sizeof(icon_wechat_page_contacts[0]); i++)
	{
		list_btn = lv_list_add_btn(list, icon_wechat_page_contacts[i].icon_img, NULL);
		
		list_label = lv_label_create(list_btn, NULL);
		lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_label_list);
		lv_label_set_text(list_label, icon_wechat_page_contacts[i].text_cn);
		lv_obj_align(list_label, list_btn, LV_ALIGN_IN_LEFT_MID, 200, 0);
	}
	list_btn = lv_list_add_btn(list, NULL, NULL);
	lv_obj_set_height(list_btn, 20);
	lv_btn_set_state(list_btn, LV_BTN_STATE_INA);
	for (i = 0; i < sizeof(icon_wechat_page_contacts1) / sizeof(icon_wechat_page_contacts1[0]); i++)
	{
		list_btn = lv_list_add_btn(list, icon_wechat_page_contacts1[i].icon_img, NULL);
		list_label = lv_label_create(list_btn, NULL);
		lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_label_list);
		lv_label_set_text(list_label, icon_wechat_page_contacts1[i].text_cn);
		lv_obj_align(list_label, list_btn, LV_ALIGN_IN_LEFT_MID, 200, 0);
	}

	/* 向右的箭头的label的样式,主要设置颜色 */
	lv_style_copy(&style_list_btn_label_right,&lv_style_scr);
	style_list_btn_label_right.text.color = lv_color_hex(0xacacac);

	/* 第三个页面的list */
	list = lv_list_create(page_discover, NULL);
	lv_obj_set_size(list, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL) - 60 - 80);
	lv_list_set_sb_mode(list, LV_SB_MODE_OFF);

	lv_list_set_style(list, LV_LIST_STYLE_SCRL, &style_list_scrl);
	lv_list_set_style(list, LV_LIST_STYLE_BG, &style_list_bg);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_PR, &style_list_btn_pr);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_REL, &style_list_btn_rel);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_INA, &style_list_btn_ina);

	index = 0;
	for (i = 0; i < sizeof(icon_wechat_page_discover) / sizeof(icon_wechat_page_discover[0]) + 5; i++)
	{
		if (i == 1 || i == 4 || i == 7 || i == 9 || i == 11)
		{
			list_btn = lv_list_add_btn(list, NULL, NULL);
			lv_obj_set_height(list_btn, 15);
			lv_btn_set_state(list_btn, LV_BTN_STATE_INA);
		}
		else
		{
			list_btn = lv_list_add_btn(list, icon_wechat_page_discover[index].icon_img, NULL);
			
			list_label = lv_label_create(list_btn, NULL);
			lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_label_list);
			lv_label_set_text(list_label, icon_wechat_page_discover[index].text_cn);
			index++;

			/* 重新设置一下btn的layout使得后面的对齐可用 */
			lv_btn_set_layout(list_btn, LV_LAYOUT_OFF);
			list_label = lv_label_create(list_btn, NULL);
			lv_label_set_text(list_label, LV_SYMBOL_RIGHT);
			lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_list_btn_label_right);
			lv_obj_align(list_label,list_btn,LV_ALIGN_IN_RIGHT_MID,-10,5);
		
		}
	}
	//list_btn = lv_list_add_btn(list, NULL, NULL);
	//lv_obj_set_height(list_btn, 72);
	//lv_btn_set_state(list_btn, LV_BTN_STATE_INA);

	/* 第四个页面的list */
	list = lv_list_create(page_me, NULL);
	lv_obj_set_size(list, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL) - 60 - 80);
	lv_list_set_sb_mode(list, LV_SB_MODE_OFF);

	lv_list_set_style(list, LV_LIST_STYLE_SCRL, &style_list_scrl);
	lv_list_set_style(list, LV_LIST_STYLE_BG, &style_list_bg);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_PR, &style_list_btn_pr);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_REL, &style_list_btn_rel);
	lv_list_set_style(list, LV_LIST_STYLE_BTN_INA, &style_list_btn_ina);

	index = 0;
	for (i = 0; i < sizeof(icon_wechat_page_me) / sizeof(icon_wechat_page_me[0]) + 3; i++)
	{
		if (i == 1 || i == 3 || i == 8)
		{
			list_btn = lv_list_add_btn(list, NULL, NULL);
			lv_obj_set_height(list_btn, 15);
			lv_btn_set_state(list_btn, LV_BTN_STATE_INA);
		}
		else
		{
			list_btn = lv_list_add_btn(list, icon_wechat_page_me[index].icon_img, NULL);
			list_label = lv_label_create(list_btn, NULL);
			lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_label_list);
			lv_label_set_text(list_label, icon_wechat_page_me[index].text_cn);
			index++;

			/* 重新设置一下btn的layout使得后面的对齐可用 */
			lv_btn_set_layout(list_btn, LV_LAYOUT_OFF);
			list_label = lv_label_create(list_btn, NULL);
			lv_label_set_text(list_label, LV_SYMBOL_RIGHT);
			lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_list_btn_label_right);
			lv_obj_align(list_label, list_btn, LV_ALIGN_IN_RIGHT_MID, -10, 5);

		}
	}
	//list_btn = lv_list_add_btn(list, NULL, NULL);
	//lv_obj_set_height(list_btn, 135);
	//lv_btn_set_state(list_btn, LV_BTN_STATE_INA);


	/* 底部的四个按钮 */
	lv_style_copy(&style_btn_header, &lv_style_scr);
	style_btn_header.body.main_color = lv_color_hex(0xf7f7f7);
	style_btn_header.body.grad_color = lv_color_hex(0xf7f7f7);
	lv_obj_t *btn_header;
	for ( i = 0; i < 4; i++)
	{
		btn_header = lv_btn_create(obj,NULL);
		lv_obj_set_size(btn_header,120,80);
		lv_obj_set_pos(btn_header, 120 * i, LV_VER_RES - 80);
		lv_btn_set_style(btn_header, LV_BTN_STATE_REL, &style_btn_header);
		lv_btn_set_style(btn_header, LV_BTN_STATE_PR, &style_btn_header);
		lv_obj_set_user_data(btn_header,i);
		lv_obj_set_event_cb(btn_header, btn_header_event_cb);

		image_header[i] = lv_img_create(btn_header, NULL);
		lv_img_set_src(image_header[i], icon_wechat_header[i].icon_img);
	}
	lv_img_set_src(image_header[0], icon_wechat_header[0+4].icon_img);



	return obj;
}







static void btn_header_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint8_t index = 0;
	
	if (event == LV_EVENT_RELEASED)
	{
		/* 切换底部图片的显示以及tabview的当前页面 */

		lv_img_set_src(image_header[lv_tabview_get_tab_act(tabview_charts)], icon_wechat_header[lv_tabview_get_tab_act(tabview_charts)].icon_img);

		index = (uint8_t)lv_obj_get_user_data(obj);

		lv_img_set_src(image_header[index], icon_wechat_header[index+4].icon_img);

		lv_tabview_set_tab_act(tabview_charts, index,false);
		
		printf("index:%d\n",index);
	}
}

/*
lv_color_t color = {0};
static const lv_style_t style_list_bg = 
{ 
	
	.body.main_color = { 0xff >> 3, 0xff >> 2, 0xff >> 3 },
	.body.grad_color = LV_COLOR_WHITE,
	.body.radius = 0,
	.body.opa = LV_OPA_COVER,
	.body.border.width = 1,
	.body.border.color = LV_COLOR_WHITE,
	.body.border.opa = LV_OPA_COVER,
	.body.shadow.color = LV_COLOR_WHITE,
	.body.shadow.type = LV_SHADOW_BOTTOM,
	.body.shadow.width = 0,
	.body.padding.left = 0,
	.body.padding.right = 0,
	.body.padding.top = 0,
	.body.padding.bottom = 0,
	.body.padding.inner = 0,

};
*/


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

