/*
*******************************************************************************************************
*
* 文件名称 : app_weather.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 天气应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"


#define WEATHER_SYMBOL_TEMP             "\xe2\x84\x83"

LV_FONT_DECLARE(font_weather_16)

static void obj_event_cb(lv_obj_t * obj, lv_event_t event);

/* 广告词 */
static const uint8_t *text_ad[] = { "\xe6\xba\x90\xe7\xa0\x81\xe6\x9d\xa5\xe8\x87\xaa\xe8\xb7\xb3\xe5\x85\x94\xe7\xa7\x91\xe6\x8a\x80OpenRabbit","\xe5\x9b\xbe\xe6\xa0\x87\xe6\x9d\xa5\xe8\x87\xaa\xe9\x98\xbf\xe9\x87\x8c\xe5\xb7\xb4\xe5\xb7\xb4Iconfont", "\xe5\x88\x9b\xe6\x84\x8f\xe6\x9d\xa5\xe8\x87\xaa\xe5\xb0\x8f\xe7\xb1\xb3\xe5\xa4\xa9\xe6\xb0\x94" };
static const lv_img_dsc_t *img_ad[] = { &weather_tiaotu, &weather_iconfont, &weather_mi };


static const lv_img_dsc_t *img_weather[] = { &weather_shower, &weather_sunny, &weather_snow };
static const uint8_t *text_weather_cn[] = { "今天-阴转小雨", "明天-晴朗", "后天-小雪" };
static const uint8_t *text_weather_temp_max_min[] = { "7\xe2\x84\x83 / 3\xe2\x84\x83", "8\xe2\x84\x83 / 5\xe2\x84\x83", "5\xe2\x84\x83 / 2\xe2\x84\x83" };
static const uint8_t *text_weather_air[] = { "\xe8\x89\xaf\xe5\xa5\xbd", "\xe4\xbc\x98\xe8\x89\xaf", "\xe4\xb8\x80\xe8\x88\xac" };



static const lv_img_dsc_t *img_life[] = { &weather_windbreaker, &weather_sun_protection, &weather_motion, &weather_car, &weather_umbrella, };
static const  uint8_t *text_life[] = { "适宜风衣", "无需防晒", "室内运动", "不宜洗车", "有雨带伞" };



static const lv_point_t line_points[] = { { 46, 15 }, { 116, 16 }, { 186, 15 }, { 256, 14 }, { 326, 16 }, { 396, 16 } };

static const lv_point_t line_arc_points[] = { { 1, 48 }, { 2, 49 }, { 3, 50 }, { 4, 51 }, { 5, 51 }, { 6, 52 }, { 7, 53 }, { 8, 54 }, { 9, 55 }, { 10, 55 }, { 11, 56 }, { 12, 57 }, { 13, 57 }, { 14, 58 }, { 15, 59 }, { 16, 60 }, { 17, 60 }, { 18, 61 }, { 19, 61 }, { 20, 62 }, { 21, 63 }, { 22, 63 }, { 23, 64 }, { 24, 64 }, { 25, 65 }, { 26, 66 }, { 27, 66 }, { 28, 67 }, { 29, 67 }, { 30, 68 }, \
{31, 68}, { 32, 69 }, { 33, 69 }, { 34, 70 }, { 35, 70 }, { 36, 71 }, { 37, 71 }, { 38, 72 }, { 39, 72 }, { 40, 73 }, { 41, 73 }, { 42, 74 }, { 43, 74 }, { 44, 75 }, { 45, 75 }, { 46, 75 }, { 47, 76 }, { 48, 76 }, { 49, 77 }, { 50, 77 }, { 51, 78 }, { 52, 78 }, { 53, 78 }, { 54, 79 }, { 55, 79 }, { 56, 80 }, { 57, 80 }, { 58, 80 }, { 59, 81 }, { 60, 81 }, \
{61, 81}, { 62, 82 }, { 63, 82 }, { 64, 82 }, { 65, 83 }, { 66, 83 }, { 67, 83 }, { 68, 84 }, { 69, 84 }, { 70, 84 }, { 71, 85 }, { 72, 85 }, { 73, 85 }, { 74, 86 }, { 75, 86 }, { 76, 86 }, { 77, 86 }, { 78, 87 }, { 79, 87 }, { 80, 87 }, { 81, 87 }, { 82, 88 }, { 83, 88 }, { 84, 88 }, { 85, 89 }, { 86, 89 }, { 87, 89 }, { 88, 89 }, { 89, 90 }, { 90, 90 }, \
{91, 90}, { 92, 90 }, { 93, 90 }, { 94, 91 }, { 95, 91 }, { 96, 91 }, { 97, 91 }, { 98, 92 }, { 99, 92 }, { 100, 92 }, { 101, 92 }, { 102, 92 }, { 103, 93 }, { 104, 93 }, { 105, 93 }, { 106, 93 }, { 107, 93 }, { 108, 94 }, { 109, 94 }, { 110, 94 }, { 111, 94 }, { 112, 94 }, { 113, 94 }, { 114, 95 }, { 115, 95 }, { 116, 95 }, { 117, 95 }, { 118, 95 }, { 119, 95 }, { 120, 96 }, \
{121, 96}, { 122, 96 }, { 123, 96 }, { 124, 96 }, { 125, 96 }, { 126, 96 }, { 127, 96 }, { 128, 97 }, { 129, 97 }, { 130, 97 }, { 131, 97 }, { 132, 97 }, { 133, 97 }, { 134, 97 }, { 135, 97 }, { 136, 97 }, { 137, 98 }, { 138, 98 }, { 139, 98 }, { 140, 98 }, { 141, 98 }, { 142, 98 }, { 143, 98 }, { 144, 98 }, { 145, 98 }, { 146, 98 }, { 147, 98 }, { 148, 99 }, { 149, 99 }, { 150, 99 }, \
{151, 99}, { 152, 99 }, { 153, 99 }, { 154, 99 }, { 155, 99 }, { 156, 99 }, { 157, 99 }, { 158, 99 }, { 159, 99 }, { 160, 99 }, { 161, 99 }, { 162, 99 }, { 163, 99 }, { 164, 99 }, { 165, 99 }, { 166, 99 }, { 167, 99 }, { 168, 99 }, { 169, 99 }, { 170, 99 }, { 171, 99 }, { 172, 99 }, { 173, 99 }, { 174, 99 }, { 175, 99 }, { 176, 100 }, { 177, 99 }, { 178, 99 }, { 179, 99 }, { 180, 99 }, \
{181, 99}, { 182, 99 }, { 183, 99 }, { 184, 99 }, { 185, 99 }, { 186, 99 }, { 187, 99 }, { 188, 99 }, { 189, 99 }, { 190, 99 }, { 191, 99 }, { 192, 99 }, { 193, 99 }, { 194, 99 }, { 195, 99 }, { 196, 99 }, { 197, 99 }, { 198, 99 }, { 199, 99 }, { 200, 99 }, { 201, 99 }, { 202, 99 }, { 203, 99 }, { 204, 99 }, { 205, 98 }, { 206, 98 }, { 207, 98 }, { 208, 98 }, { 209, 98 }, { 210, 98 }, \
{211, 98}, { 212, 98 }, { 213, 98 }, { 214, 98 }, { 215, 98 }, { 216, 97 }, { 217, 97 }, { 218, 97 }, { 219, 97 }, { 220, 97 }, { 221, 97 }, { 222, 97 }, { 223, 97 }, { 224, 97 }, { 225, 96 }, { 226, 96 }, { 227, 96 }, { 228, 96 }, { 229, 96 }, { 230, 96 }, { 231, 96 }, { 232, 96 }, { 233, 95 }, { 234, 95 }, { 235, 95 }, { 236, 95 }, { 237, 95 }, { 238, 95 }, { 239, 94 }, { 240, 94 }, \
{241, 94}, { 242, 94 }, { 243, 94 }, { 244, 94 }, { 245, 93 }, { 246, 93 }, { 247, 93 }, { 248, 93 }, { 249, 93 }, { 250, 92 }, { 251, 92 }, { 252, 92 }, { 253, 92 }, { 254, 92 }, { 255, 91 }, { 256, 91 }, { 257, 91 }, { 258, 91 }, { 259, 90 }, { 260, 90 }, { 261, 90 }, { 262, 90 }, { 263, 90 }, { 264, 89 }, { 265, 89 }, { 266, 89 }, { 267, 89 }, { 268, 88 }, { 269, 88 }, { 270, 88 }, \
{271, 87}, { 272, 87 }, { 273, 87 }, { 274, 87 }, { 275, 86 }, { 276, 86 }, { 277, 86 }, { 278, 86 }, { 279, 85 }, { 280, 85 }, { 281, 85 }, { 282, 84 }, { 283, 84 }, { 284, 84 }, { 285, 83 }, { 286, 83 }, { 287, 83 }, { 288, 82 }, { 289, 82 }, { 290, 82 }, { 291, 81 }, { 292, 81 }, { 293, 81 }, { 294, 80 }, { 295, 80 }, { 296, 80 }, { 297, 79 }, { 298, 79 }, { 299, 78 }, { 300, 78 }, \
{301, 78}, { 302, 77 }, { 303, 77 }, { 304, 76 }, { 305, 76 }, { 306, 75 }, { 307, 75 }, { 308, 75 }, { 309, 74 }, { 310, 74 }, { 311, 73 }, { 312, 73 }, { 313, 72 }, { 314, 72 }, { 315, 71 }, { 316, 71 }, { 317, 70 }, { 318, 70 }, { 319, 69 }, { 320, 69 }, { 321, 68 }, { 322, 68 }, { 323, 67 }, { 324, 67 }, { 325, 66 }, { 326, 66 }, { 327, 65 }, { 328, 64 }, { 329, 64 }, { 330, 63 }, \
{331, 63}, { 332, 62 }, { 333, 61 }, { 334, 61 }, { 335, 60 }, { 336, 60 }, { 337, 59 }, { 338, 58 }, { 339, 57 }, { 340, 57 }, { 341, 56 }, { 342, 55 }, { 343, 55 }, { 344, 54 }, { 345, 53 }, { 346, 52 }, { 347, 51 }, { 348, 51 }, { 349, 50 }, { 350, 49 }, { 351, 48 }, };



void create_line_note(lv_obj_t *line, lv_obj_t *parent, lv_point_t points[], uint16_t num);
void create_line_point(lv_obj_t *line, lv_obj_t *parent, lv_point_t points[], uint16_t num, uint16_t point_size);

static lv_task_t *line_point_update_task = NULL;
static void line_point_update_task_cb(lv_task_t *t);


static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		if (line_point_update_task != NULL)
		{
			lv_task_del(line_point_update_task);
		}
		break;
	default:
		break;
	}
}


lv_obj_t * app_weather_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	uint8_t time_buf[64];

	gui_rtc_date_time date_time;
	gui_hal_get_time(&date_time);


	static lv_style_t style_label_temp;
	static lv_style_t style_label_cn_16;
	static lv_style_t style_label_cn_12;
	static lv_style_t style_label_robot_16;
	static lv_style_t style_page;
	static lv_style_t style_bg;
	static lv_style_t style_cont;
	static lv_style_t style_label_font_weather_16;
	static lv_style_t style_arc;
	static lv_style_t style_line;
	static lv_style_t style_circle;

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "天气");
	lv_obj_set_event_cb(obj, obj_event_cb);

	lv_style_copy(&style_bg, &lv_style_scr);
	style_bg.body.main_color = lv_color_hex(0x4577af);
	style_bg.body.grad_color = style_bg.body.main_color;
	lv_obj_set_style(obj, &style_bg);


	lv_obj_t *page = lv_page_create(obj, NULL);
	lv_style_copy(&style_page, &lv_style_scr);
	style_page.body.main_color = lv_color_hex(0x4577af);
	style_page.body.grad_color = style_page.body.main_color;
	style_page.body.border.width = 0;
	style_page.body.border.part = LV_BORDER_FULL;
	style_page.body.padding.top = 0;
	style_page.body.padding.bottom = 0;
	style_page.body.padding.left = 0;
	style_page.body.padding.right = 0;
	style_page.body.padding.inner = 0;

	lv_style_copy(&style_cont, &lv_style_scr);
	style_cont.body.main_color = lv_color_hex(0x5d91cb);
	style_cont.body.grad_color = style_cont.body.main_color;
	style_cont.body.radius = 10;
	style_page.body.border.part = LV_BORDER_TOP;
	style_cont.body.padding.top = 10;

	lv_style_copy(&style_line, &lv_style_scr);
	lv_style_copy(&style_arc, &lv_style_scr);
	lv_style_copy(&style_circle, &lv_style_pretty_color);
	lv_style_copy(&style_label_font_weather_16, &lv_style_scr);
	lv_style_copy(&style_label_robot_16, &lv_style_scr);
	lv_style_copy(&style_label_cn_16, &lv_style_scr);
	lv_style_copy(&style_label_cn_12, &lv_style_scr);
	lv_style_copy(&style_label_temp, &lv_style_scr);
	style_label_temp.text.color = LV_COLOR_WHITE;
	style_label_temp.text.font = &gb2312_puhui32;

	style_label_cn_16.text.font = &gb2312_puhui16;
	style_label_cn_16.text.color = LV_COLOR_WHITE;

	style_label_font_weather_16.text.font = &font_weather_16;
	style_label_font_weather_16.text.color = LV_COLOR_WHITE;

	style_label_robot_16.text.font = &lv_font_roboto_16;
	style_label_robot_16.text.color = LV_COLOR_WHITE;

	style_label_cn_12.text.font = &gb2312_puhui12;
	style_label_cn_12.text.color = LV_COLOR_WHITE;
	style_label_cn_12.body.radius = LV_RADIUS_CIRCLE;
	style_label_cn_12.body.main_color = lv_color_hex(0x699cd4);
	style_label_cn_12.body.grad_color = style_label_cn_12.body.main_color;
	style_label_cn_12.body.padding.left = 10;
	style_label_cn_12.body.padding.right = 10;
	style_label_cn_12.body.opa = LV_OPA_100;


	style_line.line.color = lv_color_hex(0xc6daef);
	style_line.line.width = 3;

	style_circle.body.main_color = lv_color_hex(0xffdd00);
	style_circle.body.grad_color = style_circle.body.main_color;
	style_circle.body.radius = LV_RADIUS_CIRCLE;
	style_circle.body.border.color = style_circle.body.main_color;
	style_circle.body.border.width = 1;
	style_circle.body.border.opa = LV_OPA_30;
	style_circle.body.shadow.color = style_circle.body.main_color;
	style_circle.body.shadow.width = 1;




	lv_page_set_style(page, LV_PAGE_STYLE_BG, &style_page);
	lv_page_set_sb_mode(page, false);
	lv_obj_set_width(page, lv_obj_get_width(obj));
	lv_obj_set_height(page, lv_obj_get_height(obj) - 60 - 30);
	lv_obj_set_pos(page, 0, 90);

	lv_obj_t *img_big_sun = lv_img_create(page, NULL);
	lv_img_set_src(img_big_sun,&weather_sunny_big);
	lv_obj_set_pos(img_big_sun, 50, 50);

	lv_obj_t *label_time = lv_label_create(page, NULL);
	lv_label_set_style(label_time, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
	sprintf((char*)time_buf, "%04d-%02d-%02d", date_time.Year+2000, date_time.Month,date_time.Date);
	lv_label_set_text(label_time, time_buf);
	lv_obj_align(label_time, page, LV_ALIGN_IN_TOP_MID, 0, 0);

	lv_obj_t *label_temp = lv_label_create(page, NULL);
	lv_label_set_style(label_temp,LV_LABEL_STYLE_MAIN,&style_label_temp);
	lv_label_set_text(label_temp,"3");
	lv_obj_set_pos(label_temp, 160, 50);

	
	lv_obj_t *label_temp_symbol = lv_label_create(page, NULL);
	lv_label_set_style(label_temp_symbol, LV_LABEL_STYLE_MAIN, &style_label_font_weather_16);
	lv_label_set_text(label_temp_symbol, WEATHER_SYMBOL_TEMP);
	lv_obj_align(label_temp_symbol, label_temp,LV_ALIGN_OUT_RIGHT_TOP,0,5);

	lv_obj_t *label_temp_weather = lv_label_create(page, NULL);
	lv_label_set_style(label_temp_weather, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
	lv_label_set_text(label_temp_weather, "多云");
	lv_obj_align(label_temp_weather, label_temp, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 5);

	lv_obj_t *label_temp_info = lv_label_create(page, NULL);
	lv_label_set_style(label_temp_info, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
	sprintf((char*)time_buf, "数据来自跳兔天气\n最近更新时间:%02d:%02d", date_time.Hours, date_time.Minutes);
	lv_label_set_text(label_temp_info, time_buf);
	lv_obj_align(label_temp_info, label_temp, LV_ALIGN_OUT_RIGHT_MID, 100, 20);

	lv_obj_t *label_address;

	label_address = lv_label_create(obj, NULL);
	lv_label_set_text(label_address, "孝南市区 湖北职院");
	lv_label_set_style(label_address, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
	lv_obj_align(label_address, obj, LV_ALIGN_IN_TOP_MID, 0, 60);
	
	lv_obj_t *label_gps = lv_label_create(obj, NULL);
	lv_label_set_style(label_gps, LV_LABEL_STYLE_MAIN, &style_label_robot_16);
	lv_label_set_text(label_gps, LV_SYMBOL_GPS);
	lv_obj_align(label_gps, label_address, LV_ALIGN_OUT_LEFT_MID, -5, 0);


	lv_obj_t *cont_weather;
	lv_obj_t *cont_line;
	lv_obj_t *cont_sunset;
	lv_obj_t *cont_ad;
	lv_obj_t *cont_life;

	cont_weather = lv_cont_create(page,NULL);
	lv_obj_set_width(cont_weather, lv_page_get_fit_width(page) - 20);
	lv_obj_set_height(cont_weather, 180);
	lv_obj_align(cont_weather, page, LV_ALIGN_IN_TOP_MID, 0, 180);
	lv_cont_set_style(cont_weather, LV_CONT_STYLE_MAIN, &style_cont);
	lv_obj_set_drag_parent(cont_weather,true);


	cont_line = lv_cont_create(page, NULL);
	lv_obj_set_width(cont_line, lv_page_get_fit_width(page) - 20);
	lv_obj_set_height(cont_line, 200);
	lv_obj_align(cont_line, cont_weather, LV_ALIGN_OUT_BOTTOM_MID, 0, 30);
	lv_cont_set_style(cont_line, LV_CONT_STYLE_MAIN, &style_cont);
	lv_obj_set_drag_parent(cont_line, true);


	cont_sunset = lv_cont_create(page, NULL);
	lv_obj_set_width(cont_sunset, lv_page_get_fit_width(page) - 20);
	lv_obj_set_height(cont_sunset, 250);
	lv_obj_align(cont_sunset, cont_line, LV_ALIGN_OUT_BOTTOM_MID, 0, 30);
	lv_cont_set_style(cont_sunset, LV_CONT_STYLE_MAIN, &style_cont);
	lv_obj_set_drag_parent(cont_sunset, true);


	cont_ad = lv_cont_create(page, NULL);
	lv_obj_set_width(cont_ad, lv_page_get_fit_width(page) - 20);
	lv_obj_set_height(cont_ad, 230);
	lv_obj_align(cont_ad, cont_sunset, LV_ALIGN_OUT_BOTTOM_MID, 0, 30);
	lv_cont_set_style(cont_ad, LV_CONT_STYLE_MAIN, &style_cont);
	lv_obj_set_drag_parent(cont_ad, true);


	cont_life = lv_cont_create(page, NULL);
	lv_obj_set_width(cont_life, lv_page_get_fit_width(page) - 20);
	lv_obj_set_height(cont_life, 300);
	lv_obj_align(cont_life, cont_ad, LV_ALIGN_OUT_BOTTOM_MID, 0, 30);
	lv_cont_set_style(cont_life, LV_CONT_STYLE_MAIN, &style_cont);
	lv_obj_set_drag_parent(cont_life, true);

	lv_obj_t *label_data = lv_label_create(page, NULL);
	lv_label_set_style(label_data, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
	lv_label_set_text(label_data, "天气数据采用模拟数据");
	lv_obj_align(label_data, cont_life, LV_ALIGN_OUT_BOTTOM_MID, 0, 5);

	if (cont_weather != NULL)
	{

		lv_obj_t *img;
		lv_obj_t *label_weather;
		lv_obj_t *label_temperature;
		lv_obj_t *label_air;
		uint16_t y_pos;
		for (size_t i = 0; i < 3; i++)
		{
			y_pos = i * 50 + 20;
			img = lv_img_create(cont_weather, NULL);
			lv_img_set_src(img, img_weather[i]);
			lv_obj_set_pos(img, 20, y_pos);

			label_weather = lv_label_create(cont_weather, NULL);
			lv_label_set_style(label_weather, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
			lv_label_set_text(label_weather, text_weather_cn[i]);
			lv_obj_align(label_weather, img, LV_ALIGN_OUT_RIGHT_MID, 10, 0);

			label_air = lv_label_create(cont_weather, NULL);
			lv_label_set_style(label_air, LV_LABEL_STYLE_MAIN, &style_label_cn_12);
			lv_label_set_text(label_air, text_weather_air[i]);
			lv_label_set_body_draw(label_air, true);
			lv_obj_set_width(label_air, 50);
			lv_obj_align(label_air, label_weather, LV_ALIGN_OUT_RIGHT_MID, 15, 0);

			label_temperature = lv_label_create(cont_weather, NULL);
			lv_label_set_style(label_temperature, LV_LABEL_STYLE_MAIN, &style_label_font_weather_16);
			lv_label_set_text(label_temperature, text_weather_temp_max_min[i]);
			lv_obj_set_width(label_temperature, 50);
			lv_obj_set_pos(label_temperature, 350, y_pos+2);
			//lv_obj_align(label_temperature, label_air, LV_ALIGN_OUT_RIGHT_MID, 100, 0);
		}
	}

	if (cont_line != NULL)
	{

		lv_obj_t *img;
		lv_obj_t *label_time;
		lv_obj_t *label_wind;
		lv_obj_t *label_air;
		lv_obj_t *line;

		uint16_t x_pos = 0;
		uint16_t y_pos = 100;

		lv_obj_t *cont_temp = lv_cont_create(cont_line, NULL);
		lv_obj_set_width(cont_temp, lv_page_get_fit_width(page) - 20);
		lv_obj_set_height(cont_temp, 100);
		//lv_obj_align(cont_temp, cont_sunset, LV_ALIGN_OUT_BOTTOM_MID, 0, 30);
		lv_cont_set_style(cont_temp, LV_CONT_STYLE_MAIN, &style_cont);
		lv_obj_set_drag_parent(cont_temp, true);


		line = lv_line_create(cont_temp, NULL);
		lv_obj_set_pos(line, 0, 50);
		lv_line_set_style(line, LV_LINE_STYLE_MAIN, &style_line);	/* 设置样式 */
		lv_line_set_points(line, line_points, 6);					/* 设置点阵集 */
		lv_line_set_y_invert(line, true);							/* 反转y,默认是数字大的在下面,反转后数字大的在上面 */
		create_line_point(line, cont_temp, line_points, 6, 8);
		create_line_note(line, cont_temp, line_points, 6);				/* 创建注释 */

		uint8_t hour = 0;

		for (size_t i = 0; i < 6; i++)
		{
			x_pos = i * 70 + 30;

			img = lv_img_create(cont_line, NULL);
			lv_img_set_src(img, &weather_wind);
			lv_obj_set_pos(img, x_pos-6, y_pos+5);

			label_wind = lv_label_create(cont_line, NULL);
			lv_label_set_style(label_wind, LV_LABEL_STYLE_MAIN, &style_label_cn_12);
			lv_label_set_text(label_wind, "2\xe7\xba\xa7");
			lv_obj_set_pos(label_wind, x_pos+6, y_pos);

			label_air = lv_label_create(cont_line, NULL);
			lv_label_set_style(label_air, LV_LABEL_STYLE_MAIN, &style_label_cn_12);
			lv_label_set_text(label_air, "优良");
			lv_label_set_body_draw(label_air, true);
			lv_obj_set_pos(label_air, x_pos, y_pos+30);

			label_time = lv_label_create(cont_line, NULL);
			lv_label_set_style(label_time, LV_LABEL_STYLE_MAIN, &style_label_cn_12);
			hour = date_time.Hours + i;
			if (hour>=24)
			{
				hour -= 24;
			}
			sprintf((char*)time_buf, "%02d:%02d", hour, date_time.Minutes);
			lv_label_set_text(label_time, time_buf);
			lv_obj_set_pos(label_time, x_pos-8, y_pos + 60);
			
		}
	}

	if (cont_sunset != NULL)
	{

		lv_obj_t *img;
		lv_obj_t *label_parameter;
		lv_obj_t *label_info;
		lv_obj_t *label_sunrise;
		lv_obj_t *label_sunset;
		//lv_obj_t *label_sunrise_time;
		//lv_obj_t *label_sunset_time;

		uint16_t x_pos = 0;
		uint16_t y_pos = 150;
	
		uint8_t *text[5] = {"2\xe7\xba\xa7","69%","5\xe2\x84\x83","1026mb"};
		uint8_t *text1[4] = { "东风", "湿度", "体感", "气压"};

		//lv_obj_t *line_sun = lv_line_create(cont_sunset, NULL);
		//lv_obj_set_pos(line_sun,45,30);
		//lv_line_set_style(line_sun, LV_LINE_STYLE_MAIN, &style_line);
		//lv_line_set_points(line_sun, line_arc_points, sizeof(line_arc_points) / sizeof(line_arc_points[0]));
		//lv_line_set_y_invert(line_sun, true);	/* 反转y,默认是数字大的在下面,反转后数字大的在上面 */
		

		uint16_t led_x_pos = 0;
		lv_obj_t *led_circle = lv_led_create(cont_sunset, NULL);
		lv_led_set_style(led_circle,LV_LED_STYLE_MAIN,&style_circle);
		lv_obj_set_size(led_circle,20,20);
		lv_obj_set_pos(led_circle, led_x_pos + 45 - 10, 130 - line_arc_points[led_x_pos].y-5);
		//lv_obj_set_pos(led_circle, 45-10, 130-48-10);
		line_point_update_task = lv_task_create(line_point_update_task_cb, 50, LV_TASK_PRIO_LOW, led_circle);

		lv_obj_t *line_sun = lv_line_create(cont_sunset, NULL);
		lv_obj_set_pos(line_sun, 45, 30);
		lv_line_set_style(line_sun, LV_LINE_STYLE_MAIN, &style_line);
		lv_line_set_points(line_sun, line_arc_points, sizeof(line_arc_points) / sizeof(line_arc_points[0]));
		lv_line_set_y_invert(line_sun, true);	/* 反转y,默认是数字大的在下面,反转后数字大的在上面 */


		label_sunrise = lv_label_create(cont_sunset, NULL);
		lv_label_set_style(label_sunrise, LV_LABEL_STYLE_MAIN, &style_label_cn_12);
		lv_label_set_text(label_sunrise, "日出 07:02");
		lv_obj_align(label_sunrise, cont_sunset, LV_ALIGN_IN_TOP_LEFT, 50, y_pos - 55);
		//lv_obj_set_pos(label_sunrise, 50, 160);

		label_sunset = lv_label_create(cont_sunset, NULL);
		lv_label_set_style(label_sunset, LV_LABEL_STYLE_MAIN, &style_label_cn_12);
		lv_label_set_text(label_sunset, "日落 17:32");
		lv_obj_align(label_sunset, cont_sunset, LV_ALIGN_IN_TOP_RIGHT, -50, y_pos - 55);
		//lv_obj_set_pos(label_sunset, 380, 160);

		for (size_t i = 0; i < 4; i++)
		{
			x_pos = i * 110 + 30;

			label_info = lv_label_create(cont_sunset, NULL);
			lv_label_set_style(label_info, LV_LABEL_STYLE_MAIN, &style_label_cn_12);
			lv_label_set_text(label_info, text1[i]);
			lv_obj_set_pos(label_info, x_pos, y_pos + 30);

			
			label_parameter = lv_label_create(cont_sunset, NULL);
			lv_label_set_style(label_parameter, LV_LABEL_STYLE_MAIN, &style_label_font_weather_16);
			lv_label_set_text(label_parameter, text[i]);
			lv_label_set_align(label_parameter, LV_LABEL_ALIGN_CENTER);
			lv_obj_align(label_parameter, label_info,LV_ALIGN_OUT_TOP_MID,0,0);
			//lv_obj_set_pos(label_parameter, x_pos, y_pos);


		}
	}

	if (cont_ad != NULL)
	{

		lv_obj_t *img;
		lv_obj_t *label;

		uint16_t x_pos = 0;
		uint16_t y_pos = 200;

		x_pos = 20;

		for (size_t i = 0; i < 3; i++)
		{
			y_pos = 15 + i * 64;
			img = lv_img_create(cont_ad, NULL);
			lv_img_set_src(img, img_ad[i]);
			lv_obj_set_pos(img, x_pos, y_pos + 5);

			label = lv_label_create(cont_ad, NULL);
			lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
			lv_label_set_text(label, text_ad[i]);
			lv_obj_align(label,img,LV_ALIGN_OUT_RIGHT_MID,10,0);
			//lv_obj_set_pos(label, x_pos+70, y_pos);
		}
	}

	if (cont_life != NULL)
	{
		lv_obj_t *img;
		lv_obj_t *label;

		uint16_t x_pos = 0;
		uint16_t y_pos = 200;

		for (size_t i = 0; i < 5; i++)
		{
			y_pos = (i/3) * 140 + 30;
			x_pos = (35 + (i%3) * 150);
			img = lv_img_create(cont_life, NULL);
			lv_img_set_src(img, img_life[i]);
			lv_obj_set_pos(img, x_pos, y_pos + 5);

			label = lv_label_create(cont_life, NULL);
			lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_cn_16);
			lv_label_set_text(label, text_life[i]);
			lv_obj_align(label, img, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);
			//lv_obj_set_pos(label, x_pos+70, y_pos);
		}
	}

	

	return obj;
}

static void line_point_update_task_cb(lv_task_t *t)
{
	static uint16_t led_x_pos = 0;
	//t->user_data
	lv_obj_set_pos(t->user_data, led_x_pos + 45 - 10, 130 - line_arc_points[led_x_pos].y - 5);

	if ((led_x_pos++)>=350)
	{
		led_x_pos = 0;
	}
}



/**
* @brief 为line控件创建注释
* @param line-控件指针
* @param parent-注释父对象
* @param points-点阵
* @param num-数量
* @retval	None
*/
void create_line_note(lv_obj_t *line, lv_obj_t *parent, lv_point_t points[], uint16_t num)
{
	uint16_t i = 0;
	uint8_t value[25];
	for (i = 0; i < num; i++)
	{
		memset(value, 0, sizeof(value));
		lv_obj_t *label1 = lv_label_create(parent, NULL);
		sprintf(value, "%d", points[i].y);
		lv_label_set_text(label1, value);
		lv_obj_align(label1, line, LV_ALIGN_IN_BOTTOM_LEFT, points[i].x - 10, (-points[i].y) + 30);

	}
}


/**
* @brief 为line控件创建放大的点
* @param line-控件指针
* @param parent-注释父对象
* @param points-点阵
* @param num-数量
* @param point_size-点的大小
* @retval	None
* @note 实际是用led控件模拟点
*/
void create_line_point(lv_obj_t *line, lv_obj_t *parent, lv_point_t points[], uint16_t num, uint16_t point_size)
{
	uint16_t i = 0;

	static lv_style_t style_led;
	lv_style_copy(&style_led, &lv_style_pretty_color);
	style_led.body.radius = LV_RADIUS_CIRCLE;
	style_led.body.main_color = lv_color_hex(0xffffff);
	style_led.body.grad_color = style_led.body.main_color;
	style_led.body.border.color = LV_COLOR_MAKE(0xfa, 0x0f, 0x00);
	style_led.body.border.width = 1;
	style_led.body.border.opa = LV_OPA_30;
	style_led.body.shadow.color = LV_COLOR_MAKE(0xb5, 0x0f, 0x04);
	style_led.body.shadow.width = 1;


	for (i = 0; i < num; i++)
	{

		lv_obj_t *led = lv_led_create(parent, NULL);
		lv_obj_set_size(led, point_size, point_size);
		lv_led_set_style(led, LV_LED_STYLE_MAIN, &style_led);
		lv_obj_align(led, line, LV_ALIGN_IN_BOTTOM_LEFT, points[i].x - (point_size / 2), (-points[i].y) + (point_size / 2));
	}
}






/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

