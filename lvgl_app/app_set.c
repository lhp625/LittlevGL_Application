/*
*******************************************************************************************************
*
* 文件名称 : app_set.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 设置
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"




extern lv_obj_t * app_about_device_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_set_time_create(lv_obj_t *parent, void *user_data);

static const icon_item icon_set_list[] =
{
	{ &set_backlight, "backlight", "背光调节",  NULL },
	{ &set_theme, "theme", "主题",  NULL },
	{ &set_wallpaper, "wallpaper", "壁纸",  NULL },
	{ &set_clock, "clock", "系统时钟",  NULL },
	{ &set_wlan, "wlan", "WIFI",  NULL },
	{ &set_calendar, "calendar", "时间日期",  NULL },
	{ &set_about, "about", "关于本机",  NULL },

	{ &set_system_update, "system_update", "系统升级",  NULL },
	{ &set_reset_device, "reset_device", "重新启动",  NULL },

};


static const char *sysyclk = { "Turbo:180MHz\n""Overclocking:192MHz\n""Overclocking:240MHz" };
static const char *theme = { "material\n""night\n""alien\n""zen\n""nemo\n""mono\n""default\n""templ" };
static const char *wallpaper = { "跳兔\n""小米壁纸\n""青春\n""青春\n""动漫\n""游戏" };

static void list_set_event_cb(lv_obj_t * obj, lv_event_t event);

static void sw_wlan_event_cb(lv_obj_t * obj, lv_event_t event);
static void slider_backlight_event_cb(lv_obj_t * obj, lv_event_t event);
static void ddlist_theme_event_cb(lv_obj_t * obj, lv_event_t event);
static void ddlist_wallpaper_event_cb(lv_obj_t * obj, lv_event_t event);
static void ddlist_sysclk_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_save_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_about_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_set_time_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_system_update_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_reset_device_event_cb(lv_obj_t * obj, lv_event_t event);



static gui_setting gui_set_setting;


/* 记录进入消息提示框以前按下的是哪个按钮 */
enum
{
	STATUS_NORMAL = -1,
	STATUS_SYSTEM_UPDATE = 0,
	STATUS_RESET_DEVICE,
};
static int8_t status = -1;

static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		break;
	case LV_EVENT_APPLY:
		if (status == STATUS_SYSTEM_UPDATE)
		{
			status = STATUS_NORMAL;
			gui_hal_reset_device_of_update();
		}
		
		else if (status == STATUS_RESET_DEVICE)
		{
			status = STATUS_NORMAL;
			gui_hal_reset_device();
		}
		break;
	case LV_EVENT_CANCEL:
		if (status != STATUS_NORMAL)
		{
			status = STATUS_NORMAL;
		}

		break;

	default:
		break;
	}
}


lv_obj_t * app_set_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	uint8_t i = 0;

	static lv_style_t style_btn_save;		/* 保存按钮 */
	static lv_style_t style_label_save;
	static lv_style_t style_label_list;		/* list的常规label */
	static lv_style_t style_label_ddlist;		/* ddlist的常规label */

	gui_hal_setting_read(&gui_set_setting);

	lv_style_copy(&style_btn_save, &lv_style_transp);
	lv_style_copy(&style_label_save, &lv_style_plain_color);
	style_label_save.text.font = &gb2312_puhui16;

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "设置");
	lv_obj_set_event_cb(obj, obj_event_cb);


	lv_obj_t *btn_save = lv_btn_create(obj, NULL);
	lv_obj_set_size(btn_save, 100, 50);
	lv_obj_set_event_cb(btn_save, btn_save_event_cb);
	lv_obj_align(btn_save, obj, LV_ALIGN_IN_TOP_RIGHT, -10, 5);
	lv_obj_t *label_back = lv_label_create(btn_save, NULL);
	lv_label_set_style(label_back, LV_LABEL_STYLE_MAIN, &style_label_save);
	lv_label_set_text(label_back, "保存设置");


	/* 每个页面公用的对象指针 */
	lv_obj_t * list;
	lv_obj_t * list_btn;
	lv_obj_t * list_label;
	lv_obj_t *ddlist;
	lv_obj_t *slider;
	lv_obj_t *sw;

	list = lv_list_create(obj, NULL);
	lv_obj_set_pos(list, 0, 60);
	lv_obj_set_size(list, lv_obj_get_width(obj), lv_obj_get_height(obj) - 60);
	lv_list_set_sb_mode(list, LV_SB_MODE_OFF);

	/* 设置list的文本字体 */
	lv_style_copy(&style_label_list, &lv_style_plain_color);
	style_label_list.text.font = &gb2312_puhui16;
	if (gui_current_theme == THEME_MATERIAL || gui_current_theme == THEME_ZEN || gui_current_theme == THEME_MONO || gui_current_theme == THEME_TEMPL)
		style_label_list.text.color = LV_COLOR_BLACK;

	/* 往list添加按钮和文本 */
	for (i = 0; i < sizeof(icon_set_list) / sizeof(icon_set_list[0]); i++)
	{
		list_btn = lv_list_add_btn(list, icon_set_list[i].icon_img, NULL);
		list_label = lv_label_create(list_btn, NULL);
		lv_label_set_style(list_label, LV_LABEL_STYLE_MAIN, &style_label_list);
		lv_label_set_text(list_label, icon_set_list[i].text_cn);

		/* 重新设置一下btn的layout使得后面的对齐可用 */
		lv_btn_set_layout(list_btn, LV_LAYOUT_OFF);
	}
	lv_obj_set_event_cb(list, list_set_event_cb);

	/* 背光 */
	list_btn = lv_list_get_next_btn(list, NULL);
	slider = lv_slider_create(list_btn, NULL);
	lv_slider_set_value(slider, gui_set_setting.backlight, true);
	lv_obj_set_event_cb(slider, slider_backlight_event_cb);
	lv_obj_align(slider, list_btn, LV_ALIGN_IN_RIGHT_MID, -20, 8);

	/* 主题 */
	list_btn = lv_list_get_next_btn(list, list_btn);
	ddlist = lv_ddlist_create(list_btn, NULL);
	lv_ddlist_set_fix_width(ddlist, lv_obj_get_width(slider));
	lv_ddlist_set_draw_arrow(ddlist, true);
	lv_ddlist_set_options(ddlist, theme);
	lv_ddlist_set_selected(ddlist, gui_set_setting.theme_index);
	lv_obj_set_event_cb(ddlist, ddlist_theme_event_cb);
	lv_obj_align(ddlist, list_btn, LV_ALIGN_IN_RIGHT_MID, -20, 8);


	/* 壁纸 */
	list_btn = lv_list_get_next_btn(list, list_btn);
	ddlist = lv_ddlist_create(list_btn, NULL);
	lv_style_copy(&style_label_ddlist, lv_ddlist_get_style(ddlist, LV_DDLIST_STYLE_BG));
	style_label_ddlist.text.font = &gb2312_puhui16;
	lv_ddlist_set_style(ddlist, LV_DDLIST_STYLE_BG, &style_label_ddlist);
	lv_ddlist_set_fix_width(ddlist, lv_obj_get_width(slider));
	lv_ddlist_set_draw_arrow(ddlist, true);
	lv_ddlist_set_options(ddlist, wallpaper);
	lv_ddlist_set_selected(ddlist, gui_set_setting.wallpaper_index);
	lv_obj_set_event_cb(ddlist, ddlist_wallpaper_event_cb);
	lv_obj_align(ddlist, list_btn, LV_ALIGN_IN_RIGHT_MID, -20, 8);

	/* 系统时钟 */
	list_btn = lv_list_get_next_btn(list, list_btn);
	ddlist = lv_ddlist_create(list_btn, NULL);
	lv_ddlist_set_fix_width(ddlist, lv_obj_get_width(slider));
	lv_ddlist_set_draw_arrow(ddlist, true);
	lv_ddlist_set_options(ddlist, sysyclk);
	lv_ddlist_set_selected(ddlist, gui_set_setting.sysclk);
	lv_obj_set_event_cb(ddlist, ddlist_sysclk_event_cb);

	lv_obj_align(ddlist, list_btn, LV_ALIGN_IN_RIGHT_MID, -20, 8);


	/* WIFI */
	list_btn = lv_list_get_next_btn(list, list_btn);
	sw = lv_sw_create(list_btn, NULL);
	gui_set_setting.wlan_power ? lv_sw_on(sw, true) : lv_sw_off(sw, true);
	lv_obj_align(sw, list_btn, LV_ALIGN_IN_RIGHT_MID, -20, 8);
	lv_obj_set_event_cb(sw, sw_wlan_event_cb);


	/* 时间日期 */
	list_btn = lv_list_get_next_btn(list, list_btn);
	lv_obj_set_event_cb(list_btn, btn_set_time_event_cb);

	/* 关于 */
	list_btn = lv_list_get_next_btn(list, list_btn);
	lv_obj_set_event_cb(list_btn, btn_about_event_cb);

	/* 系统升级 */
	list_btn = lv_list_get_next_btn(list, list_btn);
	lv_obj_set_event_cb(list_btn, btn_system_update_event_cb);
	
	/* 重新启动 */
	list_btn = lv_list_get_next_btn(list, list_btn);
	lv_obj_set_event_cb(list_btn, btn_reset_device_event_cb);
	
	return obj;
}




static void btn_save_event_cb(lv_obj_t * obj, lv_event_t event)
{

	if (event == LV_EVENT_RELEASED)
	{
		/* 保存配置 */
		gui_hal_setting_write(&gui_set_setting);
		message_box(lv_obj_get_parent(obj), "保存成功");
	}
}



static void sw_wlan_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		gui_set_setting.wlan_power = lv_sw_get_state(obj);
		printf("wlan_power:%d\n", gui_set_setting.wlan_power);

	}
}


static void slider_backlight_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint8_t value = 0;
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		value = lv_slider_get_value(obj);
		gui_hal_backlight(value);
		if (value <= 5)
			value = 5;
		gui_set_setting.backlight = value;

	}
}


static void list_set_event_cb(lv_obj_t * obj, lv_event_t event)
{

}

static void ddlist_theme_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint8_t value = 0;
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		value = lv_ddlist_get_selected(obj);
		gui_set_setting.theme_index = value;
		gui_current_theme = gui_set_setting.theme_index;
		switch (value)
		{
		case 0:
			lv_theme_set_current(lv_theme_material_init(210, NULL));
			break;
		case 1:
			lv_theme_set_current(lv_theme_night_init(210, NULL));
			break;
		case 2:
			lv_theme_set_current(lv_theme_alien_init(210, NULL));
			break;
		case 3:
			lv_theme_set_current(lv_theme_zen_init(210, NULL));
			break;
		case 4:
			lv_theme_set_current(lv_theme_nemo_init(210, NULL));
			break;
		case 5:
			lv_theme_set_current(lv_theme_mono_init(210, NULL));
			break;
		case 6:
			lv_theme_set_current(lv_theme_default_init(210, NULL));
			break;
		case 7:
			lv_theme_set_current(lv_theme_templ_init(210, NULL));
			break;
		default:
			lv_theme_set_current(lv_theme_default_init(210, NULL));
			break;
		}
	}
}


static void ddlist_wallpaper_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint8_t value = 0;
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		value = lv_ddlist_get_selected(obj);
		gui_set_setting.wallpaper_index = value;

		/* 演示的时候打开即可,实际应用请勿打开 */
		//uint8_t bg_fname_buf[32] = { 0 };
		//sprintf((char*)bg_fname_buf, "0:/lvgl/bg%02d.bin", value);
		//lv_load_img_bin_from_file(&img_desktop, (const char*)bg_fname_buf);
	}
}


static void btn_about_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_CLICKED)
	{
		app_about_device_create(lv_scr_act(), NULL);
	}

}

static void btn_set_time_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_CLICKED)
	{
		//printf("set_time\n");
		app_set_time_create(lv_scr_act(), NULL);
	}
}


static void ddlist_sysclk_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint8_t value = 0;
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		value = lv_ddlist_get_selected(obj);
		gui_set_setting.sysclk = value;
	}
}

static void btn_system_update_event_cb(lv_obj_t * obj, lv_event_t event)
{

	if (event == LV_EVENT_CLICKED)
	{
		lv_obj_t *parent;
		parent = lv_obj_get_parent(obj);
		parent = lv_obj_get_parent(parent);
		parent = lv_obj_get_parent(parent);
		status = STATUS_SYSTEM_UPDATE;		//记录进入消息提示框前的状态
		//message_box(parent, "确认重启进入系统更新吗？");
		message_box_win(parent, " "LV_SYMBOL_POWER " INFO", "确认重启进入系统更新吗？", "确定", "取消");
	}
}



static void btn_reset_device_event_cb(lv_obj_t * obj, lv_event_t event)
{

	if (event == LV_EVENT_CLICKED)
	{
		lv_obj_t *parent;
		parent = lv_obj_get_parent(obj);
		parent = lv_obj_get_parent(parent);
		parent = lv_obj_get_parent(parent);
		status = STATUS_RESET_DEVICE;		//记录进入消息提示框前的状态
		//message_box(parent, "确定重新启动吗？");
		message_box_win(parent, " "LV_SYMBOL_POWER" INFO", "确定重新启动吗？", "确定", "取消");
	}
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
