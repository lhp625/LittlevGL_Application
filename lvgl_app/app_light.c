/*
*******************************************************************************************************
*
* 文件名称 : app_light.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 光敏传感器应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

static void update_chart(lv_task_t *t);
static void obj_event_cb(lv_obj_t * obj, lv_event_t event);



static const uint8_t *list_value_x = "0\n1\n2\n3\n4\n5\n6\n7\n\8\n\9";
static const uint8_t *list_value_y = "100\n90\n80\n70\n60\n50\n40\n30\n20\n10\n0";


static lv_chart_series_t *ser_light;
static lv_task_t *task;


lv_obj_t * app_light_create(lv_obj_t *parent,void *user_data)
{
	(void)user_data;

	static lv_style_t style_label_info;
	static lv_style_t style_chart;

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "光照");
	lv_obj_set_event_cb(obj, obj_event_cb);

	lv_style_copy(&style_label_info, &lv_style_plain_color);
	if (gui_current_theme == THEME_NEMO)
		style_label_info.text.color = LV_COLOR_WHITE;
	else
		style_label_info.text.color = LV_COLOR_BLACK;
	style_label_info.text.font = &gb2312_puhui16;

	lv_style_copy(&style_chart, &lv_style_plain_color);
	if (gui_current_theme == THEME_NEMO)
		style_chart.text.color = LV_COLOR_WHITE;
	else
		style_chart.text.color = LV_COLOR_BLACK;


	lv_obj_t * label = lv_label_create(obj,NULL);
	lv_label_set_style(label,LV_LABEL_STYLE_MAIN,&style_label_info);
	lv_label_set_text(label,"图表反映环境光照强度");
	lv_obj_align(label,obj,LV_ALIGN_IN_TOP_MID,0,70);

	lv_obj_t *chart = lv_chart_create(obj, NULL);		/* 创建图表控件 */
	lv_obj_align(chart, NULL, LV_ALIGN_IN_TOP_MID, 0, 120);	/* 设置位置 */
	lv_chart_set_point_count(chart, 10);					/* 设置显示的点数量 */
	lv_chart_set_series_width(chart, 4);					/* 设置线宽度 */
	lv_chart_set_range(chart, 0, 100);					/* 设置范围0-100 */
	lv_chart_set_div_line_count(chart, 9, 5);			/* 设置分割线的数量 */
	lv_chart_set_margin(chart, 50);						/* 设置标注的扩展长度 */
	lv_chart_set_type(chart, LV_CHART_TYPE_POINT | LV_CHART_TYPE_LINE);	/* 显示方式 显示点和线 */
	lv_chart_set_x_tick_texts(chart, list_value_x, 1, LV_CHART_AXIS_DRAW_LAST_TICK);	/* 设置标注的文本 */
	lv_obj_set_size(chart, 300, 300);						/* 设置控件尺寸 */
	lv_chart_set_x_tick_length(chart, 10, 10);			/* 刻度线长度 */
	lv_chart_set_y_tick_texts(chart, list_value_y, 1, LV_CHART_AXIS_DRAW_LAST_TICK);	/* 设置标注的文本 */
	lv_chart_set_y_tick_length(chart, 10, 10);			/* 刻度线长度 */
	lv_chart_set_style(chart,LV_CHART_STYLE_MAIN,&style_chart);


	ser_light = lv_chart_add_series(chart, LV_COLOR_BLUE);	/* 创建线条 */

	task = lv_task_create(update_chart, 500, LV_TASK_PRIO_LOW, chart);	/* 创建定期更新的任务 */

	return obj;
}

static void update_chart(lv_task_t *t)
{
	/* 更新图表 */
	lv_chart_set_next(t->user_data, ser_light, gui_hal_adc_light_get_ratio());
}


static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		if (task != NULL)
		{
			lv_task_del(task);
		}
		
		break;
	default:
		break;
	}
}



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/
