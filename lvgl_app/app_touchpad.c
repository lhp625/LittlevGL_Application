/*
*******************************************************************************************************
*
* 文件名称 : app_touchpad.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 画板应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

static lv_obj_t *canvas;
static lv_color_t *canvas_buf;
static lv_point_t touch_point;
static lv_point_t last_touch_point;
static lv_style_t style_line;

static lv_img_dsc_t img = { 0 };
static void draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t size, uint32_t color);

static void canvas_event_cb(lv_obj_t * obj, lv_event_t event);
static void touchpad_task_cb(lv_task_t *t);

static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	static uint8_t pressed = 0;
	switch (event)
	{
	case LV_EVENT_DELETE:

		break;
	case LV_EVENT_PRESSING:
	{
		pressed = 1;
		lv_indev_t * indev = lv_indev_get_act();
		if (indev->proc.types.pointer.act_point.y > 60)
		{
			touch_point.x = indev->proc.types.pointer.act_point.x;
			touch_point.y = indev->proc.types.pointer.act_point.y - 60;
		}
		//printf("x:%d, y:%d\n", indev->proc.types.pointer.act_point.x, indev->proc.types.pointer.act_point.y);
		//draw_line();
		if ((last_touch_point.x != 0) && (last_touch_point.y != 0))
		{
			draw_line(last_touch_point.x, last_touch_point.y, touch_point.x, touch_point.y,2,0);
			last_touch_point.x = touch_point.x;
			last_touch_point.y = touch_point.y;
		}
		else
		{
			last_touch_point.x = touch_point.x;
			last_touch_point.y = touch_point.y;
		}
	}
		break;
	case LV_EVENT_RELEASED:
		pressed = 0;
		memset(&touch_point, 0, sizeof(lv_point_t));
		memset(&last_touch_point, 0, sizeof(lv_point_t));
		break;

	default:
		break;
	}
}


lv_obj_t * app_touchpad_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	static lv_style_t style_label;

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "画板");
	lv_obj_set_event_cb(obj, obj_event_cb);

	lv_style_copy(&style_line,&lv_style_plain_color);
	style_line.line.color = LV_COLOR_BLUE;

	canvas_buf = (lv_color_t*)lv_mem_alloc(LV_CANVAS_BUF_SIZE_TRUE_COLOR(LV_HOR_RES, LV_VER_RES));		/* 申请一个屏幕大小的缓冲区供画布使用 */
	canvas = lv_canvas_create(obj,NULL);
	lv_canvas_set_buffer(canvas, canvas_buf, LV_HOR_RES, LV_VER_RES, LV_IMG_CF_TRUE_COLOR);	/* 设置缓冲区 */
	lv_obj_set_pos(canvas,0,60);

	touch_point.x = 10;
	touch_point.y = 10;
	lv_canvas_draw_arc(canvas, touch_point.x, touch_point.y, 2, 0, 360, &style_line);			/* 画圆 */

	//lv_task_create(touchpad_task_cb, 1, LV_TASK_PRIO_LOW, canvas);


	img.data = (void *)NULL;
	img.header.cf = LV_IMG_CF_TRUE_COLOR;
	img.header.w = 0;
	img.header.h = 0;


	return obj;
}

static void touchpad_task_cb(lv_task_t *t)
{


	lv_canvas_rotate(canvas, &img, 30, 0, 300, 0, 0);
	//lv_canvas_draw_arc(t->user_data, 100, 100, 2, 0, 360, &style_line);
	if ((touch_point.x!=0) && (touch_point.y!=0))
	{
		lv_canvas_draw_arc(t->user_data, touch_point.x, touch_point.y, 2, 0, 360, &style_line);
		touch_point.x = 0;
		touch_point.y = 0;

	}
}

/**
* @brief LTDC画一条线
* @param x1-x起始坐标
* @param y1-y起始坐标
* @param x2-x终点坐标
* @param y2-y终点坐标
* @param size-线的大小
* @param color-颜色
* @retval	None
* @note
*/
static void draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t size, uint32_t color)
{
	uint16_t t;
	int xerr = 0, yerr = 0, delta_x, delta_y, distance;
	int incx, incy, uRow, uCol;
	delta_x = x2 - x1; //计算坐标增量 
	delta_y = y2 - y1;
	uRow = x1;
	uCol = y1;
	if (delta_x>0)incx = 1; //设置单步方向 
	else if (delta_x == 0)incx = 0;//垂直线 
	else { incx = -1; delta_x = -delta_x; }
	if (delta_y>0)incy = 1;
	else if (delta_y == 0)incy = 0;//水平线 
	else{ incy = -1; delta_y = -delta_y; }
	if (delta_x>delta_y)distance = delta_x; //选取基本增量坐标轴 
	else distance = delta_y;
	for (t = 0; t <= distance + 1; t++)//画线输出 
	{

		lv_canvas_rotate(canvas, &img, 0, 0, 0, 0, 0);
		lv_canvas_draw_arc(canvas, uRow, uCol, size, 0, 360, &style_line);
		xerr += delta_x;
		yerr += delta_y;
		if (xerr>distance)
		{
			xerr -= distance;
			uRow += incx;
		}
		if (yerr>distance)
		{
			yerr -= distance;
			uCol += incy;
		}
	}
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

