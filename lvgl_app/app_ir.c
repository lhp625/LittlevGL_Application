/*
*******************************************************************************************************
*
* 文件名称 : app_ir.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 红外应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"


static lv_obj_t *label_recv;
static lv_task_t *task_ir;

static const uint8_t *btn_key_map[] = {LV_SYMBOL_POWER,LV_SYMBOL_UP,LV_SYMBOL_HOME,
										LV_SYMBOL_PREV,LV_SYMBOL_PAUSE,LV_SYMBOL_NEXT,
										LV_SYMBOL_PLUS,LV_SYMBOL_DOWN,LV_SYMBOL_MINUS,
										"1","2","3","4","5","6","7","8","9","0"};

static const uint8_t *ir_recv_key[] = { "","POWER", "UP", "HOME",
										"PREV", "PAUSE", "NEXT",
										"PLUS", "DOWN", "MINUS",
										"1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };


static void ir_scan_task(lv_task_t *t);
static void btn_key_event_cb(lv_obj_t * obj, lv_event_t event);


static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		if (task_ir != NULL)
		{
			lv_task_del(task_ir);
		}
		gui_hal_ir_receive_stop_scan();  /* 停止红外接收 */
		break;
	default:
		break;
	}
}


lv_obj_t * app_ir_create(lv_obj_t *parent,void *user_data)
{
	(void)user_data;

	uint8_t i = 0;
	uint16_t x_pos = 0, y_pos = 0;

	static lv_style_t style_label;
	static lv_style_t style_btnm_text;
	static lv_style_t style_btn_key_rel;
	static lv_style_t style_btn_key_pr;
	static lv_style_t style_label_btn_key;

	gui_hal_ir_receive_start_scan();	/* 开启红外接收 */
	
	lv_obj_t *obj = lv_app_common_int(parent, "红外");
	lv_obj_set_event_cb(obj, obj_event_cb);
	
	lv_style_copy(&style_label, &lv_style_plain_color);
	style_label.text.font = &lv_font_roboto_28;
	if (gui_current_theme == THEME_NEMO)
		style_label.text.color = LV_COLOR_WHITE;
	else
		style_label.text.color = LV_COLOR_BLACK;
	label_recv = lv_label_create(obj, NULL);
	lv_label_set_text(label_recv,"REC:");
	lv_label_set_style(label_recv, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label_recv,obj,LV_ALIGN_IN_TOP_MID,0,80);


	lv_style_copy(&style_label_btn_key, lv_theme_get_current()->style.label.hint);
	lv_style_copy(&style_btn_key_pr,lv_theme_get_current()->style.btn.pr);
	lv_style_copy(&style_btn_key_rel, lv_theme_get_current()->style.btn.rel);
	style_btn_key_pr.body.radius = LV_RADIUS_CIRCLE;
	style_btn_key_rel.body.radius = LV_RADIUS_CIRCLE;
	style_label_btn_key.text.font = &lv_font_roboto_28;

	lv_obj_t *btn;
	lv_obj_t *label;

	for ( i = 0; i < 19; i++)
	{
		x_pos = ((i % 3) * 100) + 100;
		y_pos = ((i / 3) * 90) + 170;
		btn = lv_btn_create(obj, NULL);
		label = lv_label_create(btn, NULL);
		lv_label_set_style(label,LV_LABEL_STYLE_MAIN,&style_label_btn_key);
		lv_label_set_text(label, btn_key_map[i]);
		lv_btn_set_style(btn, LV_BTN_STYLE_PR, &style_btn_key_pr);
		lv_btn_set_style(btn, LV_BTN_STYLE_REL, &style_btn_key_rel);
		lv_obj_set_pos(btn, x_pos, y_pos);
		lv_obj_set_size(btn, 80, 80);
		lv_obj_set_user_data(btn,i);
		lv_obj_set_event_cb(btn, btn_key_event_cb);

		if (i == 18)
		{
			lv_obj_set_pos(btn, x_pos+100, y_pos);
		}
	}


	task_ir = lv_task_create(ir_scan_task, 100, LV_TASK_PRIO_LOW, label_recv);	/* 创建定期更新的任务 */

	return obj;
}

static void btn_key_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED)
	{
		gui_hal_ir_send(0, (uint32_t)lv_obj_get_user_data(obj)+1, 0);
	}
}

static void ir_scan_task(lv_task_t *t)
{
	uint8_t rec_buf[32];
	uint8_t ir_code = 0;

	ir_code = gui_hal_ir_receive();
	
	if (ir_code != 0)
	{
		printf("ir_code:%d\r\n",ir_code);
		if (ir_code > 19)
			sprintf((char*)rec_buf, "code:%d key:unknown", ir_code);
		else
			sprintf((char*)rec_buf, "code:%d key:%s", ir_code,ir_recv_key[ir_code]);

		lv_label_set_text(t->user_data, (const char*)rec_buf);
		lv_obj_realign(t->user_data);

	}



}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

