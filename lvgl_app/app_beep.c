/*
*******************************************************************************************************
*
* 文件名称 : app_beep.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 蜂鸣器应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"






static void obj_event_cb(lv_obj_t * obj, lv_event_t event);
static void slider_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_event_cb(lv_obj_t * obj, lv_event_t event);
static lv_style_t style_beep_icon;
static lv_obj_t *image_beep;



static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:
		gui_hal_beep_duty_cycle(0);
		gui_hal_beep_switch(0);
		//printf("obj delete\n");
		break;
	default:
		break;
	}
}


lv_obj_t * app_beep_create(lv_obj_t *parent,void *user_data)
{
	(void)user_data;

	static lv_style_t style_label;

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "BEEP");
	lv_obj_set_event_cb(obj, obj_event_cb);

	lv_obj_t *btn = lv_btn_create(obj,NULL);
	lv_obj_set_size(btn, 100, 100);
	lv_obj_align(btn, obj, LV_ALIGN_IN_TOP_MID, 0, 75);
	lv_btn_set_toggle(btn,true);
	lv_obj_set_event_cb(btn, btn_event_cb);
	lv_btn_set_style(btn, LV_BTN_STYLE_REL, &lv_style_transp);
	lv_btn_set_style(btn, LV_BTN_STYLE_PR, &lv_style_btn_ina);
	lv_btn_set_style(btn, LV_BTN_STATE_TGL_REL, &lv_style_transp);
	lv_btn_set_style(btn, LV_BTN_STATE_TGL_PR, &lv_style_btn_ina);

	image_beep = lv_img_create(btn, NULL);
	lv_img_set_src(image_beep, &beep_open);
	lv_style_copy(&style_beep_icon, lv_img_get_style(image_beep, LV_IMG_STYLE_MAIN));
	style_beep_icon.image.color = LV_COLOR_GRAY;
	style_beep_icon.image.intense = 255;
	lv_img_set_style(image_beep, LV_IMG_STYLE_MAIN, &style_beep_icon);

	lv_obj_t *slider = lv_slider_create(obj,NULL);
	lv_obj_align(slider,obj,LV_ALIGN_IN_TOP_MID,0,200);
	lv_obj_set_event_cb(slider, slider_event_cb);

	lv_style_copy(&style_label,&lv_style_plain_color);
	if (gui_current_theme == THEME_NEMO)
		style_label.text.color = LV_COLOR_WHITE;
	else
		style_label.text.color = LV_COLOR_BLACK;
	style_label.text.font = &gb2312_puhui16;
	lv_obj_t *label = lv_label_create(obj, NULL);
	lv_label_set_text(label,"打开测试即可,勿长时间打开");
	lv_obj_align(label, obj, LV_ALIGN_IN_TOP_MID, -130, 250);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);

	return obj;
}

static void slider_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint8_t value = 0;
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		value = lv_slider_get_value(obj);
		gui_hal_beep_duty_cycle(value);
		//printf("value:%d\n", value);
	}
}

static void btn_event_cb(lv_obj_t * obj, lv_event_t event)
{
	uint32_t color = 0;
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		if (lv_btn_get_state(obj) == LV_BTN_STATE_TGL_REL)
		{
			
			/* open beep */
			gui_hal_beep_switch(1);
			//printf("LV_BTN_STATE_TGL_REL\n");
			//style_beep_icon.image.color = LV_COLOR_BLUE;
			style_beep_icon.image.intense = 0;		/* 重新着色的不透明度设置为0,就是原来的颜色 */
			lv_img_set_style(image_beep, LV_IMG_STYLE_MAIN, &style_beep_icon);
		}
		else if (lv_btn_get_state(obj) == LV_BTN_STATE_REL)
		{
			/* close beep */
			gui_hal_beep_switch(0);
			//printf("LV_BTN_STATE_TGL_REL\n");
			style_beep_icon.image.color = LV_COLOR_GRAY;
			style_beep_icon.image.intense = 255;
			lv_img_set_style(image_beep, LV_IMG_STYLE_MAIN, &style_beep_icon);
		}

	}
}



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

