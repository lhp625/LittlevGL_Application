/*
*******************************************************************************************************
*
* 文件名称 : app_wlan.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : WIFI参数设置应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

static lv_obj_t * kb;

static lv_obj_t *ta_ssid;
static lv_obj_t *ta_pwd;
static lv_obj_t *ta_ip;
static lv_obj_t *ta_port;

static gui_setting gui_wlan_setting;

static void kb_event_cb(lv_obj_t * obj, lv_event_t event);
static void ta_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_save_event_cb(lv_obj_t * obj, lv_event_t event);

lv_obj_t * app_wlan_create(lv_obj_t *parent,void *user_data)
{
	(void)user_data;

	static lv_style_t style_label_ok;

	gui_hal_setting_read(&gui_wlan_setting);
	
	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "WLAN");

	ta_ssid = lv_ta_create(obj, NULL);			/* 创建ta控件 */
	lv_ta_set_text(ta_ssid,(const char*)gui_wlan_setting.wlan_ssid);/* 设置文本 */
	lv_ta_set_one_line(ta_ssid, true);						/* 单行模式 */
	lv_obj_set_width(ta_ssid, LV_HOR_RES / 2 - 20);			/* 宽度 */
	lv_obj_set_pos(ta_ssid, 5, 80);							/* 设置坐标 */
	lv_obj_set_event_cb(ta_ssid, ta_event_cb);				/* 设置事件回调函数 */
	lv_ta_set_max_length(ta_ssid, 16);						/* 设置最大长度 */
	
	
	/* 创建label用于引导用户输入 */
	lv_obj_t * label_ssid = lv_label_create(obj, NULL);
	lv_label_set_text(label_ssid, "SSID:");
	lv_obj_align(label_ssid, ta_ssid, LV_ALIGN_OUT_TOP_LEFT, 0, 0);
	

	ta_pwd = lv_ta_create(obj, NULL);
	lv_ta_set_text(ta_pwd,(const char*)gui_wlan_setting.wlan_pwd);
	lv_ta_set_one_line(ta_pwd, true);
	lv_obj_set_width(ta_pwd, LV_HOR_RES / 2 - 20);
	lv_ta_set_cursor_type(ta_pwd, LV_CURSOR_LINE | LV_CURSOR_HIDDEN);	/* 临时隐藏光标,选择后再显示 */
	lv_obj_align(ta_pwd, ta_ssid, LV_ALIGN_OUT_BOTTOM_MID, 0, 40);
	lv_obj_set_event_cb(ta_pwd, ta_event_cb);
	lv_ta_set_pwd_mode(ta_pwd, true);						/* 设置为密文模式 */
	lv_ta_set_max_length(ta_pwd, 16);
	lv_ta_set_pwd_show_time(ta_pwd, 1000);					/* 设置密码可以查看的最长时间,过了这个时间以后变成*号 */

	

	lv_obj_t * label_pwd = lv_label_create(obj, NULL);
	lv_label_set_text(label_pwd, "PWD:");
	lv_obj_align(label_pwd, ta_pwd, LV_ALIGN_OUT_TOP_LEFT, 0, 0);


	ta_ip = lv_ta_create(obj, NULL);
	lv_ta_set_text(ta_ip,(const char*)gui_wlan_setting.server_ip);
	lv_ta_set_one_line(ta_ip, true);
	lv_obj_set_width(ta_ip, LV_HOR_RES / 2 - 20);
	lv_ta_set_cursor_type(ta_ip, LV_CURSOR_LINE | LV_CURSOR_HIDDEN);
	lv_obj_align(ta_ip, ta_ssid, LV_ALIGN_OUT_BOTTOM_MID, 0, 120);
	lv_obj_set_event_cb(ta_ip, ta_event_cb);
	lv_ta_set_max_length(ta_ip, 16);
	lv_ta_set_accepted_chars(ta_ip, "0123456789.");			/* 设置可接收的字符,相当于正则表达式 */

	lv_obj_t * label_ip = lv_label_create(obj, NULL);
	lv_label_set_text(label_ip, "SERVER_IP:");
	lv_obj_align(label_ip, ta_ip, LV_ALIGN_OUT_TOP_LEFT, 0, 0);


	ta_port = lv_ta_create(obj, NULL);
	lv_ta_set_text(ta_port,(const char*)gui_wlan_setting.server_port);
	lv_ta_set_one_line(ta_port, true);
	lv_obj_set_width(ta_port, LV_HOR_RES / 2 - 20);
	lv_ta_set_cursor_type(ta_port, LV_CURSOR_LINE | LV_CURSOR_HIDDEN);
	lv_obj_align(ta_port, ta_ssid, LV_ALIGN_OUT_BOTTOM_MID, 0, 200);
	lv_obj_set_event_cb(ta_port, ta_event_cb);
	lv_ta_set_max_length(ta_port, 5);
	lv_ta_set_accepted_chars(ta_port, "0123456789");
	lv_ta_set_placeholder_text(ta_port, "0-65535");		/* 在文本输入框为空的时候,设置一个提示文本 */

	lv_obj_t * label_port = lv_label_create(obj, NULL);
	lv_label_set_text(label_port, "SERVER_PORT:");
	lv_obj_align(label_port, ta_port, LV_ALIGN_OUT_TOP_LEFT, 0, 0);

	/* 创建确定按钮 */
	lv_obj_t *btn_ok = lv_btn_create(obj,NULL);
	lv_obj_set_width(btn_ok, LV_HOR_RES - 20);
	lv_obj_set_event_cb(btn_ok,btn_save_event_cb);
	lv_obj_align(btn_ok, obj, LV_ALIGN_IN_TOP_MID, 0, 400);
	lv_style_copy(&style_label_ok,&lv_style_plain_color);
	style_label_ok.text.color = LV_COLOR_BLACK;
	style_label_ok.text.font = &gb2312_puhui16;
	lv_obj_t *label_ok = lv_label_create(btn_ok,NULL);
	lv_label_set_style(label_ok,LV_LABEL_STYLE_MAIN,&style_label_ok);
	lv_label_set_text(label_ok,"保存配置");
	

	//lv_obj_set_event_cb(btn_ok,);


	/* 创建键盘 */
	kb = lv_kb_create(obj, NULL);
	lv_obj_set_event_cb(kb, kb_event_cb);
	lv_obj_set_height(kb,LV_VER_RES/3);
	lv_obj_align(kb, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, 0);

	/* 设置键盘对应的输入区域 */
	lv_kb_set_ta(kb, ta_ssid);
	lv_kb_set_cursor_manage(kb, true);

	return obj;
}

static void kb_event_cb(lv_obj_t * obj, lv_event_t event)
{
	/* Just call the regular event handler */
	lv_kb_def_event_cb(obj, event);

}



static void ta_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_CLICKED)
	{
		/* Focus on the clicked text area */
		if (kb != NULL)
			lv_kb_set_ta(kb, obj);
	}
}

static void btn_save_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED)
	{

		gui_hal_setting_read(&gui_wlan_setting);
		memcpy(gui_wlan_setting.wlan_ssid,lv_ta_get_text(ta_ssid),lv_ta_get_max_length(ta_ssid));
		memcpy(gui_wlan_setting.wlan_pwd, lv_ta_get_text(ta_pwd), lv_ta_get_max_length(ta_pwd));
		memcpy(gui_wlan_setting.server_ip, lv_ta_get_text(ta_ip), lv_ta_get_max_length(ta_ip));
		memcpy(gui_wlan_setting.server_port, lv_ta_get_text(ta_port), lv_ta_get_max_length(ta_port));
		gui_hal_setting_write(&gui_wlan_setting);
		message_box(lv_scr_act(), "保存成功");
	}
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

