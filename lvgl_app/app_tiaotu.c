/*
*******************************************************************************************************
*
* 文件名称 : app_tiaotu.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 跳兔介绍应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"


static const char introduction[] = { "\xe4\xb8\x93\xe4\xb8\x9a\xe7\x9a\x84\xe7\x89\xa9\xe8\x81\x94\xe7\xbd\x91,\xe5\xb5\x8c\xe5\x85\xa5\xe5\xbc\x8f,\xe4\xba\xba\xe6\x9c\xba\xe7\x95\x8c\xe9\x9d\xa2\xe8\xa7\xa3\xe5\x86\xb3\xe6\x96\xb9\xe6\xa1\x88\xe4\xbe\x9b\xe5\xba\x94\xe5\x95\x86\xe4\xbb\xa5\xe5\x8f\x8a\xe7\x94\x9f\xe4\xba\xa7\xe5\x8a\xa0\xe5\xb7\xa5\xe5\x95\x86" };
static const char url[] = { LV_SYMBOL_HOME" www.whtiaotu.com" };
static const char openrabbit[] = { "\"\xe5\xbc\x80\xe6\xba\x90\xe5\x85\x94\"\xe8\x87\xb4\xe5\x8a\x9b\xe4\xba\x8e\xe5\xbc\x80\xe6\xba\x90\xe6\x8a\x80\xe6\x9c\xaf\xe7\x9a\x84\xe5\x88\x86\xe4\xba\xab" };


static void obj_event_cb(lv_obj_t * obj, lv_event_t event);




static void obj_event_cb(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
	case LV_EVENT_DELETE:

		break;
	default:
		break;
	}
}


lv_obj_t * app_tiaotu_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	static lv_style_t style_label;
	static lv_style_t style_label_url;
	static lv_style_t style_bg;

	lv_obj_t *obj = lv_app_common_int(lv_scr_act(), "关于跳兔");
	lv_obj_set_event_cb(obj, obj_event_cb);

	lv_style_copy(&style_bg,&lv_style_scr);
	style_bg.body.main_color = lv_color_hex(0x43a9f1);
	style_bg.body.grad_color = style_bg.body.main_color;
	lv_obj_set_style(obj, &style_bg);


	lv_obj_t *img;
	img = lv_img_create(obj, NULL);
	lv_img_set_src(img,&tiaotu_logo400x300);
	lv_obj_align(img,obj,LV_ALIGN_IN_TOP_MID,0,60);

	lv_obj_t *label_introduction = lv_label_create(obj,NULL);
	lv_obj_align(label_introduction, obj, LV_ALIGN_IN_TOP_LEFT, 0, 300);
	lv_style_copy(&style_label,lv_label_get_style(label_introduction,LV_LABEL_STYLE_MAIN));
	style_label.text.font = &gb2312_puhui16;
	lv_label_set_style(label_introduction, LV_LABEL_STYLE_MAIN, &style_label);
	lv_label_set_long_mode(label_introduction, LV_LABEL_LONG_BREAK);
	lv_label_set_static_text(label_introduction,introduction);
	lv_obj_set_width(label_introduction, lv_obj_get_width(obj));
	lv_label_set_align(label_introduction, LV_LABEL_ALIGN_CENTER);

	lv_obj_t *label_url = lv_label_create(obj, NULL);
	lv_label_set_static_text(label_url, url);
	lv_style_copy(&style_label_url, lv_label_get_style(label_url, LV_LABEL_STYLE_MAIN));
	style_label_url.text.font = &lv_font_roboto_28;
	lv_label_set_style(label_url, LV_LABEL_STYLE_MAIN, &style_label_url);
	lv_obj_align(label_url, label_introduction, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);


	img = lv_img_create(obj, NULL);
	lv_img_set_src(img, &tiaotu_qrcode);
	lv_obj_align(img, obj, LV_ALIGN_IN_TOP_LEFT, 0, 450);

	lv_obj_t *label_openrabbit = lv_label_create(obj, NULL);
	lv_label_set_static_text(label_openrabbit, openrabbit);
	lv_label_set_style(label_openrabbit, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label_openrabbit, obj, LV_ALIGN_IN_TOP_MID, 0, 600);


	return obj;
}







/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

