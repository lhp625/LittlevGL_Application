/*
*******************************************************************************************************
*
* 文件名称 : gui_user.h
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : GUI 与 RTOS 和硬件的交互文件，使用时注意区分 MCU 端和模拟器端
*
*******************************************************************************************************
*/

#ifndef _GUI_USER_H
#define _GUI_USER_H

/* 头文件 -----------------------------------------------------------*/
#include "lvgl.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#if defined (__CC_ARM)
#include "bsp.h"
#include "task.h"
#endif

/* 宏定义 -----------------------------------------------------------*/

#if defined (_MSC_VER)
#define MQ_MAX_LEN 128
#endif

/* ICON描述信息结构体 */
typedef struct
{
	lv_img_dsc_t *icon_img;
	const char *text_en;
	const char *text_cn;
	lv_obj_t * (*app)(lv_obj_t *parent, void *user_data);

}icon_item;




/* setting结构体定义 */
/* 请跟BSP的同类结构体保持一致 */
typedef struct{
	uint8_t *wlan_ssid;         //WIFI名称
	uint8_t *wlan_pwd;          //WIFI密码
	uint8_t *server_ip;         //服务器IP
	uint8_t *server_port;       //服务器端口
	uint8_t backlight;          //屏幕背光
	uint8_t wlan_power;	        //WIFI开关
	uint8_t wallpaper_index;     //壁纸的编号
	uint8_t theme_index;    	//主题的编号
	uint8_t sysclk;    			//系统时钟
	uint8_t system_update;    	//系统更新标志位
}gui_setting;


//时间结构体
typedef struct
{
	//24小时制
	uint8_t Year;		//年份，最小00   最大99  时间范围  2000-2099
	uint8_t Month;
	uint8_t Date;
	uint8_t Hours;
	uint8_t Minutes;
	uint8_t Seconds;
}gui_rtc_date_time;

/* 主题 */
typedef enum {
	THEME_MATERIAL = 0,
	THEME_NIGHT,
	THEME_ALIEN,
	THEME_ZEN,
	THEME_NEMO,
	THEME_MONO,
	THEME_DEFAULT,
	THEME_TEMPL
}gui_theme;



/* 消息头定义 */
/* 请跟BSP的同类结构体保持一致 */
enum{
	GUI_MQ_TEMP = 0,		   	 /* 温度 */
	GUI_MQ_WLAN_STATUS,          /* WIFI状态更新 */
	GUI_MQ_KEY,                  /* 按键信息 */
	GUI_MQ_WLAN_UPDATA_SET,      /* 更新WIFI设置 */
	GUI_MQ_MPU_UPDATA,           /* MPU6050三轴数据更新 */
};

/* 实体按键定义 */
/* 请跟BSP的同类结构体保持一致 */
enum{
	HAL_KEY_NULL = 0,

	HAL_KEY_LEFT_CLICKED,
	HAL_KEY_LEFT_RELEASED,
	HAL_KEY_LEFT_LONG,

	HAL_KEY_WKUP_CLICKED,
	HAL_KEY_WKUP_RELEASED,
	HAL_KEY_WKUP_LONG,

	HAL_KEY_RIGHT_CLICKED,
	HAL_KEY_RIGHT_RELEASED,
	HAL_KEY_RIGHT_LONG,

	HAL_KEY_POWER_CLICKED,
	HAL_KEY_POWER_RELEASED,
	HAL_KEY_POWER_LONG,


};


#define LV_COLOR_MAKE_USER(r8, g8, b8) ({{b8 >> 3, g8 >> 2, r8 >> 3}})
#define LV_COLOR_HEX(c) LV_COLOR_MAKE_USER(((uint32_t)((uint32_t)c >> 16) & 0xFF),((uint32_t)((uint32_t)c >> 8) & 0xFF),((uint32_t) c & 0xFF))


/* 全局变量 ----------------------------------------------------------*/

/* 中文字体样式定义 */
extern lv_style_t style_cn_12;
extern lv_style_t style_cn_16;
extern lv_style_t style_cn_24;
extern lv_style_t style_cn_32;

/* 字体全局变量 */
LV_FONT_DECLARE(gb2312_puhui12)
LV_FONT_DECLARE(gb2312_puhui16)
LV_FONT_DECLARE(gb2312_puhui24)
LV_FONT_DECLARE(gb2312_puhui32)

/* 图片全局变量 */

/* 桌面图片变量定义 */
extern lv_img_dsc_t img_desktop;
extern lv_img_dsc_t icon_led;
extern lv_img_dsc_t icon_key;
extern lv_img_dsc_t icon_beep;
extern lv_img_dsc_t icon_album;
extern lv_img_dsc_t icon_wireless;
extern lv_img_dsc_t icon_wifi;
extern lv_img_dsc_t icon_wechat;
extern lv_img_dsc_t icon_weather;
extern lv_img_dsc_t icon_temp;
extern lv_img_dsc_t icon_set;
extern lv_img_dsc_t icon_note;
extern lv_img_dsc_t icon_mall;
extern lv_img_dsc_t icon_light;
extern lv_img_dsc_t icon_ir;
extern lv_img_dsc_t icon_folder;
extern lv_img_dsc_t icon_tiaotu;
extern lv_img_dsc_t icon_triaxial;
extern lv_img_dsc_t icon_nbiot;
extern lv_img_dsc_t icon_onenet;
extern lv_img_dsc_t icon_touchpad;

/* 微信图片 */
/* 微信 */
extern lv_img_dsc_t wechat_aliyun;
extern lv_img_dsc_t wechat_armfly;
extern lv_img_dsc_t wechat_ebf;
extern lv_img_dsc_t wechat_esp;
extern lv_img_dsc_t wechat_huxiaozhi;
extern lv_img_dsc_t wechat_mi;
extern lv_img_dsc_t wechat_myir;
extern lv_img_dsc_t wechat_nuclei;
extern lv_img_dsc_t wechat_onenet;
extern lv_img_dsc_t wechat_quectel;
extern lv_img_dsc_t wechat_rtt;
extern lv_img_dsc_t wechat_st;
extern lv_img_dsc_t wechat_tiaotu;
extern lv_img_dsc_t wechat_asus;
extern lv_img_dsc_t wechat_21ic;
extern lv_img_dsc_t wechat_jingdong;
extern lv_img_dsc_t wechat_grouphelp;
extern lv_img_dsc_t wechat_shunfeng;
extern lv_img_dsc_t wechat_exmail;
extern lv_img_dsc_t wechat_lcsc;
/* 通讯录 */
extern lv_img_dsc_t wechat_newfriend;
extern lv_img_dsc_t wechat_groupchat;
extern lv_img_dsc_t wechat_label;
extern lv_img_dsc_t wechat_public;
/* 发现 */
extern lv_img_dsc_t wechat_moments;
extern lv_img_dsc_t wechat_scan;
extern lv_img_dsc_t wechat_shake;
extern lv_img_dsc_t wechat_top_stories;
extern lv_img_dsc_t wechat_search;
extern lv_img_dsc_t wechat_people_nearby;
extern lv_img_dsc_t wechat_games;
extern lv_img_dsc_t wechat_mini_programs;
/* 我 */
extern lv_img_dsc_t wechat_id;
extern lv_img_dsc_t wechat_pay;
extern lv_img_dsc_t wechat_favorites;
extern lv_img_dsc_t wechat_my_posts;
extern lv_img_dsc_t wechat_cards_offers;
extern lv_img_dsc_t wechat_sticker_gallery;
extern lv_img_dsc_t wechat_settings;
/* 标题栏 */
extern lv_img_dsc_t wechat_chats0;
extern lv_img_dsc_t wechat_contacts0;
extern lv_img_dsc_t wechat_discover0;
extern lv_img_dsc_t wechat_me0;
extern lv_img_dsc_t wechat_chats1;
extern lv_img_dsc_t wechat_contacts1;
extern lv_img_dsc_t wechat_discover1;
extern lv_img_dsc_t wechat_me1;

/* 蜂鸣器APP */
extern lv_img_dsc_t beep_open;

/* 设置APP */
extern lv_img_dsc_t set_backlight;
extern lv_img_dsc_t set_calendar;
extern lv_img_dsc_t set_theme;
extern lv_img_dsc_t set_wallpaper;
extern lv_img_dsc_t set_wlan;
extern lv_img_dsc_t set_about;
extern lv_img_dsc_t set_system;
extern lv_img_dsc_t set_wireless;
extern lv_img_dsc_t set_screen;
extern lv_img_dsc_t set_ram;
extern lv_img_dsc_t set_memory;
extern lv_img_dsc_t set_mcu;
extern lv_img_dsc_t set_bat;
extern lv_img_dsc_t set_clock;
extern lv_img_dsc_t set_system_update;
extern lv_img_dsc_t set_reset_device;

/* 跳兔 */
extern lv_img_dsc_t tiaotu_logo400x300;
extern lv_img_dsc_t tiaotu_qrcode;

/* 天气 */
extern lv_img_dsc_t weather_bg0;
extern lv_img_dsc_t weather_sunny;
extern lv_img_dsc_t weather_wind;
extern lv_img_dsc_t weather_mi;
extern lv_img_dsc_t weather_iconfont;
extern lv_img_dsc_t weather_tiaotu;
extern lv_img_dsc_t weather_shower;
extern lv_img_dsc_t weather_snow;
extern lv_img_dsc_t weather_windbreaker;
extern lv_img_dsc_t weather_umbrella;
extern lv_img_dsc_t weather_sun_protection;
extern lv_img_dsc_t weather_car;
extern lv_img_dsc_t weather_motion;
extern lv_img_dsc_t weather_sunny_big;


/* 图片数据结构体 */


/* 主题编号 */
extern gui_theme gui_current_theme;

/* 保存当前APP的指针 */
extern lv_obj_t * current_app_obj_user;

/* 函数声明 ----------------------------------------------------------*/


/* APP函数声明 */
lv_obj_t * lv_app_common_int(lv_obj_t *parent, const char *title);
extern lv_obj_t * app_led_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_wechat_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_beep_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_wlan_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * message_box(lv_obj_t *parent, const char *title);
extern lv_obj_t * app_set_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_light_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_ir_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_nb_iot_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_key_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_tiaotu_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_touchpad_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_weather_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * app_mall_create(lv_obj_t *parent, void *user_data);
extern lv_obj_t * message_box_win(lv_obj_t *parent, const char *title, const char *text, const char *btn1_text, const char *btn2_text);
extern lv_obj_t * app_temperature_create(lv_obj_t *parent, void *user_data);




void lv_app_main(void);

uint32_t color_change(uint32_t color);

uint8_t lv_load_user_font_from_file(void);	//加载字体
uint8_t lv_load_user_img_from_file(void);	//加载图片
lv_img_dsc_t *lv_load_image_from_file(const char *file_name);
uint8_t lv_load_img_bin_from_file(lv_img_dsc_t *image, const char *file_name);
void lv_font_cn_style_init(void);

/* 硬件操作相关函数 */
void gui_hal_led_toggle(void);

void gui_hal_backlight(uint8_t light);

void gui_hal_led_set_color(uint32_t color);
uint32_t gui_hal_led_get_current_color(void);

uint8_t gui_hal_adc_light_get_ratio(void);

uint8_t gui_hal_cpuusage_get(void);

void gui_hal_beep_duty_cycle(uint8_t cycle);
void gui_hal_beep_switch(uint8_t sw);

uint32_t gui_hal_get_sysclk(void);

void gui_hal_get_time(gui_rtc_date_time *date_time);
void gui_hal_set_time(gui_rtc_date_time *date_time);

void gui_hal_setting_read(gui_setting *gui_read_setting);
void gui_hal_setting_write(gui_setting *gui_write_setting);

uint8_t gui_hal_message_recv(uint8_t *pbuffer, uint8_t len);
uint8_t gui_hal_message_send(uint8_t *pbuffer, uint8_t len);

void gui_hal_ir_send(uint8_t addr, uint8_t key, uint8_t times);
uint8_t gui_hal_ir_receive(void);
void gui_hal_ir_receive_start_scan(void);
void gui_hal_ir_receive_stop_scan(void);

void gui_hal_bat_get(uint8_t *ele, bool *stdby, bool *chrg);

void gui_hal_reset_device_of_update(void);
void gui_hal_reset_device(void);

void gui_hal_delay_ms(uint16_t ms);

char * gui_hal_get_firmware_version(void);

float gui_hal_get_cpu_temperature(void);
float gui_hal_get_mpu6050_temperature(void);

#ifdef NB_IOT
typedef struct
{
	uint8_t band[2];
	uint8_t csq[3];
	uint8_t	imsi[16];
	uint8_t	imei[16];
	uint8_t signal_power[16];
	uint8_t	total_power[16];
	uint8_t	tx_power[16];
	uint8_t tx_time[16];
	uint8_t	rx_time[16];
	uint8_t	cell_id[16];
	uint8_t ecl[16];
	uint8_t	snr[16];
	uint8_t	earfcn[16];
	uint8_t pci[16];
	uint8_t rsrq[16];
}_nb_iot_info;
uint8_t gui_hal_nb_message_recv(uint8_t *pbuffer, uint16_t len);
uint8_t gui_hal_nb_message_send(uint8_t *pbuffer, uint8_t len);
#endif




#endif



/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

