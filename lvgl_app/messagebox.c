/*
*******************************************************************************************************
*
* 文件名称 : messagbox.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 消息提示框应用
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

static lv_obj_t *mbox_parent;

static const char * btns2[] = { "Ok", "Cancel", "" };



static void mbox_event_cb(lv_obj_t *obj, lv_event_t event)
{
	if (event == LV_EVENT_DELETE) {
		/* 删除父窗口,去除模态化效果 */
		lv_obj_del_async(lv_obj_get_parent(obj));
		//obj = NULL; /* happens before object is actually deleted! */

	}
	else if (event == LV_EVENT_VALUE_CHANGED) {
		if (mbox_parent != NULL)
		{
			lv_mbox_get_active_btn(obj) ? lv_event_send(mbox_parent, LV_EVENT_CANCEL, NULL) : lv_event_send(mbox_parent, LV_EVENT_APPLY, NULL);
		}
		
		/* 自动关闭 */
		lv_mbox_start_auto_close(obj, 0);

	}


}



lv_obj_t * message_box(lv_obj_t *parent, const char *title)
{
	
	static lv_style_t style_mbox;
	static lv_style_t style_modal;

	mbox_parent = parent;

	/* 创建一个窗口用于模态化效果 */
	/* Create a full-screen background */
	lv_style_copy(&style_modal, &lv_style_plain_color);
	/* Set the background's style */
	style_modal.body.main_color = style_modal.body.grad_color = LV_COLOR_BLACK;
	style_modal.body.opa = LV_OPA_50;
	/* Create a base object for the modal background */
	lv_obj_t *obj = lv_obj_create(parent, NULL);
	lv_obj_set_style(obj, &style_modal);
	lv_obj_set_pos(obj, 0, 0);
	lv_obj_set_size(obj, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_opa_scale_enable(obj, true); /* Enable opacity scaling for the animation */

	

	/* 创建消息提示框 */
	lv_obj_t *mbox = lv_mbox_create(obj, NULL);
	lv_obj_set_drag(mbox,true);
	lv_style_copy(&style_mbox, lv_mbox_get_style(mbox, LV_MBOX_STYLE_BG));
	style_mbox.text.font = style_cn_16.text.font;
	lv_mbox_set_style(mbox, LV_MBOX_STYLE_BG, &style_mbox);
	
	

	lv_mbox_set_text(mbox, title);
	lv_mbox_add_btns(mbox, btns2);
	lv_obj_align(mbox, obj, LV_ALIGN_IN_TOP_MID, 0, 150);
	lv_obj_set_event_cb(mbox, mbox_event_cb);

	return obj;
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

