/*
*******************************************************************************************************
*
* 文件名称 : app_set_time.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 时间设置
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

static const uint8_t month_en[] = "January\nFebruary\nMarch\nApril\nMay\nJune\nJuly\nAugust\nSeptember\nOctober\nNovember\nDecember";
static const uint8_t year[] = "2019\n2020\n2021\n2022\n2023\n2024\n2025\n2026\n2027\n2028";

static uint8_t date_buf[100];

static lv_obj_t *roller_year;
static lv_obj_t *roller_month;
static lv_obj_t *roller_date;
static lv_obj_t *roller_hour;
static lv_obj_t *roller_min;
static lv_obj_t *label_date;


static void roller_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_ok_event_cb(lv_obj_t * obj, lv_event_t event);
static void btn_cancel_event_cb(lv_obj_t * obj, lv_event_t event);




lv_obj_t * app_set_time_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	gui_rtc_date_time gui_set_init_time;

	uint8_t date[100] = { 0 };
	uint8_t min[200] = { 0 };
	uint8_t hour[100] = { 0 };

	uint8_t i = 0;
	uint8_t buf[10] = { 0 };

	static lv_style_t style_modal;
	static lv_style_t style_label_date;
	static lv_style_t style_roller_calendar;

	gui_hal_get_time(&gui_set_init_time);

	lv_style_copy(&style_modal, &lv_style_plain_color);
	style_modal.body.main_color = style_modal.body.grad_color = LV_COLOR_BLACK;
	style_modal.body.opa = LV_OPA_50;
	
	lv_obj_t *obj = lv_obj_create(parent, NULL);
	lv_obj_set_size(obj, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_style(obj, &style_modal);


	/* 填充日期时间缓冲区 */
	for (i = 1; i <= 31; i++)
	{
		if (i < 31)
			sprintf(buf, "%d\n", i);
		else
			sprintf(buf, "%d", i);
		strcat(date, buf);
	}

	for (i = 0; i <= 23; i++)
	{
		if (i < 23)
			sprintf(buf, "%d\n", i);
		else
			sprintf(buf, "%d", i);
		strcat(hour, buf);
	}

	for (i = 0; i <= 59; i++)
	{
		if (i < 59)
			sprintf(buf, "%d\n", i);
		else
			sprintf(buf, "%d", i);
		strcat(min, buf);
	}

	lv_style_copy(&style_roller_calendar,lv_theme_get_current()->style.roller.bg);
	style_roller_calendar.body.opa = LV_OPA_90;
	style_roller_calendar.body.radius = 10;

	roller_year = lv_roller_create(obj, NULL);							/* 创建roller控件 */
	lv_obj_set_pos(roller_year, 0, 200);									/* 设置坐标 */
	lv_roller_set_options(roller_year, year, LV_ROLLER_MODE_INIFINITE);	/* 添加内容 */
	lv_roller_set_fix_width(roller_year, 100);							/* 设置宽度 */
	lv_roller_set_visible_row_count(roller_year, 8);					/* 设置显示的行数 */
	lv_obj_set_event_cb(roller_year, roller_event_cb);					/* 设置事件回调函数 */
	lv_roller_set_selected(roller_year, gui_set_init_time.Year-19, true);						/* 手动设置选择项 */
	lv_roller_set_style(roller_year, LV_ROLLER_STYLE_BG, &style_roller_calendar);


	roller_month = lv_roller_create(obj, NULL);
	lv_obj_set_pos(roller_month, 100, 200);
	lv_roller_set_options(roller_month, month_en, LV_ROLLER_MODE_INIFINITE);
	lv_roller_set_fix_width(roller_month, 100);
	lv_roller_set_visible_row_count(roller_month, 8);
	lv_obj_set_event_cb(roller_month, roller_event_cb);
	lv_roller_set_style(roller_month, LV_ROLLER_STYLE_BG, &style_roller_calendar);
	lv_roller_set_selected(roller_month, gui_set_init_time.Month-1,true);

	roller_date = lv_roller_create(obj, NULL);
	lv_obj_set_pos(roller_date, 200, 200);
	lv_roller_set_options(roller_date, date, LV_ROLLER_MODE_INIFINITE);
	lv_roller_set_fix_width(roller_date, 100);
	lv_roller_set_visible_row_count(roller_date, 8);
	lv_obj_set_event_cb(roller_date, roller_event_cb);
	lv_roller_set_style(roller_date, LV_ROLLER_STYLE_BG, &style_roller_calendar);
	lv_roller_set_selected(roller_date, gui_set_init_time.Date-1, true);

	roller_hour = lv_roller_create(obj, NULL);
	lv_obj_set_pos(roller_hour, 300, 200);
	lv_roller_set_options(roller_hour, hour, LV_ROLLER_MODE_INIFINITE);
	lv_roller_set_fix_width(roller_hour, 90);
	lv_roller_set_visible_row_count(roller_hour, 8);
	lv_obj_set_event_cb(roller_hour, roller_event_cb);
	lv_roller_set_selected(roller_hour, gui_set_init_time.Hours, true);
	lv_roller_set_style(roller_hour, LV_ROLLER_STYLE_BG, &style_roller_calendar);

	roller_min = lv_roller_create(obj, NULL);
	lv_obj_set_pos(roller_min, 390, 200);
	lv_roller_set_options(roller_min, min, LV_ROLLER_MODE_INIFINITE);
	lv_roller_set_fix_width(roller_min, 90);
	lv_roller_set_visible_row_count(roller_min, 8);
	lv_obj_set_event_cb(roller_min, roller_event_cb);
	lv_roller_set_selected(roller_min, gui_set_init_time.Minutes, true);
	lv_roller_set_style(roller_min, LV_ROLLER_STYLE_BG, &style_roller_calendar);


	lv_style_copy(&style_label_date,&lv_style_plain_color);
	style_label_date.text.font = &gb2312_puhui16;
	style_label_date.text.color = LV_COLOR_WHITE;
	style_label_date.body.radius = 10;
	/* 创建label用于显示选择的时间 */
	label_date = lv_label_create(obj, NULL);
	lv_label_set_long_mode(label_date, LV_LABEL_LONG_EXPAND);
	lv_label_set_text(label_date, "");
	lv_label_set_body_draw(label_date,true);
	lv_label_set_style(label_date,LV_LABEL_STYLE_MAIN,&style_label_date);

	/* 手动发送一个事件,使得label显示的时间可以更新 */
	lv_event_send(roller_min, LV_EVENT_VALUE_CHANGED, NULL);

	lv_obj_t *label;

	lv_obj_t *btn_ok = lv_btn_create(obj, NULL);
	lv_obj_set_pos(btn_ok,20,450);
	lv_obj_set_width(btn_ok,200);
	label = lv_label_create(btn_ok, NULL);
	lv_label_set_style(label,LV_LABEL_STYLE_MAIN,&style_label_date);
	lv_label_set_text(label,"保存");
	lv_obj_set_event_cb(btn_ok, btn_ok_event_cb);

	lv_obj_t *btn_cancel = lv_btn_create(obj, NULL);
	lv_obj_set_pos(btn_cancel, 260, 450);
	lv_obj_set_width(btn_cancel, 200);
	label = lv_label_create(btn_cancel, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_date);
	lv_label_set_text(label, "取消");
	lv_obj_set_event_cb(btn_cancel,btn_cancel_event_cb);

	return obj;
}

static void btn_cancel_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED)
	{
		/* 退出窗口 */
		lv_obj_del(lv_obj_get_parent(obj));

	}
}

static void btn_ok_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED)
	{

		gui_rtc_date_time gui_set_time;

		gui_set_time.Year = lv_roller_get_selected(roller_year)+19;
		gui_set_time.Month = lv_roller_get_selected(roller_month) + 1;
		gui_set_time.Date = lv_roller_get_selected(roller_date) + 1;
		gui_set_time.Hours = lv_roller_get_selected(roller_hour);
		gui_set_time.Minutes = lv_roller_get_selected(roller_min);

		printf("%d-%d-%d %d:%d", gui_set_time.Year, gui_set_time.Month, gui_set_time.Date, gui_set_time.Hours, gui_set_time.Minutes);

		gui_hal_set_time(&gui_set_time);

		/* 退出窗口 */
		lv_obj_del(lv_obj_get_parent(obj));

	}
}

static void roller_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_VALUE_CHANGED)
	{
		char buf[100];
		memset(date_buf, 0, sizeof(date_buf));

		lv_roller_get_selected_str(roller_year, buf, sizeof(buf));
		strcat(date_buf, buf);
		strcat(date_buf, "-");
		memset(buf, 0, sizeof(buf));

		sprintf(buf, "%02d", lv_roller_get_selected(roller_month) + 1);
		strcat(date_buf, buf);
		strcat(date_buf, "-");
		memset(buf, 0, sizeof(buf));

		sprintf(buf, "%02d", lv_roller_get_selected(roller_date) + 1);		/* 获取选择的索引 */
		//lv_roller_get_selected_str(roller_date, buf, sizeof(buf));
		strcat(date_buf, buf);
		strcat(date_buf, " ");
		memset(buf, 0, sizeof(buf));



		sprintf(buf, "%02d", lv_roller_get_selected(roller_hour));
		//lv_roller_get_selected_str(roller_hour, buf, sizeof(buf));
		strcat(date_buf, buf);
		strcat(date_buf, ":");
		memset(buf, 0, sizeof(buf));

		sprintf(buf, "%02d", lv_roller_get_selected(roller_min));
		//lv_roller_get_selected_str(roller_min, buf, sizeof(buf));
		strcat(date_buf, buf);
		strcat(date_buf, " ");
		memset(buf, 0, sizeof(buf));

		lv_label_set_text(label_date, date_buf);						/* 设置选择的时间 */
		lv_obj_align(label_date, NULL, LV_ALIGN_IN_TOP_MID, 0, 160);		/* 文本改变后重新设置一下位置 */
	}
}


/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

