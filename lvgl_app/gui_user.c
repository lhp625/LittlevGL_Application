/*
*******************************************************************************************************
*
* 文件名称 : gui_user.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : GUI 与 RTOS 和硬件的交互文件，使用时注意区分 MCU 端和模拟器端
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"
#include <stdio.h>
#include <stdlib.h>

/* 定义中文字体样式 */
lv_style_t style_cn_12;
lv_style_t style_cn_16;
lv_style_t style_cn_24;
lv_style_t style_cn_32;



/* user_img 描述信息结构体 */
typedef struct
{
	lv_img_dsc_t *icon_img;		/* 图片变量的指针 */
	const char *icon_fname;		/* 文件名 */

}user_img_item;

/* 桌面图片变量定义 */
lv_img_dsc_t img_desktop;
lv_img_dsc_t icon_led;
lv_img_dsc_t icon_key;
lv_img_dsc_t icon_beep;
lv_img_dsc_t icon_album;
lv_img_dsc_t icon_wireless;
lv_img_dsc_t icon_wifi;
lv_img_dsc_t icon_wechat;
lv_img_dsc_t icon_weather;
lv_img_dsc_t icon_temp;
lv_img_dsc_t icon_set;
lv_img_dsc_t icon_note;
lv_img_dsc_t icon_mall;
lv_img_dsc_t icon_light;
lv_img_dsc_t icon_ir;
lv_img_dsc_t icon_folder;
lv_img_dsc_t icon_tiaotu;
lv_img_dsc_t icon_triaxial;
lv_img_dsc_t icon_nbiot;
lv_img_dsc_t icon_onenet;
lv_img_dsc_t icon_touchpad;
/* 微信图片 */
/* 微信 */
lv_img_dsc_t wechat_aliyun;
lv_img_dsc_t wechat_armfly;
lv_img_dsc_t wechat_ebf;
lv_img_dsc_t wechat_esp;
lv_img_dsc_t wechat_huxiaozhi;
lv_img_dsc_t wechat_mi;
lv_img_dsc_t wechat_myir;
lv_img_dsc_t wechat_nuclei;
lv_img_dsc_t wechat_onenet;
lv_img_dsc_t wechat_quectel;
lv_img_dsc_t wechat_rtt;
lv_img_dsc_t wechat_st;
lv_img_dsc_t wechat_tiaotu;
lv_img_dsc_t wechat_asus;
lv_img_dsc_t wechat_21ic;
lv_img_dsc_t wechat_jingdong;
lv_img_dsc_t wechat_grouphelp;
lv_img_dsc_t wechat_shunfeng;
lv_img_dsc_t wechat_exmail;
lv_img_dsc_t wechat_lcsc;





/* 通讯录 */
lv_img_dsc_t wechat_newfriend;
lv_img_dsc_t wechat_groupchat;
lv_img_dsc_t wechat_label;
lv_img_dsc_t wechat_public;
/* 发现 */
lv_img_dsc_t wechat_moments;
lv_img_dsc_t wechat_scan;
lv_img_dsc_t wechat_shake;
lv_img_dsc_t wechat_top_stories;
lv_img_dsc_t wechat_search;
lv_img_dsc_t wechat_people_nearby;
lv_img_dsc_t wechat_games;
lv_img_dsc_t wechat_mini_programs;
/* 我 */
lv_img_dsc_t wechat_id;
lv_img_dsc_t wechat_pay;
lv_img_dsc_t wechat_favorites;
lv_img_dsc_t wechat_my_posts;
lv_img_dsc_t wechat_cards_offers;
lv_img_dsc_t wechat_sticker_gallery;
lv_img_dsc_t wechat_settings;
/* 标题栏 */
lv_img_dsc_t wechat_chats0;
lv_img_dsc_t wechat_contacts0;
lv_img_dsc_t wechat_discover0;
lv_img_dsc_t wechat_me0;
lv_img_dsc_t wechat_chats1;
lv_img_dsc_t wechat_contacts1;
lv_img_dsc_t wechat_discover1;
lv_img_dsc_t wechat_me1;

/* 蜂鸣器APP */
lv_img_dsc_t beep_open;

/* 设置APP */
lv_img_dsc_t set_backlight;
lv_img_dsc_t set_calendar;
lv_img_dsc_t set_theme;
lv_img_dsc_t set_wallpaper;
lv_img_dsc_t set_wlan;
lv_img_dsc_t set_about;
lv_img_dsc_t set_system;
lv_img_dsc_t set_wireless;
lv_img_dsc_t set_screen;
lv_img_dsc_t set_ram;
lv_img_dsc_t set_memory;
lv_img_dsc_t set_mcu;
lv_img_dsc_t set_bat;
lv_img_dsc_t set_clock;
lv_img_dsc_t set_system_update;
lv_img_dsc_t set_reset_device;

/* 跳兔 */
lv_img_dsc_t tiaotu_logo400x300;
lv_img_dsc_t tiaotu_qrcode;

/* weather */
lv_img_dsc_t weather_bg0;
lv_img_dsc_t weather_sunny;
lv_img_dsc_t weather_wind;
lv_img_dsc_t weather_mi;
lv_img_dsc_t weather_iconfont;
lv_img_dsc_t weather_tiaotu;
lv_img_dsc_t weather_shower;
lv_img_dsc_t weather_snow;
lv_img_dsc_t weather_windbreaker;
lv_img_dsc_t weather_umbrella;
lv_img_dsc_t weather_sun_protection;
lv_img_dsc_t weather_car;
lv_img_dsc_t weather_motion;
lv_img_dsc_t weather_sunny_big;



static const user_img_item user_img[]=
{
	/* desktop */
	/* page1 */
	{ &icon_tiaotu, "0:/lvgl/icon_tiaotu.bin" },
	{ &icon_led, "0:/lvgl/icon_led.bin" },
	{ &icon_key, "0:/lvgl/icon_key.bin" },
	{ &icon_beep, "0:/lvgl/icon_beep.bin" },
	{ &icon_album, "0:/lvgl/icon_album.bin" },
	{ &icon_wireless, "0:/lvgl/icon_wireless.bin" },
	{ &icon_wechat, "0:/lvgl/icon_wechat.bin" },
	{ &icon_wifi, "0:/lvgl/icon_wifi.bin" },
	{ &icon_weather, "0:/lvgl/icon_weather.bin" },
	{ &icon_set, "0:/lvgl/icon_set.bin" },
	{ &icon_folder, "0:/lvgl/icon_folder.bin" },
	{ &icon_note, "0:/lvgl/icon_note.bin" },
	{ &icon_nbiot, "0:/lvgl/icon_nbiot.bin" },
	{ &icon_onenet, "0:/lvgl/icon_onenet.bin" },
	{ &icon_touchpad, "0:/lvgl/icon_touchpad.bin" },
	/* page2 */
	{ &icon_mall, "0:/lvgl/icon_mall.bin"},
	{ &icon_light, "0:/lvgl/icon_light.bin"},
	{ &icon_temp, "0:/lvgl/icon_temp.bin"},
	/* page3 */
	{ &icon_ir, "0:/lvgl/icon_ir.bin"},
	{ &icon_triaxial, "0:/lvgl/icon_triaxial.bin"},

	/* set */
	{ &set_backlight, "0:/lvgl/set_backlight.bin"},
	{ &set_theme, "0:/lvgl/set_theme.bin"},
	{ &set_wallpaper, "0:/lvgl/set_wallpaper.bin" },
	{ &set_clock,"0:/lvgl/set_clock.bin"},
	{ &set_wlan, "0:/lvgl/set_wlan.bin"},
	{ &set_calendar, "0:/lvgl/set_calendar.bin" },
	{ &set_about, "0:/lvgl/set_about.bin"},
	{ &set_system_update, "0:/lvgl/set_system_update.bin" },
	{ &set_reset_device, "0:/lvgl/set_reset_device.bin" },

	/* set_about_device */
	{ &set_system, "0:/lvgl/set_system.bin"},
	{ &set_mcu, "0:/lvgl/set_mcu.bin"},
	{ &set_memory, "0:/lvgl/set_memory.bin"},
	{ &set_ram, "0:/lvgl/set_ram.bin"},
	{ &set_screen, "0:/lvgl/set_screen.bin"},
	{ &set_bat, "0:/lvgl/set_bat.bin"},
	{ &set_wireless,"0:/lvgl/set_wireless.bin"},

	/* wechat */
	/* page_chats */
	{ &wechat_tiaotu, "0:/lvgl/wechat_tiaotu.bin" },
	{ &wechat_aliyun, "0:/lvgl/wechat_aliyun.bin" },
	{ &wechat_armfly, "0:/lvgl/wechat_armfly.bin" },
	{ &wechat_ebf, "0:/lvgl/wechat_ebf.bin" },
	{ &wechat_esp, "0:/lvgl/wechat_esp.bin" },
	{ &wechat_huxiaozhi, "0:/lvgl/wechat_huxiaozhi.bin" },
	{ &wechat_mi, "0:/lvgl/wechat_mi.bin" },
	{ &wechat_myir, "0:/lvgl/wechat_myir.bin" },
	{ &wechat_nuclei, "0:/lvgl/wechat_nuclei.bin" },
	{ &wechat_onenet, "0:/lvgl/wechat_onenet.bin" },
	{ &wechat_quectel, "0:/lvgl/wechat_quectel.bin" },
	{ &wechat_rtt, "0:/lvgl/wechat_rtt.bin"},
	{ &wechat_st, "0:/lvgl/wechat_st.bin"},
	{ &wechat_asus, "0:/lvgl/wechat_asus.bin" },
	{ &wechat_21ic, "0:/lvgl/wechat_21ic.bin"},
	{ &wechat_jingdong, "0:/lvgl/wechat_jingdong.bin" },
	{ &wechat_grouphelp, "0:/lvgl/wechat_grouphelp.bin" },
	{ &wechat_shunfeng, "0:/lvgl/wechat_shunfeng.bin" },
	{ &wechat_exmail, "0:/lvgl/wechat_exmail.bin" },
	{ &wechat_lcsc, "0:/lvgl/wechat_lcsc.bin"},
	/* page_contacts */
	{ &wechat_newfriend, "0:/lvgl/wechat_newfriend.bin" },
	{ &wechat_groupchat,"0:/lvgl/wechat_groupchat.bin" },
	{ &wechat_label, "0:/lvgl/wechat_label.bin" },
	{ &wechat_public, "0:/lvgl/wechat_public.bin" },
	/* page_contacts1 */
	/* page_discover */
	{ &wechat_moments,"0:/lvgl/wechat_moments.bin" },
	{ &wechat_scan, "0:/lvgl/wechat_scan.bin" },
	{ &wechat_shake, "0:/lvgl/wechat_shake.bin" },
	{ &wechat_top_stories, "0:/lvgl/wechat_top_stories.bin" },
	{ &wechat_search, "0:/lvgl/wechat_search.bin" },
	{ &wechat_people_nearby, "0:/lvgl/wechat_people_nearby.bin" },
	{ &wechat_games, "0:/lvgl/wechat_games.bin"},
	{ &wechat_mini_programs, "0:/lvgl/wechat_mini_programs.bin"},
	/* page_me */
	{ &wechat_id, "0:/lvgl/wechat_id.bin" },
	{ &wechat_pay, "0:/lvgl/wechat_pay.bin" },
	{ &wechat_favorites, "0:/lvgl/wechat_favorites.bin" },
	{ &wechat_my_posts, "0:/lvgl/wechat_my_posts.bin" },
	{ &wechat_cards_offers, "0:/lvgl/wechat_cards_offers.bin" },
	{ &wechat_sticker_gallery, "0:/lvgl/wechat_sticker_gallery.bin" },
	{ &wechat_settings, "0:/lvgl/wechat_settings.bin" },
	/* header */
	{ &wechat_chats0, "0:/lvgl/wechat_chats0.bin" },
	{ &wechat_contacts0, "0:/lvgl/wechat_contacts0.bin" },
	{ &wechat_discover0, "0:/lvgl/wechat_discover0.bin" },
	{ &wechat_me0, "0:/lvgl/wechat_me0.bin" },
	{ &wechat_chats1, "0:/lvgl/wechat_chats1.bin"},
	{ &wechat_contacts1, "0:/lvgl/wechat_contacts1.bin" },
	{ &wechat_discover1, "0:/lvgl/wechat_discover1.bin" },
	{ &wechat_me1, "0:/lvgl/wechat_me1.bin" },

	/* beep */
	{ &beep_open, "0:/lvgl/beep_open.bin" },
	
	/* tiaotu */
	{ &tiaotu_logo400x300, "0:/lvgl/tiaotu_logo400x300.bin" },
	{ &tiaotu_qrcode, "0:/lvgl/tiaotu_qrcode.bin" },

	/* weather */
	{ &weather_bg0, "0:/lvgl/weather_bg0.bin" }, 
	{ &weather_sunny, "0:/lvgl/weather_sunny.bin" },
	{ &weather_wind, "0:/lvgl/weather_wind.bin" },
	{ &weather_mi, "0:/lvgl/weather_mi.bin" },
	{ &weather_iconfont, "0:/lvgl/weather_iconfont.bin" },
	{ &weather_tiaotu, "0:/lvgl/weather_tiaotu.bin" },
	{ &weather_shower, "0:/lvgl/weather_shower.bin" },
	{ &weather_snow, "0:/lvgl/weather_snow.bin" },
	{ &weather_windbreaker, "0:/lvgl/weather_windbreaker.bin" },
	{ &weather_umbrella, "0:/lvgl/weather_umbrella.bin" },
	{ &weather_sun_protection, "0:/lvgl/weather_sun_protection.bin" },
	{ &weather_car, "0:/lvgl/weather_car.bin" },
	{ &weather_motion, "0:/lvgl/weather_motion.bin" },
	{ &weather_sunny_big, "0:/lvgl/weather_sunny_big.bin" },

};

/**
* @brief 从文件系统加载所有图片到内存
* @param None
* @retval	0-加载成功 非0-加载失败
*/
uint8_t lv_load_user_img_from_file(void)
{
	uint16_t i = 0;
	for (i = 0; i < sizeof(user_img) / sizeof(user_img[0]); ++i)
	{
		if (lv_load_img_bin_from_file(user_img[i].icon_img, user_img[i].icon_fname) != 0)
			return 1;
	}
	return 0;
}



/**
* @brief 初始化中文字体
* @param None
* @retval	0-加载成功 非0-加载失败
*/
void lv_font_cn_style_init(void)
{

	/* 复制style的属性 */
	lv_style_copy(&style_cn_12, &lv_style_pretty_color);
	lv_style_copy(&style_cn_16, &lv_style_pretty_color);
	lv_style_copy(&style_cn_24, &lv_style_pretty_color);
	lv_style_copy(&style_cn_32, &lv_style_pretty_color);

	style_cn_12.text.font = &gb2312_puhui12;
	style_cn_16.text.font = &gb2312_puhui16;
	style_cn_24.text.font = &gb2312_puhui24;
	style_cn_32.text.font = &gb2312_puhui32;

}



/**
* @brief 颜色转换函数
* @param color-原颜色
* @retval	EMWIN需要的颜色
* @note 大小端转换
*/
uint32_t color_change(uint32_t color)
{
	uint32_t ret_color = 0;
	uint8_t r, g, b;
	b = color & 0X000000FF;
	g = (color & 0X0000FF00) >> 8;
	r = (color & 0X00FF0000) >> 16;


	ret_color |= b;
	ret_color <<= 8;

	ret_color |= g;
	ret_color <<= 8;

	ret_color |= r;

	return ret_color;
}




/**
* @brief 从文件系统加载字体
* @param None
* @retval	0-加载成功 非0-加载失败
*/
uint8_t lv_load_user_font_from_file(void)
{
#if defined (__CC_ARM)
	uint8_t res;
	uint32_t br;
	uint8_t *pfont;
	pfont = (uint8_t *)LV_USER_FONT_ADDR;

	res = f_open(&SDFile, "0:/lvgl/gb2312_font.bin", FA_OPEN_EXISTING | FA_READ);
	if (res != FR_OK)
	{
		return 1;
	}

	if (SDFile.obj.objsize > LV_USER_FONT_MAX_SIZE)
	{
		/* 文件过大了 */
		f_close(&SDFile);
		return 2;
	}

	res = f_read(&SDFile, pfont, SDFile.obj.objsize, &br);
	if ((res != FR_OK) || (br == 0))
	{
		f_close(&SDFile);
		return 3;
	}

	f_close(&SDFile);

	return 0;
#elif defined (_MSC_VER)
	return 0;
#endif
}



/**
* @brief 从文件系统获取bmp图片
* @param image-存储图片信息的指针
* @param file_name-文件名
* @retval	0-加载成功 非0-加载失败
*/
uint8_t lv_load_img_bin_from_file(lv_img_dsc_t *image, const char *file_name)
{
#if defined (__CC_ARM)
	uint8_t res;
	uint32_t br;
	uint8_t *pbuf;		//读取数据缓冲区
	uint8_t *pbmp;
	uint32_t i;
	uint32_t header;
	uint32_t file_size;

	if (image == NULL)
		return 1;
	if (file_name == NULL)
		return 1;

	res = f_open(&SDFile, (const TCHAR*)file_name, FA_OPEN_EXISTING | FA_READ);
	if (res != FR_OK)
	{
		return 1;
	}

	file_size = SDFile.obj.objsize;

	if (file_size > 1 * 1024 * 1024)
	{
		/* 文件过大了 */
		f_close(&SDFile);
		return 2;
	}

	/* 为图片申请内存,如果图片需要一直显示,内存不用释放 */
	pbuf = lv_mem_alloc(file_size);
	if (pbuf == NULL)
		return 1;

	res = f_read(&SDFile, pbuf, file_size, &br);
	if ((res != FR_OK) || (br == 0))
	{
		f_close(&SDFile);
		return 3;
	}


	/* 获取头 */
	header = (uint32_t)pbuf[3] << 24;
	header |= (uint32_t)pbuf[2] << 16;
	header |= (uint32_t)pbuf[1] << 8;
	header |= (uint32_t)pbuf[0];

	//rt_kprintf("header:0X%X\r\n",header);
	//rt_kprintf("width:%d height:%d\r\n", (uint16_t)(header >> 10), header >> 21);


	/* 获取图片数据流 */
	pbmp = (uint8_t *)pbuf + 4;

	image->header.cf = pbuf[0];
	image->header.always_zero = 0;
	image->header.w = (uint16_t)(header >> 10);
	image->header.h = (uint16_t)(header >> 21);
	image->data_size = file_size - 4;		//去掉4字节的头,剩余的就是图片数据流长度
	image->data = pbmp;



	f_close(&SDFile);

	return 0;
#elif defined (_MSC_VER)


	uint8_t res;
	uint32_t br;
	uint8_t *pbuf;		//读取数据缓冲区
	uint8_t *pbmp;
	uint32_t i;
	uint32_t header;
	int file_size;
	uint8_t fname[256];

	if (image == NULL)
		return 1;
	if (file_name == NULL)
		return 1;

	sprintf(fname, "../../sd_add_file/%s", file_name + 2);

	//printf("fname:%s\n", fname);

	FILE *fp = NULL;

	fp = fopen(fname, "rb");
	if (fp == NULL)
	{
		printf("open file failed!\r\n");
		return 1;
	}

	//获取文件长度
	fseek(fp, 0, SEEK_END);		/* 定位到文件末 */
	file_size = ftell(fp);		/* 文件长度 */

	fseek(fp, 0, SEEK_SET);		/* 恢复到文件头 */

	if (file_size > 1 * 1024 * 1024)
	{
		/* 文件过大了 */
		fclose(fp);
		return 2;
	}


	pbuf = lv_mem_alloc(file_size);
	if (pbuf == NULL)
		return 1;

	//printf("file_size:%d\n", file_size);

	if (fread(pbuf, 1, file_size, fp) < file_size)
	{
		printf("read file failed!\r\n");
		fclose(fp);
		return 3;
	}

	//printf("file:%x", pbuf[0]);

	/* 获取头 */
	header = (uint32_t)pbuf[3] << 24;
	header |= (uint32_t)pbuf[2] << 16;
	header |= (uint32_t)pbuf[1] << 8;
	header |= (uint32_t)pbuf[0];

	//printf("header:0X%X\r\n", header);
	//printf("width:%d height:%d\r\n", (uint16_t)(header >> 10), header >> 21);


	/* 获取图片数据流 */
	pbmp = (uint8_t *)pbuf + 4;

	image->header.cf = pbuf[0];
	image->header.always_zero = 0;
	image->header.w = (uint16_t)(header >> 10);
	image->header.h = (uint16_t)(header >> 21);
	image->data_size = file_size - 4;		//去掉4字节的头,剩余的就是图片数据流长度
	image->data = pbmp;


	fclose(fp);

	return 0;

#endif

}


/* 存储当前APP的obj指针,NULL表示当前在桌面 */
lv_obj_t * current_app_obj_user = NULL;
/**
* @brief 退出APP
* @param obj-事件对象
* @param event-事件类型
* @retval	None
*/
static void lv_app_btn_back_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_PRESS_LOST)
	{

		lv_style_t *style;
		style = lv_label_get_style(lv_obj_get_child(obj, NULL), LV_LABEL_STYLE_MAIN);
		style->text.color = LV_COLOR_BLACK;

	}
	else if (event == LV_EVENT_PRESSED)
	{
		lv_style_t *style;
		style = lv_label_get_style(lv_obj_get_child(obj,NULL),LV_LABEL_STYLE_MAIN);
		style->text.color = LV_COLOR_GRAY;

	}
	else if (event == LV_EVENT_CLICKED)
	{
		current_app_obj_user = NULL;
		/* 退出窗口 */
		lv_obj_del(lv_obj_get_parent(obj));

	}


}

/**
* @brief APP公用初始化
* @param parent-父对象
* @param title-APP标题
* @retval	创建的APP的对象
*/
lv_obj_t * lv_app_common_int(lv_obj_t *parent, const char *title)
{
	static lv_style_t style_btn_back;
	static lv_style_t style_label_back;
	static lv_style_t style_label_title;
	static lv_style_t style_bg;

	lv_style_copy(&style_btn_back, &lv_style_transp);
	lv_style_copy(&style_label_title, &lv_style_plain_color);
	lv_style_copy(&style_label_back, &lv_style_plain_color);


	if (gui_current_theme == THEME_NEMO)
		style_label_back.text.color = LV_COLOR_WHITE;
	else
		style_label_back.text.color = LV_COLOR_BLACK;
	style_label_back.text.font = &lv_font_roboto_28;


	if (gui_current_theme == THEME_NEMO)
		style_label_title.text.color = LV_COLOR_WHITE;
	else
		style_label_title.text.color = LV_COLOR_BLACK;
	style_label_title.text.font = &gb2312_puhui16;

	lv_obj_t *obj = lv_obj_create(parent, NULL);
	lv_obj_set_size(obj, LV_HOR_RES, LV_VER_RES);

	lv_obj_t *btn_back = lv_btn_create(obj, NULL);
	lv_obj_set_size(btn_back, 100, 60);
	lv_btn_set_layout(btn_back, LV_LAYOUT_OFF);		/* 设置布局 */
	lv_obj_set_event_cb(btn_back, lv_app_btn_back_event_cb);
	lv_btn_set_style(btn_back, LV_BTN_STYLE_REL, &style_btn_back);
	lv_btn_set_style(btn_back, LV_BTN_STYLE_PR, &style_btn_back);

	lv_obj_t *label_back = lv_label_create(btn_back, NULL);
	lv_label_set_style(label_back, LV_LABEL_STYLE_MAIN, &style_label_back);
	lv_label_set_text(label_back, LV_SYMBOL_LEFT);
	lv_obj_align(label_back, btn_back, LV_ALIGN_IN_LEFT_MID, 20, 0);


	lv_obj_t *label_title = lv_label_create(obj, NULL);
	lv_label_set_style(label_title, LV_LABEL_STYLE_MAIN, &style_label_title);
	lv_label_set_text(label_title, title);
	lv_obj_align(label_title, obj, LV_ALIGN_IN_TOP_MID, 0, 16);

	current_app_obj_user = obj;
	return obj;
}




/**
* @brief LED变换
* @param None
* @retval	None
*/
void gui_hal_led_toggle(void)
{
#if defined (__CC_ARM)
	led_toggle();
#elif defined (_MSC_VER)

#endif
}



#if defined (_MSC_VER)
static uint32_t led_color_temp;
#endif
/**
* @brief LED颜色设置
* @param color-LED颜色,注意大小端
* @retval	None
*/
void gui_hal_led_set_color(uint32_t color)
{
#if defined (__CC_ARM)
	led_color(color);
#elif defined (_MSC_VER)
	led_color_temp = color;
#endif
}

/**
* @brief LED获取颜色
* @param None
* @retval	None
*/
uint32_t gui_hal_led_get_current_color(void)
{
#if defined (__CC_ARM)
	return led_current_color();
#elif defined (_MSC_VER)
	return led_color_temp;
#endif

}


/**
* @brief 背光调节
* @param light-背光亮度
* @retval	None
*/
void gui_hal_backlight(uint8_t light)
{

#if defined (__CC_ARM)
	if (light <= 5)
		light = 5;
	lcd_backlight(light);
#elif defined (_MSC_VER)

#endif


}


/**
* @brief 获取光敏ADC的百分比值
* @param light-背光亮度
* @retval	None
*/
uint8_t gui_hal_adc_light_get_ratio(void)
{

#if defined (__CC_ARM)

	uint8_t res = 0;

	res = (float)adc_light_get() / 4096 * 100;

	return res;

#elif defined (_MSC_VER)
	return rand() % 100;
#endif


}



/**
* @brief 获取CPU利用率
* @param light-背光亮度
* @retval	None
*/
uint8_t gui_hal_cpuusage_get(void)
{

#if defined (__CC_ARM)

	static uint8_t cpu_major = 0;
	extern void cpu_usage_get(rt_uint8_t *major, rt_uint8_t *minor);
	cpu_usage_get(&cpu_major, NULL);
	return cpu_major;

#elif defined (_MSC_VER)
	return rand() % 100;
#endif

}

/**
* @brief 获取CPU时钟
* @param None
* @retval	CPU时钟
* @note STM32F429经常会工作在非180MHz的标称频率,所以此函数有必要
*/
uint32_t gui_hal_get_sysclk(void)
{
#if defined ( __CC_ARM   )
	return (uint32_t)SystemCoreClock;
#elif defined (_MSC_VER)
	return 240 * 1000 * 1000;
#endif
}


/**
* @brief 控制无源蜂鸣器的PWM开关
* @param sw 0-关闭 1-打开
* @retval	None
*/
void gui_hal_beep_switch(uint8_t sw)
{
#if defined ( __CC_ARM   )

	if (sw)
		beep_enable(1);
	else
		beep_enable(0);

#elif defined (_MSC_VER)

#endif
}

/**
* @brief 调节无源蜂鸣器的占空比
* @param cycle-占空比
* @retval	None
*/
void gui_hal_beep_duty_cycle(uint8_t cycle)
{
#if defined ( __CC_ARM   )
	beep_adjust(cycle);
#elif defined (_MSC_VER)

#endif
}



/**
* @brief 获取硬件RTC时间
* @param None
* @retval	时间结构体
* @note WIN32获取的是真实时间
*/
void gui_hal_get_time(gui_rtc_date_time *date_time)
{
#if defined ( __CC_ARM   )

	static RTC_DateTypeDef current_date = { 0 };
	static RTC_TimeTypeDef current_time = { 0 };

	rtc_current(&current_date, &current_time);

	gui_rtc_date_time current;
	date_time->Year = current_date.Year;
	date_time->Month = current_date.Month;
	date_time->Date = current_date.Date;
	date_time->Hours = current_time.Hours;
	date_time->Minutes = current_time.Minutes;
	date_time->Seconds = current_time.Seconds;

#elif defined (_MSC_VER)

	/* WIN32端获取的是真实时间 */
	gui_rtc_date_time current_time;

	time_t nowtime;
	nowtime = time(NULL); //获取日历时间  
	struct tm *local;
	local = localtime(&nowtime);  //获取当前系统时间  

	date_time->Year = (local->tm_year - 100); //tm_year是从1900开始的 Yeay是从2000年开始的
	date_time->Month = local->tm_mon+1;
	date_time->Date = local->tm_mday;	//这里读到的日期比实际大1天，原因未知
	date_time->Hours = local->tm_hour;
	date_time->Minutes = local->tm_min;
	date_time->Seconds = local->tm_sec;



#endif
}


/**
* @brief 设置RTC时间
* @param DateTime-需要设置的时间的结构体指针
* @retval	None
* @note WIN32上没有任何操作
*/
void gui_hal_set_time(gui_rtc_date_time *date_time)
{
#if defined ( __CC_ARM   )

	rtc_set_date(date_time->Year, date_time->Month, date_time->Date, 1);
	rtc_set_time(date_time->Hours, date_time->Minutes, date_time->Seconds);
#elif defined (_MSC_VER)

#endif
}




#if defined (_MSC_VER)
static gui_setting gui_setting_temp;
#endif
/**
* @brief 读取配置信息
* @param *gui_read_setting-存储配置信息的地址
* @retval	None
* @note GUI_SETTING结构体请跟硬件的_SETTING保持一致
*/
void gui_hal_setting_read(gui_setting *gui_read_setting)
{
#if defined (__CC_ARM)
	_SETTING setting = { 0 };
	setting_read(&setting);

	gui_read_setting->wlan_ssid = setting.wlan_ssid;
	gui_read_setting->wlan_pwd = setting.wlan_pwd;
	gui_read_setting->server_ip = setting.server_ip;
	gui_read_setting->server_port = setting.server_port;
	gui_read_setting->backlight = setting.backlight;
	gui_read_setting->wlan_power = setting.wlan_power;
	gui_read_setting->wallpaper_index = setting.wallpaper_index;
	gui_read_setting->theme_index = setting.theme_index;
	gui_read_setting->sysclk = setting.sysclk;
	gui_read_setting->system_update = setting.system_update;


#elif defined (_MSC_VER)
	static uint8_t ssid[] = "openrabbit";
	static uint8_t pwd[] = "openrabbit";
	static uint8_t ip[] = "192.168.0.110";
	static uint8_t port[] = "1500";
	static uint8_t backlight = 100;
	static uint8_t wlan_power = 1;
	static uint8_t theme_index = 0;
	static uint8_t wallpaper_index = 4;
	static uint8_t sysclk = 1;

	static uint8_t init = 0;
	if (init == 0)
	{
		gui_setting_temp.wlan_ssid = ssid;
		gui_setting_temp.wlan_pwd = pwd;
		gui_setting_temp.server_ip = ip;
		gui_setting_temp.server_port = port;
		gui_setting_temp.backlight = backlight;
		gui_setting_temp.wlan_power = wlan_power;
		gui_setting_temp.theme_index = theme_index;
		gui_setting_temp.wallpaper_index = wallpaper_index;
		gui_setting_temp.sysclk = sysclk;
		gui_setting_temp.system_update = 0;
		init = 1;
	}



	gui_read_setting->wlan_ssid = gui_setting_temp.wlan_ssid;
	gui_read_setting->wlan_pwd = gui_setting_temp.wlan_pwd;
	gui_read_setting->server_ip = gui_setting_temp.server_ip;
	gui_read_setting->server_port = gui_setting_temp.server_port;
	gui_read_setting->backlight = gui_setting_temp.backlight;
	gui_read_setting->wlan_power = gui_setting_temp.wlan_power;
	gui_read_setting->theme_index = gui_setting_temp.theme_index;
	gui_read_setting->wallpaper_index = gui_setting_temp.wallpaper_index;
	gui_read_setting->sysclk = gui_setting_temp.sysclk;
	gui_read_setting->system_update = gui_setting_temp.system_update;

#endif

}


/**
* @brief 保存配置信息
* @param *gui_write_setting-待保存的配置信息的存储地址
* @retval	None
*/
void gui_hal_setting_write(gui_setting *gui_write_setting)
{
#if defined (__CC_ARM)

	_SETTING setting = { 0 };

	setting.wlan_ssid = gui_write_setting->wlan_ssid;
	setting.wlan_pwd = gui_write_setting->wlan_pwd;
	setting.server_ip = gui_write_setting->server_ip;
	setting.server_port = gui_write_setting->server_port;
	setting.backlight = gui_write_setting->backlight;
	setting.wlan_power = gui_write_setting->wlan_power;
	setting.wallpaper_index = gui_write_setting->wallpaper_index;
	setting.theme_index = gui_write_setting->theme_index;
	setting.sysclk = gui_write_setting->sysclk;
	setting.system_update = gui_write_setting->system_update;

	setting_write(&setting);


#elif defined (_MSC_VER)

	gui_setting_temp.wlan_ssid = gui_write_setting->wlan_ssid;
	gui_setting_temp.wlan_pwd = gui_write_setting->wlan_pwd;
	gui_setting_temp.server_ip = gui_write_setting->server_ip;
	gui_setting_temp.server_port = gui_write_setting->server_port;
	gui_setting_temp.backlight = gui_write_setting->backlight;
	gui_setting_temp.wlan_power = gui_write_setting->wlan_power;
	gui_setting_temp.wallpaper_index = gui_write_setting->wallpaper_index;
	gui_setting_temp.theme_index = gui_write_setting->theme_index;
	gui_setting_temp.sysclk = gui_write_setting->sysclk;
	gui_setting_temp.system_update = gui_write_setting->system_update;



#endif
}



/**
* @brief 获取RTOS的消息队列的数据
* @param pBuffer-数据存储区
* @param len-需要获取的长度
* @retval	0-有消息 1-无消息
*/
uint8_t gui_hal_message_recv(uint8_t *pbuffer, uint8_t len)
{
#if defined ( __CC_ARM   )
	return rt_mq_recv(gui_mq, pbuffer, MQ_MAX_LEN, RT_WAITING_NO);
#elif defined (_MSC_VER)
	static uint8_t mq_buf[MQ_MAX_LEN] = { 0 };
	mq_buf[0] = GUI_MQ_WLAN_STATUS;
	mq_buf[1] = 1;
	memcpy(pbuffer, mq_buf, MQ_MAX_LEN);
	return 0;
#endif
}

/**
* @brief 向RTOS的消息队列发送消息
* @param pBuffer-数据存储区
* @param len-数据长度
* @retval	0-发送成功 1-发送失败
*/
uint8_t gui_hal_message_send(uint8_t *pbuffer, uint8_t len)
{
#if defined ( __CC_ARM   )
	switch (pbuffer[0])
	{
	case MQ_WLAN_UPDATA_SET:
		rt_mq_send(wlan_mq, pbuffer, len);
		break;
	}
	return 0;

#elif defined (_MSC_VER)
	return 1;
#endif
}


#if defined (_MSC_VER)
static uint8_t ir_code_temp = 0;
#endif
/**
* @brief 红外发送
* @param addr-地址码
* @param key-控制码
* @param times-控制码连发次数
* @retval	None
*/
void gui_hal_ir_send(uint8_t addr, uint8_t key, uint8_t times)
{
#if defined ( __CC_ARM   )

	ir_send_data(addr, key, times);
#elif defined (_MSC_VER)
	ir_code_temp = key;

#endif
}

/**
* @brief 红外接收
* @param None
* @retval	None
*/
uint8_t gui_hal_ir_receive(void)
{
#if defined ( __CC_ARM   )

	return ir_receive_scan();
#elif defined (_MSC_VER)
	//return rand()%10;
	uint8_t ret = ir_code_temp;
	ir_code_temp = 0;

	return ret;

#endif
}

/**
* @brief 开启接收
* @param None
* @retval	None
*/
void gui_hal_ir_receive_start_scan(void)
{
#if defined ( __CC_ARM   )

	ir_receive_start_scan();
#elif defined (_MSC_VER)

#endif
}
/**
* @brief 关闭红外接收
* @param None
* @retval	None
*/
void gui_hal_ir_receive_stop_scan(void)
{
#if defined ( __CC_ARM   )

	ir_receive_stop_scan();
#elif defined (_MSC_VER)

#endif
}

/**
* @brief 获取电池信息
* @param *ele-电量存储指针0-100
* @param *stdby-充满标识存储指针
* @param *chrg-充电标识存储指针
* @retval	None
*/
void gui_hal_bat_get(uint8_t *ele, bool *stdby, bool *chrg)
{
#if defined ( __CC_ARM   )

	static BAT_TypeDef current_bat = { 0 };

	bat_manage_ele_get();
	bat_manage_current(&current_bat);

	*ele = current_bat.ele;
	*chrg = current_bat.chrg;
	*stdby = current_bat.stdby;

#elif defined (_MSC_VER)
	*ele = 100;
	*chrg = false;
	*stdby = false;



#endif
}

/**
* @brief 复位设备进入boot
* @param None
* @retval	None
*/
void gui_hal_reset_device_of_update(void)
{
#if defined ( __CC_ARM   )
	#include "cmsis_armcc.h"
	gui_setting setting;
	gui_hal_setting_read(&setting);
	setting.system_update = 1;
	gui_hal_setting_write(&setting);
	HAL_Delay(50);

	__set_FAULTMASK(1);
	HAL_NVIC_SystemReset();
#elif defined (_MSC_VER)
	printf("reset_device_of_update\n");

#endif
}

/**
* @brief 复位设备
* @param None
* @retval	None
*/
void gui_hal_reset_device(void)
{
#if defined ( __CC_ARM   )
	#include "cmsis_armcc.h"

	__set_FAULTMASK(1);
	HAL_NVIC_SystemReset();
#elif defined (_MSC_VER)
	printf("reset_device\n");
#endif
}


/**
* @brief 延时
* @param ms-延时的ms数
* @retval	None
*/
void gui_hal_delay_ms(uint16_t ms)
{
#if defined ( __CC_ARM   )
	rt_thread_delay(ms);
#elif defined (_MSC_VER)
	Sleep(ms);
#endif
}

/**
* @brief 获取固件版本
* @param None
* @retval	None
*/
char * gui_hal_get_firmware_version(void)
{
#if defined ( __CC_ARM   )
	static uint8_t buf[32];
	sprintf((char*)buf,"Firmware Version:%s",FIRMWARE_VERSION);
	return buf;
#elif defined (_MSC_VER)
	static uint8_t buf[32];
	sprintf((char*)buf, "Firmware Version:%s", "V1.0");
	return buf;
#endif
}


/**
* @brief 获取CPU温度
* @param None
* @retval	None
*/
float gui_hal_get_cpu_temperature(void)
{
#if defined ( __CC_ARM   )
	float value = 0;
	value = adc_tempsensor_get();
	value=(float)value*(3.3/4096);	
	value=(value-0.76)/0.0025 + 25; //转换为温度值 
	
	return value;
#elif defined (_MSC_VER)
	return rand() % 60;
#endif
}

/**
* @brief 获取MPU6050温度
* @param None
* @retval	None
*/
float gui_hal_get_mpu6050_temperature(void)
{
#if defined ( __CC_ARM   )
	return rand() % 60;
#elif defined (_MSC_VER)
	return rand() % 60;
#endif
}


#ifdef NB_IOT
/**
* @brief 获取RTOS的消息队列的数据
* @param pBuffer-数据存储区
* @param len-需要获取的长度
* @retval	0-有消息 1-无消息
*/
uint8_t gui_hal_nb_message_recv(uint8_t *pbuffer, uint16_t len)
{
#if defined ( __CC_ARM   )
	return rt_mq_recv(gui_nb_mq, pbuffer, len, RT_WAITING_NO);
#elif defined (_MSC_VER)
	return 1;
#endif
}

/**
* @brief 向RTOS的消息队列发送消息
* @param pBuffer-数据存储区
* @param len-数据长度
* @retval	0-发送成功 1-发送失败
*/
uint8_t gui_hal_nb_message_send(uint8_t *pbuffer, uint8_t len)
{
#if defined ( __CC_ARM   )
	rt_mq_send(nb_mq, pbuffer, len);
	return 0;

#elif defined (_MSC_VER)
	return 1;
#endif
}


#endif





/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/


