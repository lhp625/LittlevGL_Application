/*
*******************************************************************************************************
*
* 文件名称 : app_about_device.c
* 版    本 : V1.0
* 作    者 : OpenRabbit
* 说    明 : 关于本机
*
*******************************************************************************************************
*/

/* 头文件 -----------------------------------------------------------*/
#include "gui_user.h"

/* 设备名称,为了防止VS编译器中文支持问题,定义成这样 */
static const char device_name[] = { "\xe4\xba\xa7\xe5\x93\x81\xe5\x90\x8d:\xe9\x87\x8e\xe7\x89\x9b\xe6\x89\x8b\xe6\x8c\x81\xe6\x9c\xba" };
static const char vender_name[] = { "品牌:跳兔科技" };

/**
* @brief 退出APP
* @param obj-事件对象
* @param event-事件类型
* @retval	None
*/
static void lv_app_btn_back_event_cb(lv_obj_t * obj, lv_event_t event)
{
	if (event == LV_EVENT_RELEASED)
	{
		/* 退出窗口 */
		lv_obj_del(lv_obj_get_parent(obj));

	}
}


lv_obj_t * app_about_device_create(lv_obj_t *parent, void *user_data)
{
	(void)user_data;

	static lv_style_t style_btn_back;
	static lv_style_t style_label_back;
	static lv_style_t style_label_title;
	static lv_style_t style_bg;
	static lv_style_t style_label;
	static lv_style_t style_label_en;
	static lv_style_t style_label_en1;
	static lv_style_t style_img;


	lv_style_copy(&style_btn_back, &lv_style_transp);
	lv_style_copy(&style_label_title, &lv_style_plain_color);
	lv_style_copy(&style_label_back, &lv_style_plain_color);

	lv_style_copy(&style_img, &lv_style_transp);
	style_img.image.color = lv_color_hex(0x004874e8);
	style_img.image.intense = 255;

	if (gui_current_theme == THEME_NEMO)
		style_label_back.text.color = LV_COLOR_WHITE;
	else
		style_label_back.text.color = LV_COLOR_BLACK;
	style_label_back.text.font = &lv_font_roboto_28;


	if (gui_current_theme == THEME_NEMO)
		style_label_title.text.color = LV_COLOR_WHITE;
	else
		style_label_title.text.color = LV_COLOR_BLACK;
	style_label_title.text.font = &gb2312_puhui16;

	lv_obj_t *obj = lv_obj_create(parent, NULL);
	lv_obj_set_size(obj, LV_HOR_RES, LV_VER_RES);

	lv_obj_t *btn_back = lv_btn_create(obj, NULL);
	lv_obj_set_size(btn_back, 100, 60);
	lv_obj_set_event_cb(btn_back, lv_app_btn_back_event_cb);
	lv_btn_set_style(btn_back, LV_BTN_STYLE_REL, &style_btn_back);
	lv_btn_set_style(btn_back, LV_BTN_STYLE_PR, &style_btn_back);

	lv_obj_t *label_back = lv_label_create(obj, NULL);
	lv_label_set_style(label_back, LV_LABEL_STYLE_MAIN, &style_label_back);
	lv_label_set_text(label_back, LV_SYMBOL_LEFT);
	lv_obj_align(label_back, btn_back, LV_ALIGN_IN_LEFT_MID, 20, 0);


	lv_obj_t *label_title = lv_label_create(obj, NULL);
	lv_label_set_style(label_title, LV_LABEL_STYLE_MAIN, &style_label_title);
	lv_label_set_text(label_title, "关于本机");
	lv_obj_align(label_title, obj, LV_ALIGN_IN_TOP_MID, 0, 16);




	lv_obj_t *page;
	lv_obj_t *img;
	lv_obj_t *label;
	lv_obj_t *cont;

	//page = lv_page_create(obj,NULL);

	cont = lv_cont_create(obj, NULL);
	lv_obj_set_pos(cont, 0, 60);
	lv_obj_set_size(cont, lv_obj_get_width(lv_obj_get_parent(cont)), 150);
	//lv_page_set_sb_mode(page, LV_SB_MODE_OFF);



	img = lv_img_create(cont, NULL);
	lv_img_set_src(img,&set_system);
	lv_obj_align(img, cont, LV_ALIGN_IN_LEFT_MID, 18, 0);

	/* 这里创建label只是为了复制其属性 */
	label = lv_label_create(cont, NULL);
	lv_label_get_style(label,LV_LABEL_STYLE_MAIN);

	lv_style_copy(&style_label, lv_label_get_style(label, LV_LABEL_STYLE_MAIN));
	style_label.text.font = &gb2312_puhui16;

	lv_style_copy(&style_label_en, lv_label_get_style(label, LV_LABEL_STYLE_MAIN));
	lv_style_copy(&style_label_en1, lv_label_get_style(label, LV_LABEL_STYLE_MAIN));
	style_label_en.text.font = &lv_font_roboto_16;
	style_label_en1.text.font = &lv_font_roboto_22;

	lv_obj_del(label);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label, img, LV_ALIGN_OUT_RIGHT_MID, 20, -60);
	lv_label_set_static_text(label, device_name);
	//lv_label_set_text(label, device_name);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label, img, LV_ALIGN_OUT_RIGHT_MID, 20, -30);
	lv_label_set_static_text(label, vender_name);
	//lv_label_set_text(label, "品牌:跳兔科技");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label,LV_LABEL_STYLE_MAIN,&style_label);
	lv_obj_align(label, img, LV_ALIGN_OUT_RIGHT_MID, 20, -0);
	lv_label_set_text(label,"RTOS:RT-Thread-V4.0.0");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label, img, LV_ALIGN_OUT_RIGHT_MID, 20, 30);
	lv_label_set_text(label, "GUI:LittlevGL-V6.0.0");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label);
	lv_obj_align(label, img, LV_ALIGN_OUT_RIGHT_MID, 20, 60);
	lv_label_set_text(label, gui_hal_get_firmware_version());

	uint16_t y_pos = 0;

	y_pos = 210;

	/* MCU */
	cont = lv_cont_create(obj, NULL);
	lv_obj_set_pos(cont, 0, y_pos);
	lv_obj_set_size(cont, lv_obj_get_width(lv_obj_get_parent(cont)) / 3, lv_obj_get_width(lv_obj_get_parent(cont)) / 3);
	
	img = lv_img_create(cont, NULL);
	lv_img_set_style(img, LV_IMG_STYLE_MAIN, &style_img);
	lv_img_set_src(img, &set_mcu);
	lv_obj_align(img, cont, LV_ALIGN_CENTER, 0, -20);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en1);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 5);
	lv_label_set_text(label, "MCU");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 100);
	lv_label_set_text(label, "STM32F429IGT6");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 120);
	lv_label_set_text(label, "FLASH:1MByte");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 140);
	lv_label_set_text(label, "SRAM:192KByte");



	/* memory */
	cont = lv_cont_create(obj, NULL);
	lv_obj_set_pos(cont, lv_obj_get_width(lv_obj_get_parent(cont)) / 3, y_pos);
	lv_obj_set_size(cont, lv_obj_get_width(lv_obj_get_parent(cont)) / 3, lv_obj_get_width(lv_obj_get_parent(cont)) / 3);

	img = lv_img_create(cont, NULL);
	lv_img_set_style(img, LV_IMG_STYLE_MAIN, &style_img);
	lv_img_set_src(img, &set_memory);
	lv_obj_align(img, cont, LV_ALIGN_CENTER, 0, -20);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en1);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 5);
	lv_label_set_text(label, "MEMORY");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 100);
	lv_label_set_text(label, "EEPROM:BL24C02");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 120);
	lv_label_set_text(label, "FLASH:XM25QH128");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 140);
	lv_label_set_text(label, "SDCARD:MMC");


	/* RAM */
	cont = lv_cont_create(obj, NULL);
	lv_obj_set_pos(cont, (lv_obj_get_width(lv_obj_get_parent(cont)) / 3) * 2, y_pos);
	lv_obj_set_size(cont, lv_obj_get_width(lv_obj_get_parent(cont)) / 3, lv_obj_get_width(lv_obj_get_parent(cont)) / 3);

	img = lv_img_create(cont, NULL);
	lv_img_set_style(img, LV_IMG_STYLE_MAIN, &style_img);
	lv_img_set_src(img, &set_ram);
	lv_obj_align(img, cont, LV_ALIGN_CENTER, 0, -20);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en1);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 5);
	lv_label_set_text(label, "RAM");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 100);
	lv_label_set_text(label, "W9825G6KH-6");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 120);
	lv_label_set_text(label, "SDRAM:32M");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 140);
	lv_label_set_text(label, "Data width:16bit");

	y_pos = 370;
	/* screen */
	cont = lv_cont_create(obj, NULL);
	lv_obj_set_pos(cont, (lv_obj_get_width(lv_obj_get_parent(cont)) / 3) * 0, y_pos);
	lv_obj_set_size(cont, lv_obj_get_width(lv_obj_get_parent(cont)) / 3, lv_obj_get_width(lv_obj_get_parent(cont)) / 3);

	img = lv_img_create(cont, NULL);
	lv_img_set_style(img, LV_IMG_STYLE_MAIN, &style_img);
	lv_img_set_src(img, &set_screen);
	lv_obj_align(img, cont, LV_ALIGN_CENTER, 0, -20);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en1);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 5);
	lv_label_set_text(label, "SCREEN");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 100);
	lv_label_set_text(label, "LCD-RGB");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 120);
	lv_label_set_text(label, "Resolution:800*480");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 140);
	lv_label_set_text(label, "Touch:GT9147");


	/* bat */
	cont = lv_cont_create(obj, NULL);
	lv_obj_set_pos(cont, (lv_obj_get_width(lv_obj_get_parent(cont)) / 3) * 1, y_pos);
	lv_obj_set_size(cont, lv_obj_get_width(lv_obj_get_parent(cont)) / 3, lv_obj_get_width(lv_obj_get_parent(cont)) / 3);

	img = lv_img_create(cont, NULL);
	lv_img_set_style(img, LV_IMG_STYLE_MAIN, &style_img);
	lv_img_set_src(img, &set_bat);
	lv_obj_align(img, cont, LV_ALIGN_CENTER, 0, -20);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en1);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 5);
	lv_label_set_text(label, "BATTERY");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 100);
	lv_label_set_text(label, "lithium battery");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 120);
	lv_label_set_text(label, "3000mah");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 140);
	lv_label_set_text(label, "3.7V");


	/* wireless */
	cont = lv_cont_create(obj, NULL);
	lv_obj_set_pos(cont, (lv_obj_get_width(lv_obj_get_parent(cont)) / 3) * 2, y_pos);
	lv_obj_set_size(cont, lv_obj_get_width(lv_obj_get_parent(cont)) / 3, lv_obj_get_width(lv_obj_get_parent(cont)) / 3);

	img = lv_img_create(cont, NULL);
	lv_img_set_style(img, LV_IMG_STYLE_MAIN, &style_img);
	lv_img_set_src(img, &set_wireless);
	lv_obj_align(img, cont, LV_ALIGN_CENTER, 0, -20);

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en1);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 5);
	lv_label_set_text(label, "WIRELESS");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 100);
	lv_label_set_text(label, "IR Send & Recv");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 120);
	lv_label_set_text(label, "WIFI:ESP12S");

	label = lv_label_create(cont, NULL);
	lv_label_set_style(label, LV_LABEL_STYLE_MAIN, &style_label_en);
	lv_obj_align(label, cont, LV_ALIGN_IN_TOP_LEFT, 5, 140);
	lv_label_set_text(label, "2.4G:NRF24L01P");

	return obj;
}





/***************************** 跳兔科技 www.whtiaotu.com (END OF FILE) *********************************/

